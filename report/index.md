---
title: An Open Source Protocol and Implementation of Wireless DMX for Consumer Hardware
author: Ryan Delaney (rjd22 / 180016561)
linkcolor: fuc-gray
citecolor: fuc-gray
include-before: |
    > Word Count: {{WC}}
---

[*include '00.abstract']
[*include '05.acknowledgements']
[*include '10.declaration']
[*include '20.introduction']
[*include '30.context']
[*include '40.ethics']
[*include '50.design']
[*include '60.implementation']
[*include '70.testing']
[*include '80.evaluation']
[*include '85.future-work']
[*include '90.conclusion']
[*include '95.appendix']

\pagebreak
