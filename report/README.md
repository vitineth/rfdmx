# Report

This folder contains the actual report that is submitted this this project along with the building structure. This uses a custom written builder which assembles the provided markdown files and runs the result through pandoc with a few configuration options and utilities performed. 

## Builder

The builder adds a new syntax to the `index.md` file, if it encounters the form `[*include 'name']` it will attempt to read the file from `segments/name.md` and replace the line with this data. Included files will be forced to end with a new line, unless they end with `\`. 

The build script will take this combined document and then apply the pandoc filter `builder/wordcount.lua` against it which will return the complete number of words, ignoring any markdown formatting which will then be used to replace any occurrences of the pattern `{{WC}}`. 

The newly produced file will be run through pandoc with the following properties:

* Ensure support for the `startnum` package
* Apply all LaTeX in `builder/options.sty` in the header, this does general page setup and structure and all blocks are commented individually
* Add the bibliography `bibliography.bib` which should be synced with Zotero through Better BibTeX (and citations should be formatted according to the rules in `builder/bibliography.csl` which is the IEEE standard)
* Use xelatex as the pdf builder engine
* Use `Ubuntu Mono` as the monspace font globally
* Use `Crimson Text` as the serif font globally
* Highlight code blocks using the syntax highlighting theme in `builder/tango.theme`
* Add a Table of Contents
* Expand citations to link with the bibliography
* Number the sections of the report
* Write the result to build.pdf

As you can see, it handles a lot. Once this is produced, on supported platforms, it will invoke `cpdf` from `builder/cpdf` to insert `assets/title-page.pdf` at the start of the document, maintaining hyperlinks. You should install the `cpdf` dependency as outlined in `builder/cpfg/README.md`.

