import fs from 'node:fs/promises'
import fsr from 'node:fs';
import { execSync } from 'child_process';
import path from 'path';

function getCpdfExecutable(){
  switch(process.platform){
    case 'darwin':
      if(execSync("uname -m", {encoding: 'utf8'}).includes("arm64")){
        return "builder/cpdf/OSX-ARM/cpdf";
      }else{
        return "builder/cpdf/OSX-Intel/cpdf";
      }
      break;
    case 'win32':
      if(process.arch.includes("64")){
        return "builder/cpdf/Windows64bit/cpdf";
      }else{
        return "builder/cpdf/Windows32bit/cpdf";
      }
      break;
    case 'linux':
      if(process.arch.includes("64")){
        return "builder/cpdf/Linux-Intel-64bit/cpdf";
      }else{
        return "builder/cpdf/Linux-Intel-32bit/cpdf";
      }
  }
  return null;
}

// Source: https://stackoverflow.com/a/12420737
var getDuration = function(millis) {
  var dur = {};
  var units = [
    { label: "ms", mod: 1000 },
    { label: "s", mod: 60 },
    { label: "m", mod: 60 },
    { label: "h", mod: 24 },
    { label: "d", mod: 31 }
  ];
  // calculate the individual unit values...
  units.forEach(function(u) {
    millis = (millis - (dur[u.label] = (millis % u.mod))) / u.mod;
  });
  // convert object to a string representation...
  var nonZero = function(u) { return dur[u.label]; };
  dur.toString = function() {
    return units
      .reverse()
      .filter(nonZero)
      .map(function(u) {
        return (Math.round(dur[u.label] * 100) / 100) + " " + (dur[u.label] == 1 ? u.label.slice(0, -1) : u.label);
      })
      .join(', ');
  };
  return dur;
};

// Check for existence
const segmentsExists = fsr.existsSync('segments');
if (!fsr.existsSync('bibliography.bib')) {
  console.error('Failed to compile - bibliography does not exist');
  process.exit(1);
}

// Load the file
console.log('Loading base markdown file');
let baseContent;
try {
  baseContent = await fs.readFile('index.md', { encoding: 'utf8' });
} catch (e) {
  console.error('Failed to load the index markdown file - an error occured while reading the file');
  if (Object.hasOwn(e, 'message')) console.error(`  [${Object.hasOwn(e, 'code') ? e.code : 'error'}]: `, e.message);
  process.exit(1);
}

// Insert the bibliography footer
baseContent += '\n\n# Bibliography\n# \n';

// Then process includes
//    format: [*include <filename>]
console.log('Processing for included files');
const marker = /\[\*include '(.+?)'\]/gi;
const segments = [];
let lastEnd = 0;
let startIndex = 0;
let search;
while ((search = baseContent.substr(startIndex).search(marker)) !== -1) {
  let end = search;
  while (baseContent.substr(startIndex).charAt(end) !== ']') end++;

  const match = /\[\*include '(.+?)'\]/gi.exec(baseContent.substring(search + startIndex, end + startIndex + 1));
  if (match === null) {
    console.error('Failed to parse an include statement - it did not match the regex correctly');
    process.exit(1);
  }

  segments.push(baseContent.substring(lastEnd, search + startIndex));
  lastEnd = end + startIndex + 1;

  if (!segmentsExists) {
    console.error(`Found an include but the segments/ folder does not exist`);
    process.exit(1);
  }

  try {
    let segmentContent = await fs.readFile(path.join('segments', match[1] + '.md'), { encoding: 'utf8' });
    if (!segmentContent.endsWith('\n\n')) {
      if (segmentContent.endsWith("\\")) {
        segmentContent = segmentContent.substring(0, segmentContent.length - 1);
      } else {
        segmentContent += "\n\n";
      }
    }

    segments.push(segmentContent.split('\n').filter((e, i) => {
      let et = e.trim();
      if (et.startsWith("//TODO") || et.startsWith("// TODO")) {
        console.log(`[TODO][${match[1]}:${i + 1}]: ${et.substring(et.indexOf(':') + 1)}`);
        return false;
      }

      return true;
    }).join('\n'));

    // segments.push(segmentContent);
  } catch (e) {
    console.error(`Failed to load the included file ${match[1]}.md - an error occured while reading the file`);
    if (Object.hasOwn(e, 'message')) console.error(`  [${Object.hasOwn(e, 'code') ? e.code : 'error'}]: `, e.message);
    process.exit(1);
  }

  startIndex = startIndex + end;
}

segments.push(baseContent.substring(lastEnd));
let content = segments.join('');
// const words = content.split(/\s+/gi).length;
// content = content.replaceAll('{{WC}}', String(words));
// console.log(`Approximate word count: ${words}`);

// Then write and compile
console.log('Outputting temp file');
await fs.writeFile('temp.md', content, { encoding: 'utf8' });

console.log('Counting words')
const wordCountCmd = `pandoc --lua-filter builder/wordcount.lua temp.md`;
console.log(`    [${wordCountCmd}]`);
const wordCount = execSync(wordCountCmd, { encoding: 'utf8' });
console.log(`Approximate word count: ${wordCount}`);
content = content.replaceAll('{{WC}}', String(wordCount));
await fs.writeFile('temp.md', content, { encoding: 'utf8' });

const start = performance.now();
const command = 'pandoc --from=markdown+startnum temp.md -H builder/options.sty --bibliography bibliography.bib --csl builder/bibliography.csl --pdf-engine=xelatex -V monofont="Ubuntu Mono" -V mainfont="Crimson Text" --highlight-style builder/tango.theme --toc --citeproc -N -o build.pdf';
console.log(`    [${command}]`);
execSync(command);
const time = (performance.now() - start);
await fs.rm('temp.md');

const cpdfExecutable = getCpdfExecutable();
if(cpdfExecutable === null){
  console.log('Could not determine platform, cannot merge title page automatically');
}else{
  console.log(`   [${cpdfExecutable} assets/title-page.pdf build.pdf -o build.pdf]`)
  execSync(`${cpdfExecutable} assets/title-page.pdf build.pdf -o build.pdf`);
}

console.log(`Built successfully in ${getDuration(time).toString()}`);
