import fs from "node:fs";
import { fork } from "node:child_process";
import { join, dirname } from "node:path";
import { fileURLToPath } from "node:url";

let activeRun = undefined;

fs.watch("../", { persistent: true, recursive: true }, (type, name) => {
  if (name === "report/build.pdf") return;
  if (name === "report/temp.md") return;
  if(!name.startsWith("report/")) return; // Ignore everything else in the project
  
  if (activeRun !== undefined) {
    console.debug("Active run defined, not recompiling");
    return;
  }
  activeRun = Date.now();
  try {
    console.log(type, `"${name}"`);
    fork(join(dirname(fileURLToPath(import.meta.url)), "index.mjs"));
  } catch (e) {
  } finally {
    activeRun = undefined;
  }
});
