# An Open Source Protocol and Implementation of Wireless DMX for Consumer Hardware

This project was completed as my MSci Computer Science at the University of St Andrews. A description of the project is given below in its original proposal form. 

The full report for this project can be read in [the submitted report](report/build.pdf) (with minor modifications for publishing). 

_More information on the IP rights of this project can be found in the [University policy](https://www.st-andrews.ac.uk/policy/research-external-work/intellectual-property-policy.pdf)_

## Original Proposal

*Proposed by Ryan Delaney, including discussion with Saleem Bhatti*

### Overview

Lighting control is universally controlled via the DMX-512A protocol with the original design created in 1986 with revisions up to 2008. To expand the usage, multiplexed systems were developed called ArtNet and sACN which use conventional ethernet networks to transmit their data. These protocols are both open, and can be implemented by anyone. In the wireless field, choice is limited with the leading standard being W-DMX. Currently to use W-DMX you must register as an OEM supplier. The goal of this project is to implement a fully open source wireless lighting protocol over radio networks on readily available consumer hardware with a translation layer from ArtNet or sACN at the transmitter for integration with existing lighting systems. The intended artifacts are:

### Protocol Design

A protocol would be designed for reliable transmission of lighting control data over 2.4/5GHz radio networks.

### Protocol Implementation

An example implementation of the protocol would be implemented in a language that allows embeddable programing - likely Rust for its memory safe nature and ability to cross compile to various platforms. This implementation would also handle conversion of ArtNet to this protocol.

### Hardware Implementation

The protocol would be implemented on consumer hardware, ideally producing valid DMX512A signals that could be used to control a real light. This would be a proof of concept implementation using any available radio network but the protocol design and implementation should be implementable on any radio frequency.

### Funding

No funding from the department would be required as I am happy to cover these costs. Access to existing hardware would be appreciated if it is available (access to Arduino boards for example).

### Risk

This project is quite large and therefore carries some risk so there are a series of steps through which the scope of the project can be reduced without changing the core principles of the project such as removing the reliance on hardware, using less complex hardware, and changing languages from Rust to something with which I have more familiarity.
