//
// Created by Ryan on 27/04/2023.
//

#include <iostream>
#include "test_array_map.h"
#include "array_map.h"

int test_array_map(){
    array_map* map = new array_map(2);

    uint8_t v1 = 120;
    uint8_t v2 = 242;
    uint8_t v3 = 43;

    void* v1p = &v1;
    void* v2p = &v2;
    void* v3p = &v3;

    // Check for addition + query
    map->set(0, v1p);
    if(map->get(0) != v1p){
        std::cout << "[array-map]: Fail, get(0) did not match" << std::endl;
        std::cout << "got " << map->get(0) << " - expected " << v1p << std::endl;
        delete map;
        return 1;
    }

    map->set(1, v2p);
    if(map->get(1) != v2p){
        std::cout << "[array-map]: Fail, get(1) did not match" << std::endl;
        delete map;
        return 1;
    }

    // Check for valid rejection
    if(map->get(2) != nullptr){
        std::cout << "[array-map]: Fail, get(2) did not match" << std::endl;
        delete map;
        return 1;
    }

    // Check for resize + is_present
    map->set(2, v3p);
    if(!map->is_present(2)){
        std::cout << "[array-map]: Fail, is_present(2) did not match" << std::endl;
        delete map;
        return 1;
    }

    std::cout << "Array map valid" << std::endl;

    return 0;
}