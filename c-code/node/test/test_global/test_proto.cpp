//
// Created by ryan on 3/15/23.
//    I know some of these tests violate DRY with a lot of the same content but this may be abstracted out later. For
//    now its better that it works successfully than it doesn't work but its written beautifully
//

#include <iostream>
#include <thread>
#include "unity.h"
#include "protocol.h"
#include "logging.h"
#include "xtest_simulator/test_simulator.h"
#include "ztest_node_store/test_node_store.h"
#include "array-map/test_array_map.h"

#define HEX_WIDTH 20

inline void dump_hex(uint8_t *values, size_t length) {
    char buffer[4];
    size_t current_line = 0;
    for (size_t i = 0; i < length; ++i) {
        if (current_line % HEX_WIDTH == 0) {
            if (current_line != 0) std::cout << std::endl;
            sprintf(buffer, "%02zu", current_line);
            buffer[3] = 0;
            std::cout << buffer << " | ";
        }
        sprintf(buffer, "%02X ", values[i]);
        buffer[3] = '\0';
        std::cout << buffer;
        current_line++;
    }
    std::cout << std::endl;
}

void test__rf_checkin_t() {
    static auto callback_called = false;
    std::cout << std::endl << std::endl << "## BEGIN TESTING " << __func__ << std::endl << std::endl;
    static auto test__rf_checkin_t__packet = rf_checkin_t(13, 243, 123, 143);
    auto on_checkin = [](rf_checkin_t *checkin, rf_proto_handler *handler) {
        TEST_ASSERT_NOT_NULL_MESSAGE(checkin, "checkin pointer was null");
        TEST_ASSERT_TRUE_MESSAGE(test__rf_checkin_t__packet.universe == checkin->universe,
                                 "universe did not match");
        TEST_ASSERT_EQUAL_UINT8_MESSAGE(test__rf_checkin_t__packet.packet_type, checkin->packet_type,
                                        "packet_type did not match");
        TEST_ASSERT_EQUAL_UINT8_MESSAGE(test__rf_checkin_t__packet.sequence, checkin->sequence,
                                        "sequence did not match");
        TEST_ASSERT_EQUAL_UINT8_MESSAGE(test__rf_checkin_t__packet.flags, checkin->flags,
                                        "flags did not match");
        TEST_ASSERT_EQUAL_UINT8_MESSAGE(test__rf_checkin_t__packet.device_id, checkin->device_id,
                                        "device_id did not match");
        callback_called = true;
        TEST_PASS_MESSAGE("Test passed successfully");
    };

    auto handler = rf_proto_handler(
            [](uint8_t a, uint8_t b, rf_proto_handler *handler) { TEST_FAIL_MESSAGE("Requested resent"); },
            nullptr,
            on_checkin,
            nullptr,
            [](rf_request_t *, rf_proto_handler *) {},
            [](int line, rf_proto_handler *handler) { TEST_FAIL_MESSAGE("Panicked"); },
            0,
            UINT8_MAX
    );

    uint8_t buffer[test__rf_checkin_t__packet.required_size()];
    test__rf_checkin_t__packet.serialize(buffer);

    handler.parse(buffer, test__rf_checkin_t__packet.required_size());
    if (!callback_called) {
        TEST_FAIL_MESSAGE("callback has never been called");
    }
}

void test__rf_dmx_t__single_packet() {
    static auto callback_called = false;
    std::cout << std::endl << std::endl << "## BEGIN TESTING " << __func__ << std::endl << std::endl;
    char buffer[0];
    static rf_dmx_t input = rf_dmx_t(14, 0, 0, DIFF, 0, 10, 0, buffer);
    input.set_start_flag();
    input.set_termination_flag();

    auto handler = rf_proto_handler(
            [](uint8_t a, uint8_t b, rf_proto_handler *handler) { TEST_FAIL_MESSAGE("Requested resend"); },
            [](rf_dmx_t *dmx, rf_proto_handler *handler) {
                TEST_ASSERT_TRUE_MESSAGE(dmx->has_start_flag(), "has start flag");
                TEST_ASSERT_FALSE_MESSAGE(dmx->has_continuation_flag(), "has continue flag");
                TEST_ASSERT_TRUE_MESSAGE(dmx->has_termination_flag(), "has terminate flag");
                TEST_ASSERT_FALSE_MESSAGE(dmx->has_resend_flag(), "has resend flag");
                TEST_ASSERT_TRUE_MESSAGE(dmx->size == 0, "size == 0");
                TEST_ASSERT_TRUE_MESSAGE(dmx->universe == 10, "universe == 10");
                TEST_ASSERT_TRUE_MESSAGE(dmx->offset == 0, "offset == 0");
                TEST_ASSERT_TRUE_MESSAGE(dmx->device_id == 14, "device_id == 14");
                TEST_ASSERT_TRUE_MESSAGE(dmx->sequence == 0, "sequence == 0");
                TEST_ASSERT_TRUE_MESSAGE(dmx->compression_type == DIFF, "compression_type == DIFF");
                callback_called = true;
                TEST_PASS_MESSAGE("callback executed");
            },
            [](rf_checkin_t *checkin, rf_proto_handler *handler) { TEST_FAIL_MESSAGE("Handled a checkin"); },
            nullptr,
            [](rf_request_t *, rf_proto_handler *) {},
            [](int line, rf_proto_handler *handler) { TEST_FAIL_MESSAGE("Panicked"); },
            10,
            14
    );

    uint8_t serialized[input.required_size()];
    input.serialize(serialized);

    handler.parse(serialized, input.required_size());
    if (!callback_called) {
        TEST_FAIL_MESSAGE("callback has never been called");
    }
}

void test__rf_dmx_t__multi_packet_normal_delivery() {
    static auto callback_called = false;
    std::cout << std::endl << std::endl << "## BEGIN TESTING " << __func__ << std::endl << std::endl;
    uint8_t b1[rf_dmx_t::PAYLOAD_SIZE] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13,
                                          0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x20, 0x21, 0x22, 0x23};
    uint8_t b2[rf_dmx_t::PAYLOAD_SIZE] = {0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36,
                                          0x37, 0x38, 0x39, 0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46};
    uint8_t b3[rf_dmx_t::PAYLOAD_SIZE] = {0x47, 0x48, 0x49, 0x50, 0x51, 0x52, 0x53, 0x5, 0x55};

    static uint8_t combined[rf_dmx_t::PAYLOAD_SIZE + rf_dmx_t::PAYLOAD_SIZE + 9];
    std::copy(b1, b1 + rf_dmx_t::PAYLOAD_SIZE, combined);
    std::copy(b2, b2 + rf_dmx_t::PAYLOAD_SIZE, combined + rf_dmx_t::PAYLOAD_SIZE);
    std::copy(b3, b3 + 9, combined + (2 * rf_dmx_t::PAYLOAD_SIZE));

    static auto p1 = rf_dmx_t(14, 0, 0, DIFF, 0, 10, rf_dmx_t::PAYLOAD_SIZE, reinterpret_cast<char *>(b1));
    static auto p2 = rf_dmx_t(14, 1, 0, DIFF, 1, 10, rf_dmx_t::PAYLOAD_SIZE, reinterpret_cast<char *>(b2));
    static auto p3 = rf_dmx_t(14, 2, 0, DIFF, 2, 10, 9, reinterpret_cast<char *>(b3));

    p1.set_start_flag();
    p2.set_continuation_flag();
    p3.set_termination_flag();

    uint8_t p1s[p1.required_size()];
    uint8_t p2s[p2.required_size()];
    uint8_t p3s[p3.required_size()];

    p1.serialize(p1s);
    p2.serialize(p2s);
    p3.serialize(p3s);

    auto handler = rf_proto_handler(
            [](uint8_t a, uint8_t b, rf_proto_handler *handler) { TEST_FAIL_MESSAGE("Requested resend"); },
            [](rf_dmx_t *dmx, rf_proto_handler *handler) {
                TEST_ASSERT_FALSE_MESSAGE(dmx->has_resend_flag(), "has resend flag");
                TEST_ASSERT_TRUE_MESSAGE(dmx->size == 55, "size == 55");
                TEST_ASSERT_TRUE_MESSAGE(dmx->universe == 10, "universe == 10");
                TEST_ASSERT_TRUE_MESSAGE(dmx->device_id == 14, "device_id == 14");
                TEST_ASSERT_TRUE_MESSAGE(dmx->compression_type == DIFF, "compression_type == DIFF");
                TEST_ASSERT_EQUAL_UINT8_ARRAY_MESSAGE(combined, dmx->data, dmx->size, "combined == dmx->data");
                dump_hex(reinterpret_cast<uint8_t *>(dmx->data), dmx->size);
                callback_called = true;
                TEST_PASS_MESSAGE("callback executed");
            },
            [](rf_checkin_t *checkin, rf_proto_handler *handler) { TEST_FAIL_MESSAGE("Handled a checkin"); },
            nullptr,
            [](rf_request_t *, rf_proto_handler *) {},
            [](int line, rf_proto_handler *handler) { TEST_FAIL_MESSAGE("Panicked"); },
            10,
            14
    );

    handler.parse(p1s, p1.required_size());
    handler.parse(p2s, p2.required_size());
    handler.parse(p3s, p3.required_size());
    if (!callback_called) {
        TEST_FAIL_MESSAGE("callback has never been called");
    }
}

void test__rf_dmx_t__multi_packet_missing_start() {
    static auto callback_called = false;
    std::cout << std::endl << std::endl << "## BEGIN TESTING " << __func__ << std::endl << std::endl;
    uint8_t b1[rf_dmx_t::PAYLOAD_SIZE] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13,
                                          0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x20, 0x21, 0x22, 0x23};
    uint8_t b2[rf_dmx_t::PAYLOAD_SIZE] = {0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36,
                                          0x37, 0x38, 0x39, 0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46};
    uint8_t b3[rf_dmx_t::PAYLOAD_SIZE] = {0x47, 0x48, 0x49, 0x50, 0x51, 0x52, 0x53, 0x5, 0x55};

    static uint8_t combined[rf_dmx_t::PAYLOAD_SIZE + rf_dmx_t::PAYLOAD_SIZE + 9];
    std::copy(b1, b1 + rf_dmx_t::PAYLOAD_SIZE, combined);
    std::copy(b2, b2 + rf_dmx_t::PAYLOAD_SIZE, combined + rf_dmx_t::PAYLOAD_SIZE);
    std::copy(b3, b3 + 9, combined + (2 * rf_dmx_t::PAYLOAD_SIZE));

    static auto p1 = rf_dmx_t(14, 0, 0, DIFF, 0, 10, rf_dmx_t::PAYLOAD_SIZE, reinterpret_cast<char *>(b1));
    static auto p2 = rf_dmx_t(14, 1, 0, DIFF, 1, 10, rf_dmx_t::PAYLOAD_SIZE, reinterpret_cast<char *>(b2));
    static auto p3 = rf_dmx_t(14, 2, 0, DIFF, 2, 10, 9, reinterpret_cast<char *>(b3));

    p1.set_start_flag();
    p1.set_resend_flag();
    p2.set_continuation_flag();
    p3.set_termination_flag();

    uint8_t p1s[p1.required_size()];
    uint8_t p2s[p2.required_size()];
    uint8_t p3s[p3.required_size()];

    static auto p1sp = &p1s;

    p1.serialize(p1s);
    p2.serialize(p2s);
    p3.serialize(p3s);

    static auto handler = rf_proto_handler(
            [](uint8_t a, uint8_t b, rf_proto_handler *handler) {
                TEST_ASSERT_EQUAL_MESSAGE(a, 0, "sequence == 0");
                TEST_ASSERT_EQUAL_MESSAGE(b, 0, "offset == 0");
                std::cout << "resend requested" << std::endl;
                handler->parse(*p1sp, p1.required_size());
            },
            [](rf_dmx_t *dmx, rf_proto_handler *handler) {
                TEST_ASSERT_FALSE_MESSAGE(dmx->has_resend_flag(), "has resend flag");
                TEST_ASSERT_TRUE_MESSAGE(dmx->size == 55, "size == 55");
                TEST_ASSERT_TRUE_MESSAGE(dmx->universe == 10, "universe == 10");
                TEST_ASSERT_TRUE_MESSAGE(dmx->device_id == 14, "device_id == 14");
                TEST_ASSERT_TRUE_MESSAGE(dmx->compression_type == DIFF, "compression_type == DIFF");
                TEST_ASSERT_EQUAL_UINT8_ARRAY_MESSAGE(combined, dmx->data, dmx->size, "combined == dmx->data");
                dump_hex(reinterpret_cast<uint8_t *>(dmx->data), dmx->size);
                callback_called = true;
                TEST_PASS_MESSAGE("callback executed");
            },
            [](rf_checkin_t *checkin, rf_proto_handler *handler) { TEST_FAIL_MESSAGE("Handled a checkin"); },
            nullptr,
            [](rf_request_t *, rf_proto_handler *) {},
            [](int line, rf_proto_handler *handler) { TEST_FAIL_MESSAGE("Panicked"); },
            10,
            14
    );

//    handler.parse(p1s, p1.required_size());
    handler.parse(p2s, p2.required_size());
    handler.parse(p3s, p3.required_size());
    if (!callback_called) {
        TEST_FAIL_MESSAGE("callback has never been called");
    }
}

void test__rf_dmx_t__multi_packet_missing_continuation() {
    static auto callback_called = false;
    std::cout << std::endl << std::endl << "## BEGIN TESTING " << __func__ << std::endl << std::endl;

    uint8_t b1[rf_dmx_t::PAYLOAD_SIZE] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13,
                                          0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x20, 0x21, 0x22, 0x23};
    uint8_t b2[rf_dmx_t::PAYLOAD_SIZE] = {0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36,
                                          0x37, 0x38, 0x39, 0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46};
    uint8_t b3[rf_dmx_t::PAYLOAD_SIZE] = {0x47, 0x48, 0x49, 0x50, 0x51, 0x52, 0x53, 0x5, 0x55};

    static uint8_t combined[rf_dmx_t::PAYLOAD_SIZE + rf_dmx_t::PAYLOAD_SIZE + 9];
    std::copy(b1, b1 + rf_dmx_t::PAYLOAD_SIZE, combined);
    std::copy(b2, b2 + rf_dmx_t::PAYLOAD_SIZE, combined + rf_dmx_t::PAYLOAD_SIZE);
    std::copy(b3, b3 + 9, combined + (2 * rf_dmx_t::PAYLOAD_SIZE));

    static auto p1 = rf_dmx_t(14, 0, 0, DIFF, 0, 10, rf_dmx_t::PAYLOAD_SIZE, reinterpret_cast<char *>(b1));
    static auto p2 = rf_dmx_t(14, 1, 0, DIFF, 1, 10, rf_dmx_t::PAYLOAD_SIZE, reinterpret_cast<char *>(b2));
    static auto p3 = rf_dmx_t(14, 2, 0, DIFF, 2, 10, 9, reinterpret_cast<char *>(b3));

    p1.set_start_flag();
    p2.set_continuation_flag();
    p2.set_resend_flag();
    p3.set_termination_flag();

    uint8_t p1s[p1.required_size()];
    uint8_t p2s[p2.required_size()];
    uint8_t p3s[p3.required_size()];

    static auto p2sp = &p2s;

    p1.serialize(p1s);
    p2.serialize(p2s);
    p3.serialize(p3s);

    static auto handler = rf_proto_handler(
            [](uint8_t a, uint8_t b, rf_proto_handler *handler) {
                TEST_ASSERT_EQUAL_MESSAGE(1, a, "sequence == 1");
                TEST_ASSERT_EQUAL_MESSAGE(1, b, "offset == 1");
                std::cout << "resend requested" << std::endl;
                handler->parse(*p2sp, p1.required_size());
            },
            [](rf_dmx_t *dmx, rf_proto_handler *handler) {
                TEST_ASSERT_FALSE_MESSAGE(dmx->has_resend_flag(), "has resend flag");
                TEST_ASSERT_TRUE_MESSAGE(dmx->size == 55, "size == 55");
                TEST_ASSERT_TRUE_MESSAGE(dmx->universe == 10, "universe == 10");
                TEST_ASSERT_TRUE_MESSAGE(dmx->device_id == 14, "device_id == 14");
                TEST_ASSERT_TRUE_MESSAGE(dmx->compression_type == DIFF, "compression_type == DIFF");
                TEST_ASSERT_EQUAL_UINT8_ARRAY_MESSAGE(combined, dmx->data, dmx->size, "combined == dmx->data");
                dump_hex(reinterpret_cast<uint8_t *>(dmx->data), dmx->size);
                callback_called = true;
                TEST_PASS_MESSAGE("callback executed");
            },
            [](rf_checkin_t *checkin, rf_proto_handler *handler) { TEST_FAIL_MESSAGE("Handled a checkin"); },
            nullptr,
            [](rf_request_t *, rf_proto_handler *) {},
            [](int line, rf_proto_handler *handler) { TEST_FAIL_MESSAGE("Panicked"); },
            10,
            14
    );

    handler.parse(p1s, p1.required_size());
//    handler.parse(p2s, p2.required_size());
    handler.parse(p3s, p3.required_size());
    if (!callback_called) {
        TEST_FAIL_MESSAGE("callback has never been called");
    }
}

void test__rf_dmx_t__multi_packet_missing_terminate() {
    static auto callback_called = false;
    // Note, this test completing successfully relies on a timing aspect which has not be included in the rest of the
    // tests.
    std::cout << std::endl << std::endl << "## BEGIN TESTING " << __func__ << std::endl << std::endl;

    uint8_t b1[rf_dmx_t::PAYLOAD_SIZE] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13,
                                          0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x20, 0x21, 0x22, 0x23};
    uint8_t b2[rf_dmx_t::PAYLOAD_SIZE] = {0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36,
                                          0x37, 0x38, 0x39, 0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46};
    uint8_t b3[rf_dmx_t::PAYLOAD_SIZE] = {0x47, 0x48, 0x49, 0x50, 0x51, 0x52, 0x53, 0x5, 0x55};

    static uint8_t combined[rf_dmx_t::PAYLOAD_SIZE + rf_dmx_t::PAYLOAD_SIZE + 9];
    std::copy(b1, b1 + rf_dmx_t::PAYLOAD_SIZE, combined);
    std::copy(b2, b2 + rf_dmx_t::PAYLOAD_SIZE, combined + rf_dmx_t::PAYLOAD_SIZE);
    std::copy(b3, b3 + 9, combined + (2 * rf_dmx_t::PAYLOAD_SIZE));

    static auto p1 = rf_dmx_t(14, 0, 0, DIFF, 0, 10, rf_dmx_t::PAYLOAD_SIZE, reinterpret_cast<char *>(b1));
    static auto p2 = rf_dmx_t(14, 1, 0, DIFF, 1, 10, rf_dmx_t::PAYLOAD_SIZE, reinterpret_cast<char *>(b2));
    static auto p3 = rf_dmx_t(14, 2, 0, DIFF, 2, 10, 9, reinterpret_cast<char *>(b3));

    p1.set_start_flag();
    p2.set_continuation_flag();
    p3.set_termination_flag();
    p3.set_resend_flag();

    uint8_t p1s[p1.required_size()];
    uint8_t p2s[p2.required_size()];
    uint8_t p3s[p3.required_size()];

    static auto p3sp = &p3s;

    p1.serialize(p1s);
    p2.serialize(p2s);
    p3.serialize(p3s);

    static auto handler = rf_proto_handler(
            [](uint8_t a, uint8_t b, rf_proto_handler *handler) {
                TEST_ASSERT_EQUAL_MESSAGE(a, 2, "sequence == 2");
                TEST_ASSERT_EQUAL_MESSAGE(b, 2, "offset == 2");
                std::cout << "resend requested" << std::endl;
                handler->parse(*p3sp, p1.required_size());
            },
            [](rf_dmx_t *dmx, rf_proto_handler *handler) {
                TEST_ASSERT_FALSE_MESSAGE(dmx->has_resend_flag(), "has resend flag");
                TEST_ASSERT_TRUE_MESSAGE(dmx->size == 55, "size == 55");
                TEST_ASSERT_TRUE_MESSAGE(dmx->universe == 10, "universe == 10");
                TEST_ASSERT_TRUE_MESSAGE(dmx->device_id == 14, "device_id == 14");
                TEST_ASSERT_TRUE_MESSAGE(dmx->compression_type == DIFF, "compression_type == DIFF");
                TEST_ASSERT_EQUAL_UINT8_ARRAY_MESSAGE(combined, dmx->data, dmx->size, "combined == dmx->data");
                dump_hex(reinterpret_cast<uint8_t *>(dmx->data), dmx->size);
                callback_called = true;
                TEST_PASS_MESSAGE("callback executed");
            },
            [](rf_checkin_t *checkin, rf_proto_handler *handler) { TEST_FAIL_MESSAGE("Handled a checkin"); },
            nullptr,
            [](rf_request_t *, rf_proto_handler *) {},
            [](int line, rf_proto_handler *handler) { TEST_FAIL_MESSAGE("Panicked"); },
            10,
            14
    );

    handler.parse(p1s, p1.required_size());
    handler.parse(p2s, p2.required_size());
    handler.tick();
    std::this_thread::sleep_for(std::chrono::milliseconds(200));
    handler.tick();
    if (!callback_called) {
        TEST_FAIL_MESSAGE("callback has never been called");
    }
}

void test__rf_dmx_t__multi_packet_missing_continue_with_missed_redelivery() {
    static auto callback_called = false;
    std::cout << std::endl << std::endl << "## BEGIN TESTING " << __func__ << std::endl << std::endl;

    uint8_t b1[rf_dmx_t::PAYLOAD_SIZE] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13,
                                          0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x20, 0x21, 0x22, 0x23};
    uint8_t b2[rf_dmx_t::PAYLOAD_SIZE] = {0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36,
                                          0x37, 0x38, 0x39, 0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46};
    uint8_t b3[rf_dmx_t::PAYLOAD_SIZE] = {0x47, 0x48, 0x49, 0x50, 0x51, 0x52, 0x53, 0x5, 0x55};

    static uint8_t combined[rf_dmx_t::PAYLOAD_SIZE + rf_dmx_t::PAYLOAD_SIZE + 9];
    std::copy(b1, b1 + rf_dmx_t::PAYLOAD_SIZE, combined);
    std::copy(b2, b2 + rf_dmx_t::PAYLOAD_SIZE, combined + rf_dmx_t::PAYLOAD_SIZE);
    std::copy(b3, b3 + 9, combined + (2 * rf_dmx_t::PAYLOAD_SIZE));

    static auto p1 = rf_dmx_t(14, 0, 0, DIFF, 0, 10, rf_dmx_t::PAYLOAD_SIZE, reinterpret_cast<char *>(b1));
    static auto p2 = rf_dmx_t(14, 1, 0, DIFF, 1, 10, rf_dmx_t::PAYLOAD_SIZE, reinterpret_cast<char *>(b2));
    static auto p3 = rf_dmx_t(14, 2, 0, DIFF, 2, 10, 9, reinterpret_cast<char *>(b3));

    p1.set_start_flag();
    p2.set_continuation_flag();
    p2.set_resend_flag();
    p3.set_termination_flag();

    uint8_t p1s[p1.required_size()];
    uint8_t p2s[p2.required_size()];
    uint8_t p3s[p3.required_size()];

    static auto p2sp = &p2s;

    p1.serialize(p1s);
    p2.serialize(p2s);
    p3.serialize(p3s);

    static bool first_request = true;

    static auto handler = rf_proto_handler(
            [](uint8_t a, uint8_t b, rf_proto_handler *handler) {
                if (first_request) {
                    std::cout << "first send requested, ignoring" << std::endl;
                    first_request = false;
                } else {
                    TEST_ASSERT_EQUAL_MESSAGE(a, 1, "sequence == 1");
                    TEST_ASSERT_EQUAL_MESSAGE(b, 1, "offset == 1");
                    std::cout << "resend requested" << std::endl;
                    handler->parse(*p2sp, p1.required_size());
                }
            },
            [](rf_dmx_t *dmx, rf_proto_handler *handler) {
                TEST_ASSERT_FALSE_MESSAGE(dmx->has_resend_flag(), "has resend flag");
                TEST_ASSERT_TRUE_MESSAGE(dmx->size == 55, "size == 55");
                TEST_ASSERT_TRUE_MESSAGE(dmx->universe == 10, "universe == 10");
                TEST_ASSERT_TRUE_MESSAGE(dmx->device_id == 14, "device_id == 14");
                TEST_ASSERT_TRUE_MESSAGE(dmx->compression_type == DIFF, "compression_type == DIFF");
                TEST_ASSERT_EQUAL_UINT8_ARRAY_MESSAGE(combined, dmx->data, dmx->size, "combined == dmx->data");
                dump_hex(reinterpret_cast<uint8_t *>(dmx->data), dmx->size);
                callback_called = true;
                TEST_PASS_MESSAGE("callback executed");
            },
            [](rf_checkin_t *checkin, rf_proto_handler *handler) { TEST_FAIL_MESSAGE("Handled a checkin"); },
            nullptr,
            [](rf_request_t *, rf_proto_handler *) {},
            [](int line, rf_proto_handler *handler) { TEST_FAIL_MESSAGE("Panicked"); },
            10,
            14
    );

    handler.parse(p1s, p1.required_size());
    handler.parse(p3s, p3.required_size());
    handler.tick();
    std::this_thread::sleep_for(std::chrono::milliseconds(200));
    handler.tick();
    if (!callback_called) {
        TEST_FAIL_MESSAGE("callback has never been called");
    }
}

int main() {
    UNITY_BEGIN();
    RUN_TEST(test__rf_checkin_t);
    RUN_TEST(test__rf_dmx_t__single_packet);
    RUN_TEST(test__rf_dmx_t__multi_packet_normal_delivery);
    RUN_TEST(test__rf_dmx_t__multi_packet_missing_start);
    RUN_TEST(test__rf_dmx_t__multi_packet_missing_continuation);
    RUN_TEST(test__rf_dmx_t__multi_packet_missing_terminate);
    RUN_TEST(test__rf_dmx_t__multi_packet_missing_continue_with_missed_redelivery);
    int init_result = init();
    int node_store_result = test_node_store();
    int array_map = test_array_map();

    if(init_result != 0) return init_result;
    if(node_store_result != 0) return node_store_result;
    if(array_map != 0) return array_map;
    return UNITY_END();
}