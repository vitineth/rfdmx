//
// Created by ryan on 3/20/23.
//
#include "test_simulator.h"
#include "unity.h"
#include <iostream>
#include <cstdint>
#include <utility>
#include <vector>
#include <queue>
#include <functional>
#include <random>
#include <chrono>
#include <thread>
#include <mutex>
#include <fstream>
#include <iomanip>
#include "protocol.h"

#define min(a, b) a < b ? a : b

// Packet structure
struct Packet {
    uint8_t *data;
    size_t length;
    uint64_t timestamp;
    bool from_master;
};


std::random_device rd_;

class RadioSimulator {
    uint64_t drop_count_ = 0;
    uint64_t normal_count_ = 0;
    double loss_probability_;
    double reorder_probability_;
    uint64_t delay_mean_;
    uint64_t delay_std_dev_;
    std::mt19937_64 gen_;
    std::normal_distribution<double> dis_;
    std::uniform_real_distribution<double> drop_;
    std::queue<Packet> packet_queue_;
    std::queue<Packet> reorder_queue_;
    std::function<void(const uint8_t *, size_t, bool)> receiver_;
public:
    RadioSimulator(double loss_probability, double reorder_probability, uint64_t delay_mean, uint64_t delay_std_dev,
                   std::function<void(const uint8_t *, size_t, bool)> receiver)
            : loss_probability_(loss_probability), reorder_probability_(reorder_probability),
              delay_mean_(delay_mean), delay_std_dev_(delay_std_dev), receiver_(std::move(receiver)), gen_(rd_()),
              dis_(0.0, 1.0), drop_(0, 1) {}

    void transmit_packet(const Packet &packet) {
        auto delay = static_cast<long long>(delay_mean_ + dis_(gen_) * delay_std_dev_);
        std::cout << "  [delay] delay is " << delay << "us" << std::endl;
        std::this_thread::sleep_for(std::chrono::microseconds(delay));

        if (drop_(gen_) < loss_probability_) {
            std::cout << "  [manip] packet dropped" << std::endl;
            drop_count_++;
            return;
        } else {
            normal_count_++;
        }

        if (reorder_probability_ > 0 && dis_(gen_) < reorder_probability_) {
            std::cout << "  [manip] packet reordered" << std::endl;
            reorder_queue_.push(packet);
        } else {
            std::cout << "  [manip] packet delivered normally" << std::endl;
            packet_queue_.push(packet);
        }
        process_queue();
    }

    void process_queue() {
        while (!reorder_queue_.empty() || !packet_queue_.empty()) {
            if (!reorder_queue_.empty()) {
                auto packet = reorder_queue_.front();
                reorder_queue_.pop();
                receiver_(packet.data, packet.length, packet.from_master);
            } else {
                auto packet = packet_queue_.front();
                packet_queue_.pop();
                receiver_(packet.data, packet.length, packet.from_master);
            }
        }
    }

    void finish() {
        std::cout << std::endl << "Radio Finish" << std::endl;
        std::cout << "  Sent    : " << (drop_count_ + normal_count_) << std::endl;
        std::cout << "  Dropped : " << drop_count_ << std::endl;
        std::cout << "  Normal  : " << normal_count_ << std::endl;
        std::cout << "  Drop %  : " << ((drop_count_ / (double) (normal_count_ + drop_count_)) * 100) << std::fixed
                  << std::setprecision(2) << "%"
                  << " (target is "
                  << (loss_probability_ * 100)
                  << std::setprecision(2)
                  << "%)"
                  << std::endl;
    }
};

uint8_t last_sequence;
std::vector<std::vector<std::uint8_t>> cache;

static rf_proto_handler *master_handler = nullptr;
static rf_proto_handler *receiver_handler = nullptr;

uint8_t received = 0;

void handle(const uint8_t *buffer, size_t size, bool from_master) {
    if (from_master) {
        std::cout << "master --> receiver" << std::endl;
        receiver_handler->parse(const_cast<uint8_t *>(buffer), size);
    } else {
        std::cout << "master <-- receiver" << std::endl;
        master_handler->parse(const_cast<uint8_t *>(buffer), size);
    }
}

int init() {
    std::cout << "If you want to profile, attach now - otherwise just hit enter" << std::endl;
    std::cin.get();

    std::ifstream file("/home/ryan/dev/scratch/sacn-to-artnet/raw-buffer-dump.bin",
                       std::ios::binary); // open binary file in binary mode
    if (!file.is_open()) {
        std::cerr << "Error opening file\n";
        return 1;
    }

    uint64_t read = 0;
    std::vector<std::uint8_t> buffer(512); // create a buffer to hold 512 bytes at a time
    std::vector<std::vector<std::uint8_t>> arrays; // create a vector of vectors to hold the arrays

    while (file.read((char *) buffer.data(), buffer.size())) { // read 512 bytes at a time
        arrays.push_back(buffer); // add the buffer to the vector of arrays
        buffer.resize(512); // clear the buffer for the next iteration
        read += 512;
        std::cout << read << " bytes" << std::endl;
    }
    for (auto &val: arrays[0]) {
        std::cout << std::hex << std::setfill('0') << std::setw(2) << static_cast<int>(val) << " ";
    }
    std::cout << std::endl;

    arrays.resize(10);
    file.close(); // close the file

    std::cout << "configuring radio" << std::endl;

    static auto radio = RadioSimulator(0.05, 0, 10, 3, handle);
    static uint8_t master_sequence = 1;
    static uint8_t receiver_sequence = 1;


    master_handler = new rf_proto_handler(
            [](uint8_t sequence, uint8_t offset, rf_proto_handler *self) {
                TEST_FAIL_MESSAGE("A resend was requested on the master side which should not be possible");
            },
            [](rf_dmx_t *, rf_proto_handler *) {},
            [](rf_checkin_t *, rf_proto_handler *) {},
            nullptr,
            [](rf_request_t *request, rf_proto_handler *self) {
                if (request->sequence + request->offset > last_sequence) {
                    std::cout << std::fixed << "[0] " << unsigned(request->sequence) << "." << unsigned(request->offset)
                              << " > " << unsigned(last_sequence) << "(" << cache.size() << ")" << std::endl;
                    if (cache.size() <= request->offset) return;
                    auto b = cache.at(request->offset);
                    // Need to set the resent flag on these ugh
                    b[3] |= 0b00010000;
                    auto p = Packet();
                    p.data = new uint8_t[b.size()];
                    memcpy(p.data, b.data(), b.size());
                    p.length = b.size();
                    p.from_master = true;
                    p.timestamp = std::chrono::system_clock::now().time_since_epoch().count();
                    radio.transmit_packet(p);
                }
            },
            [](int, rf_proto_handler *) {
                TEST_FAIL_MESSAGE("The system panicked!");
            },
            0,
            0
    );

    std::cout << "master made" << std::endl;

    receiver_handler = new rf_proto_handler(
            [](uint8_t sequence, uint8_t offset, rf_proto_handler *self) {
                std::cout << "[1] " << unsigned(sequence) << "." << unsigned(offset) << " request resend" << std::endl;
                auto request = rf_request_t(0, receiver_sequence++, 0, offset, sequence);
                auto buffer = new uint8_t[request.required_size()];
                request.serialize(buffer);
                auto p = Packet();
                p.data = buffer;
                p.length = request.required_size();
                p.from_master = false;
                p.timestamp = std::chrono::system_clock::now().time_since_epoch().count();
                radio.transmit_packet(p);
            },
            [](rf_dmx_t *, rf_proto_handler *) {
                std::cout << "dmx received" << std::endl;
                received++;
            },
            [](rf_checkin_t *, rf_proto_handler *) {},
            nullptr,
            [](rf_request_t *request, rf_proto_handler *self) {
                TEST_FAIL_MESSAGE("A request was received by the client - this shouldn't ever happen!");
            },
            [](int, rf_proto_handler *) {
                TEST_FAIL_MESSAGE("The system panicked!");
            },
            0,
            0
    );

    std::cout << "configured" << std::endl;

    for (int i = 0; i < arrays.size(); ++i) {
        std::cout << "################# BEGIN TRANSMIT : " << i << " sending" << std::endl;
        auto entry = arrays.at(i);
        auto raw = entry.data();
        auto packet_count = entry.size() / rf_dmx_t::PAYLOAD_SIZE;

        std::cout << "  splitting into " << packet_count << " packets" << std::endl;
        // Then run through the data and transmit
        size_t pointer = 0;
        cache.resize(0);
        for (size_t j = 0; j < packet_count; ++j) {
            size_t this_size = min(rf_dmx_t::PAYLOAD_SIZE, entry.size() - pointer);
            auto new_packet = rf_dmx_t(
                    0,
                    master_sequence++,
                    0,
                    FULL,
                    j,
                    0,
                    this_size,
                    reinterpret_cast<char *>(raw + pointer)
            );
            pointer += this_size;

            if (j == 0) {
                new_packet.set_start_flag();
            }
            if (j == packet_count - 1) {
                new_packet.set_termination_flag();
            }
            if (j != 0 && j != packet_count - 1) {
                new_packet.set_continuation_flag();
            }

            size_t required_size = new_packet.required_size();
            auto send_buffer = new uint8_t[required_size];
            new_packet.serialize(send_buffer);
            auto v = new std::vector<uint8_t>(send_buffer, send_buffer + required_size);
            cache.push_back(*v);

            auto p = Packet();
            p.data = send_buffer;
            p.length = required_size;
            p.from_master = true;
            p.timestamp = std::chrono::system_clock::now().time_since_epoch().count();

            std::cout << std::endl << "transmit " << unsigned(new_packet.sequence) << std::endl;
            radio.transmit_packet(p);
            std::this_thread::sleep_for(std::chrono::milliseconds(rand() % 300));
        }
    }

    radio.finish();
    if (arrays.size() == received) {
        std::cout << "SUCCESS [" << arrays.size() << " sent, " << unsigned(received) << " received]" << std::endl;
        return 0;
    } else {
        std::cout << "FAILURE [" << arrays.size() << " sent, " << unsigned(received) << " received]" << std::endl;
        return 1;
    }
}