//
// Created by Ryan on 20/04/2023.
//

#include <vector>
#include <set>
#include <map>
#include <iostream>
#include <thread>
#include "node_store.h"

int test_node_store(){
    auto* store= new node_storage();
    std::map<uint8_t, uint8_t> device_mapping;

    uint8_t MAX_VALUES = 255;

    for (int i = 0; i < 10000; ++i) {
        auto device_id= (uint8_t )(arc4random() % MAX_VALUES);
        auto universe  =(uint8_t)( arc4random() % MAX_VALUES);

        store->checkin(device_id, universe);
        device_mapping[device_id] = universe;
    }

    size_t output;
    uint8_t* results = store->get_universes(&output);

    std::set<uint8_t> generated;
    std::set<uint8_t> mapped;

    for (const auto& [key, value] : device_mapping)
        mapped.insert((uint8_t)value);
    for (int i = 0; i < output; ++i) {
        generated.insert(results[i]);
    }

    if(mapped == generated){
        std::cout << "Node store managed universes successfully" << std::endl;
    }else{
        std::cout << "Failure - universes did not match" << std::endl;
        std::for_each(mapped.begin(), mapped.end(), [](uint8_t universe){
            std::cout << unsigned(universe) << ", ";
        });
        std::cout <<std::endl;

        std::for_each(generated.begin(), generated.end(), [](uint8_t universe){
            std::cout << unsigned(universe) << ", ";
        });
        std::cout <<std::endl;
        return 1;
    }

    // Then wait and confirm that pruning all entries works
    std::this_thread::sleep_for(std::chrono::seconds(1));
    store->prune(10);

    if (store->length != 0){
        std::cout << "Fail - prune did not remove entries" << std::endl;
        std::cout << "   left with " << unsigned(store->length) << " entries" << std::endl;
        return 1;
    }else{
        std::cout << "Pass - prune worked as expected" << std::endl;
    }

    delete store;
    return 0;
}