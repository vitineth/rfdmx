//
// Created by Ryan on 27/04/2023.
//

#ifndef NODE_ARRAY_MAP_H
#define NODE_ARRAY_MAP_H

#ifdef __AVR_ARCH__
#include <Arduino.h>
#else
#include <cstdint>
#include <cstring>
#define size_t std::size_t
#endif

class array_map {
protected:
    /**
     * The set of keys within this store, this should be unique as the same
     * value should replace the previous one
     */
    uint8_t* key_set{};
    /**
     * The set of actual values, the index within this array matches that
     * within used and key
     */
    void** value_set;
    /**
     * The set indicating if cells are actually used in value_set and key_set
     * which provides distinction between nullptr and a missing value
     */
    bool* used_set;
    /**
     * The amount of values present in this array map
     */
    size_t used = 0;
    /**
     * The current capacity of the arrays
     */
    size_t capacity = 0;
public:
    /**
     * Constructs a new arraymap of the given initial storage side. Resizing is
     * an expensive operation so where possible this should try to be as
     * appropriate as possible
     * @param capacity  initial allocation size
     */
    explicit array_map(size_t capacity);

    /**
     * Deletes the allocated stores
     */
    virtual ~array_map();

protected:
    /**
     * Expands the array map storage, starting with a scale factor of 1.1 but
     * automatically increasing until the size will actually change
     */
    void expand();
public:
    /**
     * Sets the given key value pairing in the array map
     * @param key the key to set
     * @param value the associated value
     */
    void set(uint8_t key, void* value);
    /**
     * Attempts to retrieve the value associated with the key. If the key
     * is not found, then it returns nullptr
     * @param key the value to lookup
     * @return the found value or nullptr
     */
    void* get(uint8_t key);
    /**
     * Equivalent to get(uint8_t) but with explicit handling for the case
     * where nullptrs are stored in the map itself. You can read directly
     * whether a value was found or not, regardless of the value. Found can
     * be nullptr as well in which case it functions exactly like get(uint8_t)
     * @param key the key to look up
     * @param found the pointer to place the found indicator in or nullptr
     * @return the pointer if found or nullptr
     */
    void* get(uint8_t key, bool* found);

    /**
     * Returns if the key is present in the map
     * @param key the key to lookup
     * @return if the key can be located
     */
    bool is_present(uint8_t key);
};


#endif //NODE_ARRAY_MAP_H
