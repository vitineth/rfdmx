//
// Created by Ryan on 27/04/2023.
//

#include "array_map.h"

void array_map::expand() {
    float scale_factor = 1.1;

    // TODO: is size a concern?
    while ((uint8_t) (this->used * scale_factor) == this->used) scale_factor += 0.1;
    size_t new_size = scale_factor * this->used;

    uint8_t * new_key_set = new uint8_t [new_size];
    void** new_value_set = new void*[new_size];
    bool* new_used_set = new bool[new_size];

    memcpy(new_key_set, this->key_set, this->used * sizeof(uint8_t));
    memcpy(new_value_set, this->value_set, this->used * sizeof(void*));
    memcpy(new_used_set, this->used_set, this->used * sizeof(bool));

    delete this->key_set;
    delete this->value_set;
    delete this->used_set;

    this->key_set = new_key_set;
    this->value_set = new_value_set;
    this->used_set = new_used_set;
    this->capacity = new_size;
}

void array_map::set(uint8_t key, void *value) {
    for (int i = 0; i < this->used; ++i) {
        if (this->key_set[i] == key && this->used_set[i] == true){
            this->value_set[i] = value;
            return;
        }
    }

    if (this->used == this->capacity){
        this->expand();
    }

    this->key_set[this->used] = key;
    this->value_set[this->used] = value;
    this->used_set[this->used] = true;
    this->used++;
}

array_map::array_map(size_t capacity) : capacity(capacity) {
    this->capacity = capacity;
    this->key_set = new uint8_t [capacity];
    this->value_set = new void*[capacity];
    this->used_set = new bool [capacity];
    this->used = 0;
}

void *array_map::get(uint8_t key) {
    return this->get(key, nullptr);
}

void *array_map::get(uint8_t key, bool *found) {
    for (int i = 0; i < this->used; ++i) {
        if (this->key_set[i] == key && this->used_set[i] == true){
            if (found != nullptr){
                *found = true;
            }
            return this->value_set[i];
        }
    }

    if (found != nullptr){
        *found = false;
    }

    return nullptr;
}

bool array_map::is_present(uint8_t key) {
    bool found = false;
    this->get(key, &found);
    return found;
}

array_map::~array_map() {
    delete this->value_set;
    delete this->used_set;
    delete this->key_set;
}

