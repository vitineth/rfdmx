//
// Created by ryan on 3/1/23.
//

#include "dmx_store.h"
#include "logging.h"

uint8_t dmx_store::used() const {
    if (this->used_frames > 0) return this->used_frames - 1;
    return 0;
}

uint8_t dmx_store::value(uint8_t frame, uint16_t index) const {
    if (frame >= this->used_frames) return 0;
    return this->frames[frame][index];
}

void dmx_store::assign(const uint8_t values[512]) {
//    for (int i = 0; i < 512; ++i) {
//        this->frames[this->active_pointer][i] = values[i];
//    }
    memcpy(this->frames[this->active_pointer], values, 512);
    this->last_written_frame = this->active_pointer;

    this->active_pointer++;
    this->used_frames++;

    if (this->used_frames > dmx_store::MAX_FRAMES) this->used_frames = dmx_store::MAX_FRAMES;
    if (this->active_pointer >= dmx_store::MAX_FRAMES) this->active_pointer = 0;
}

bool dmx_store::changed(uint8_t value, uint16_t index) const {
    // * = Because we are pushing the data into the store _early_ to save memory, it means that
    //      the data being sent will already be in this store by the time it needs to be compressed
    //      therefore we need to ignore the most recently inserted entry in the store to prevent
    //      every single packet being compressed in full

    if (index >= 512) return false;
    for (int i = 0; i < this->used_frames; ++i) {
        if (i == this->last_written_frame) continue; // *
        if (this->frames[i][index] != value) {
            return true;
        }
    }
    return false;
}

void dmx_store::export_last(uint8_t *target) const {
    if (this->used_frames == 0) return;

    uint8_t target_address = 0;
    if (this->active_pointer == 0) target_address = dmx_store::MAX_FRAMES - 1;
    else target_address = this->active_pointer - 1;

    memcpy(target, this->frames[target_address], 512);
}