//
// Created by ryan on 3/1/23.
//

#ifndef NODE_DMX_STORE_H
#define NODE_DMX_STORE_H

#ifdef __AVR_ARCH__
#include <Arduino.h>
#else
#include <cstdint>
#include <cstring>
#endif

/**
 * Stores sequential DMX frames with support for comparing data and storing frames. Designed for use with a single
 * universe of data, each new universe should allocate their own store.
 */
struct dmx_store {
protected:
    /**
     * The number of frames that will be included in this store before entries begin getting overridden
     */
    const static uint8_t MAX_FRAMES = 2;
    /**
     * The storage for full DMX frames, indexed first by the frame number then the dmx index
     */
    uint8_t frames[MAX_FRAMES][512]{};
    /**
     * The number of frames which are stored within this struct
     */
    uint8_t used_frames = 0;
    /**
     * The next free position in the {@link #frames} array
     */
    uint8_t active_pointer = 0;
    /**
     * Contans the index to the last written frame in the {@link #frames} array which can be ignored on change
     * processing. If nothing has been written yet then it should be set to UINT8_MAX.
     */
    uint8_t last_written_frame = UINT8_MAX;
public:
    /**
     * Returns the number of entries in this store that have been used. Maximum value will be {@link #MAX_FRAMES}
     * @return the used entries
     */
    [[nodiscard]] uint8_t used() const;

    /**
     * Returns the value of the data of the given frame at the given index. You should confirm that a frame is available
     * for reading via {@link #used()} before calling.
     *
     * @param frame the frame in which to look up the data
     * @param index the index within the frame to return
     * @return the data, or 0 if the frame is not present
     */
    [[nodiscard]] uint8_t value(uint8_t frame, uint16_t index) const;

    /**
     * Copies the provided data into the next available frame store. This will perform a copy of the data so any type
     * of pointer is valid so long as 512 values can be read
     * @param values the values to copy into the next available frame
     */
    void assign(const uint8_t values[512]);

    /**
     * Returns if the provided value at the given index has changed in any of the stored frames within the store
     * @param value the value to compare
     * @param index the index of that value in the orginal data
     * @return if this value has changed in any stored frame, excluding {@link #last_written_frame}
     */
    [[nodiscard]] bool changed (uint8_t value, uint16_t index) const;

    /**
     * Copies the last written frame out of the store and into the provided target address. The target location should
     * be able to accommodate a full 512 byte write and no checks will be done to confirm that
     * @param target the location into which the data should be copied
     */
    void export_last(uint8_t *target) const;
};

#endif //NODE_DMX_STORE_H
