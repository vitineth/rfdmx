//
// Created by ryan on 3/6/23.
//

#include "logging.h"

//#define DISABLE_MASTER_SERIAL 1

#ifdef __AVR_ARCH__
#include "Arduino.h"
#define PRINT Serial.print
#define PRINTLN Serial.println
#else
#include <iostream>
#include <sstream>
#include <iomanip>

#define F(v) v
#define PRINT(v) std::cout << v
#define PRINTLN(v) std::cout << v << std::endl
#endif

void test_for_blocking(){
#ifndef _IS_RPI
#ifdef __AVR_ARCH__
    Serial.flush ();
  while ((UCSR0A & _BV (TXC0)) == 0)
    {}
#endif
#endif
}

#ifdef __AVR_ARCH__
void log_print(const __FlashStringHelper *ifsh) {
#ifdef RFD_MASTER
#ifndef DISABLE_MASTER_SERIAL
    PRINT(ifsh);
#endif // DISABLE_MASTER_SERIAL
#else
#ifndef DMX_OUTPUT_ENABLED
    PRINT(ifsh);
#endif
#endif

#ifdef TRY_BLOCKING_SERIAL
    test_for_blocking();
#endif
}
#endif

void log_print(const String &s) {
#ifdef RFD_MASTER
#ifndef DISABLE_MASTER_SERIAL
    PRINT(s);
#endif // DISABLE_MASTER_SERIAL
#else
#ifndef DMX_OUTPUT_ENABLED
    PRINT(s);
#endif
#endif

#ifdef TRY_BLOCKING_SERIAL
    test_for_blocking();
#endif
}

void log_print(const char str[]) {
#ifdef RFD_MASTER
#ifndef DISABLE_MASTER_SERIAL
    PRINT(str);
#endif // DISABLE_MASTER_SERIAL
#else
#ifndef DMX_OUTPUT_ENABLED
    PRINT(str);
#endif
#endif

#ifdef TRY_BLOCKING_SERIAL
    test_for_blocking();
#endif
}

void log_print(char c) {
#ifdef RFD_MASTER
#ifndef DISABLE_MASTER_SERIAL
    PRINT(c);
#endif // DISABLE_MASTER_SERIAL
#else
#ifndef DMX_OUTPUT_ENABLED
    PRINT(c);
#endif
#endif

#ifdef TRY_BLOCKING_SERIAL
    test_for_blocking();
#endif
}

void log_print(unsigned char b, int base) {
#ifdef RFD_MASTER
#ifndef DISABLE_MASTER_SERIAL
#ifdef __AVR_ARCH__
    PRINT(b, base);
#else // __AVR_ARCH__
    PRINT(unsigned(b) << std::setbase(base));
#endif // __AVR_ARCH__
#endif // DISABLE_MASTER_SERIAL
#else // RFD_MASTER
#ifndef DMX_OUTPUT_ENABLED
#ifdef __AVR_ARCH__
    PRINT(b, base);
#else // __AVR_ARCH__
    PRINT(unsigned(b) << std::setbase(base));
#endif // __AVR_ARCH__
#endif // DMX_OUTPUT_ENABLED
#endif // RFD_MASTER

#ifdef TRY_BLOCKING_SERIAL
    test_for_blocking();
#endif
}

void log_print(int n, int base) {
#ifdef RFD_MASTER
#ifndef DISABLE_MASTER_SERIAL
#ifdef __AVR_ARCH__
    PRINT(n, base);
#else // __AVR_ARCH__
    PRINT(n << std::setbase(base));
#endif // __AVR_ARCH__
#endif // DISABLE_MASTER_SERIAL
#else
#ifndef DMX_OUTPUT_ENABLED
#ifdef __AVR_ARCH__
    PRINT(n, base);
#else // __AVR_ARCH__
    PRINT(n << std::setbase(base));
#endif // __AVR_ARCH__
#endif
#endif

#ifdef TRY_BLOCKING_SERIAL
    test_for_blocking();
#endif
}

void log_print(unsigned int n, int base) {
#ifdef RFD_MASTER
#ifndef DISABLE_MASTER_SERIAL
#ifdef __AVR_ARCH__
    PRINT(n, base);
#else // __AVR_ARCH__
    PRINT(n << std::setbase(base));
#endif // __AVR_ARCH__
#endif // DISABLE_MASTER_SERIAL
#else
#ifndef DMX_OUTPUT_ENABLED
#ifdef __AVR_ARCH__
    PRINT(n, base);
#else // __AVR_ARCH__
    PRINT(n << std::setbase(base));
#endif // __AVR_ARCH__
#endif
#endif

#ifdef TRY_BLOCKING_SERIAL
    test_for_blocking();
#endif
}

void log_print(long n, int base) {
#ifdef RFD_MASTER
#ifndef DISABLE_MASTER_SERIAL
#ifdef __AVR_ARCH__
    PRINT(n, base);
#else // __AVR_ARCH__
    PRINT(n << std::setbase(base));
#endif // __AVR_ARCH__
#endif // DISABLE_MASTER_SERIAL
#else
#ifndef DMX_OUTPUT_ENABLED
#ifdef __AVR_ARCH__
    PRINT(n, base);
#else // __AVR_ARCH__
    PRINT(n << std::setbase(base));
#endif // __AVR_ARCH__
#endif
#endif

#ifdef TRY_BLOCKING_SERIAL
    test_for_blocking();
#endif
}

void log_print(unsigned long n, int base) {
#ifdef RFD_MASTER
#ifndef DISABLE_MASTER_SERIAL
#ifdef __AVR_ARCH__
    PRINT(n, base);
#else // __AVR_ARCH__
    PRINT(n << std::setbase(base));
#endif // __AVR_ARCH__
#endif // DISABLE_MASTER_SERIAL
#else
#ifndef DMX_OUTPUT_ENABLED
#ifdef __AVR_ARCH__
    PRINT(n, base);
#else // __AVR_ARCH__
    PRINT(n << std::setbase(base));
#endif // __AVR_ARCH__
#endif
#endif

#ifdef TRY_BLOCKING_SERIAL
    test_for_blocking();
#endif
}

void log_print(double n, int digits) {
#ifdef RFD_MASTER
#ifndef DISABLE_MASTER_SERIAL
#ifdef __AVR_ARCH__
    PRINT(n, digits);
#else // __AVR_ARCH__
    PRINT(std::setprecision(digits) << std::fixed << n);
#endif // __AVR_ARCH__
#endif // DISABLE_MASTER_SERIAL
#else // RFD_MASTER
#ifndef DMX_OUTPUT_ENABLED
#ifdef __AVR_ARCH__
    PRINT(n, digits);
#else // __AVR_ARCH__
    PRINT(std::setprecision(digits) << std::fixed << n);
#endif // __AVR_ARCH__
#endif // DMX_OUTPUT_ENABLED
#endif // RFD_MASTER

#ifdef TRY_BLOCKING_SERIAL
    test_for_blocking();
#endif
}

#ifdef __AVR_ARCH__
void log_println(const __FlashStringHelper *ifsh) {
#ifdef RFD_MASTER
#ifndef DISABLE_MASTER_SERIAL
    PRINTLN(ifsh);
#endif // DISABLE_MASTER_SERIAL
#else
#ifndef DMX_OUTPUT_ENABLED
    PRINTLN(ifsh);
#endif
#endif

#ifdef TRY_BLOCKING_SERIAL
    test_for_blocking();
#endif
}
#endif

void log_println(const String &s) {
#ifdef RFD_MASTER
#ifndef DISABLE_MASTER_SERIAL
    PRINTLN(s);
#endif // DISABLE_MASTER_SERIAL
#else
#ifndef DMX_OUTPUT_ENABLED
    PRINTLN(s);
#endif
#endif

#ifdef TRY_BLOCKING_SERIAL
    test_for_blocking();
#endif
}

void log_println(const char str[]) {
#ifdef RFD_MASTER
#ifndef DISABLE_MASTER_SERIAL
    PRINTLN(str);
#endif // DISABLE_MASTER_SERIAL
#else
#ifndef DMX_OUTPUT_ENABLED
    PRINTLN(str);
#endif
#endif

#ifdef TRY_BLOCKING_SERIAL
    test_for_blocking();
#endif
}

void log_println(char c) {
#ifdef RFD_MASTER
#ifndef DISABLE_MASTER_SERIAL
    PRINTLN(c);
#endif // DISABLE_MASTER_SERIAL
#else
#ifndef DMX_OUTPUT_ENABLED
    PRINTLN(c);
#endif
#endif

#ifdef TRY_BLOCKING_SERIAL
    test_for_blocking();
#endif
}

void log_println(unsigned char b, int base) {
#ifdef RFD_MASTER
#ifndef DISABLE_MASTER_SERIAL
    #ifdef __AVR_ARCH__
    PRINTLN(b, base);
#else // __AVR_ARCH__
    PRINTLN(unsigned(b) << std::setbase(base));
#endif // __AVR_ARCH__
#endif // DISABLE_MASTER_SERIAL
#else // RFD_MASTER
#ifndef DMX_OUTPUT_ENABLED
#ifdef __AVR_ARCH__
    PRINTLN(b, base);
#else // __AVR_ARCH__
    PRINTLN(unsigned(b) << std::setbase(base));
#endif // __AVR_ARCH__
#endif // DMX_OUTPUT_ENABLED
#endif // RFD_MASTER

#ifdef TRY_BLOCKING_SERIAL
    test_for_blocking();
#endif
}

void log_println(int n, int base) {
#ifdef RFD_MASTER
#ifndef DISABLE_MASTER_SERIAL
    #ifdef __AVR_ARCH__
    PRINTLN(n, base);
#else // __AVR_ARCH__
    PRINTLN(n << std::setbase(base));
#endif // __AVR_ARCH__
#endif // DISABLE_MASTER_SERIAL
#else
#ifndef DMX_OUTPUT_ENABLED
#ifdef __AVR_ARCH__
    PRINTLN(n, base);
#else // __AVR_ARCH__
    PRINTLN(n << std::setbase(base));
#endif // __AVR_ARCH__
#endif
#endif

#ifdef TRY_BLOCKING_SERIAL
    test_for_blocking();
#endif
}

void log_println(unsigned int n, int base) {
#ifdef RFD_MASTER
#ifndef DISABLE_MASTER_SERIAL
    #ifdef __AVR_ARCH__
    PRINTLN(n, base);
#else // __AVR_ARCH__
    PRINTLN(n << std::setbase(base));
#endif // __AVR_ARCH__
#endif // DISABLE_MASTER_SERIAL
#else
#ifndef DMX_OUTPUT_ENABLED
#ifdef __AVR_ARCH__
    PRINTLN(n, base);
#else // __AVR_ARCH__
    PRINTLN(n << std::setbase(base));
#endif // __AVR_ARCH__
#endif
#endif

#ifdef TRY_BLOCKING_SERIAL
    test_for_blocking();
#endif
}

void log_println(long n, int base) {
#ifdef RFD_MASTER
#ifndef DISABLE_MASTER_SERIAL
    #ifdef __AVR_ARCH__
    PRINTLN(n, base);
#else // __AVR_ARCH__
    PRINTLN(n << std::setbase(base));
#endif // __AVR_ARCH__
#endif // DISABLE_MASTER_SERIAL
#else
#ifndef DMX_OUTPUT_ENABLED
#ifdef __AVR_ARCH__
    PRINTLN(n, base);
#else // __AVR_ARCH__
    PRINTLN(n << std::setbase(base));
#endif // __AVR_ARCH__
#endif
#endif

#ifdef TRY_BLOCKING_SERIAL
    test_for_blocking();
#endif
}

void log_println(unsigned long n, int base) {
#ifdef RFD_MASTER
#ifndef DISABLE_MASTER_SERIAL
    #ifdef __AVR_ARCH__
    PRINTLN(n, base);
#else // __AVR_ARCH__
    PRINTLN(n << std::setbase(base));
#endif // __AVR_ARCH__
#endif // DISABLE_MASTER_SERIAL
#else
#ifndef DMX_OUTPUT_ENABLED
#ifdef __AVR_ARCH__
    PRINTLN(n, base);
#else // __AVR_ARCH__
    PRINTLN(n << std::setbase(base));
#endif // __AVR_ARCH__
#endif
#endif

#ifdef TRY_BLOCKING_SERIAL
    test_for_blocking();
#endif
}

void log_println(double n, int digits) {
#ifdef RFD_MASTER
#ifndef DISABLE_MASTER_SERIAL
    #ifdef __AVR_ARCH__
    PRINTLN(n, digits);
#else // __AVR_ARCH__
    PRINTLN(std::setprecision(digits) << std::fixed << n);
#endif // __AVR_ARCH__
#endif // DISABLE_MASTER_SERIAL
#else // RFD_MASTER
#ifndef DMX_OUTPUT_ENABLED
#ifdef __AVR_ARCH__
    PRINTLN(n, digits);
#else // __AVR_ARCH__
    PRINTLN(std::setprecision(digits) << std::fixed << n);
#endif // __AVR_ARCH__
#endif // DMX_OUTPUT_ENABLED
#endif // RFD_MASTER

#ifdef TRY_BLOCKING_SERIAL
    test_for_blocking();
#endif
}

void log_println_n(long long n) {
#ifdef RFD_MASTER
#ifndef DISABLE_MASTER_SERIAL
    #ifdef __AVR_ARCH__
    if (n < 10) {
        PRINT((unsigned int) n);
    } else {
        unsigned long long subproblem = n / 10;
        int remainder = n % 10;
        log_println_n(subproblem);
        PRINT(remainder);
    }
#else // __AVR_ARCH__
    PRINTLN(std::setbase(10) << n);
#endif // __AVR_ARCH__
#endif // DISABLE_MASTER_SERIAL
#else // RFD_MASTER
#ifndef DMX_OUTPUT_ENABLED
#ifdef __AVR_ARCH__
    if (n < 10) {
        PRINT((unsigned int) n);
    } else {
        unsigned long long subproblem = n / 10;
        int remainder = n % 10;
        log_println_n(subproblem);
        PRINT(remainder);
    }
    PRINTLN("");
#else // __AVR_ARCH__
    PRINTLN(std::setbase(10) << n);
#endif // __AVR_ARCH__
#endif // DMX_OUTPUT_ENABLED
#endif // RFD_MASTER

#ifdef TRY_BLOCKING_SERIAL
    test_for_blocking();
#endif
}

void log_println_n(unsigned long long n) {
#ifdef RFD_MASTER
#ifndef DISABLE_MASTER_SERIAL
    #ifdef __AVR_ARCH__
    if (n < 10) {
        PRINT((unsigned int) n);
    } else {
        unsigned long long subproblem = n / 10;
        int remainder = n % 10;
        log_println_n(subproblem);
        PRINT(remainder);
    }
#else // __AVR_ARCH__
    PRINTLN(std::setbase(10) << n);
#endif // __AVR_ARCH__
#endif // DISABLE_MASTER_SERIAL
#else // RFD_MASTER
#ifndef DMX_OUTPUT_ENABLED
#ifdef __AVR_ARCH__
    if (n < 10) {
        PRINT((unsigned int) n);
    } else {
        unsigned long long subproblem = n / 10;
        int remainder = n % 10;
        log_println_n(subproblem);
        PRINT(remainder);
    }
    PRINTLN("");
#else // __AVR_ARCH__
    PRINTLN(std::setbase(10) << n);
#endif // __AVR_ARCH__
#endif // DMX_OUTPUT_ENABLED
#endif // RFD_MASTER

#ifdef TRY_BLOCKING_SERIAL
    test_for_blocking();
#endif
}

void log_println(unsigned long long n){
    log_println_n(n);
#ifdef RFD_MASTER
#ifndef DISABLE_MASTER_SERIAL
#ifdef __AVR_ARCH__
    PRINTLN("");
#endif
#endif
#endif
}

void log_println(long long n){
    log_println_n(n);
#ifdef RFD_MASTER
#ifndef DISABLE_MASTER_SERIAL
#ifdef __AVR_ARCH__
    PRINTLN("");
#endif
#endif
#endif
}