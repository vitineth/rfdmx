//
// Created by ryan on 3/6/23.
//

#ifndef NODE_LOGGING_H
#define NODE_LOGGING_H

#ifdef __AVR_ARCH__
#include <Arduino.h>
#else

#include <cstdint>
#include <string>

typedef std::string String;
typedef std::size_t size_t;
#endif

// These are mostly just aliases through to Serial.print__
//   All functions share the same general structure. On a master node it will
//   go straight through to the Serial.print(ln)? call.
//   On a node, it will check if DMX_OUTPUT_ENABLED is set (from _global.h)
//   and if so it will not log anything. If not (DMX output is NOT enabled) it will
//   pass it through to the Serial.print(ln)? call
#ifdef __AVR_ARCH__
void log_print(const __FlashStringHelper *ifsh);
#endif

void log_print(const String &s);

void log_print(const char str[]);

void log_print(char c);

void log_print(unsigned char b, int base);

void log_print(int n, int base);

void log_print(unsigned int n, int base);

void log_print(long n, int base);

void log_print(unsigned long n, int base);

void log_print(double n, int digits);

#ifdef __AVR_ARCH__
void log_println(const __FlashStringHelper *ifsh);
#endif

void log_println(const String &s);

void log_println(const char str[]);

void log_println(char c);

void log_println(unsigned char b, int base);

void log_println(int n, int base);

void log_println(unsigned int n, int base);

void log_println(long n, int base);

void log_println(unsigned long n, int base);

void log_println(double n, int digits);

void log_println(long long n);

void log_println(unsigned long long n);

#endif //NODE_LOGGING_H
