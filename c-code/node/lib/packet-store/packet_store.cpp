//
// Created by ryan on 4/19/23.
//

#include "packet_store.h"
#include "logging.h"

packet_store::packet_store() {
    memset(this->lengths, 0, 32);
    for (auto &packet: this->packets) {
        memset(packet, 0, 32);
    }
    this->stored = 0;
}

uint8_t *packet_store::store(uint8_t offset, uint8_t length) {
    if (offset >= 23) return nullptr;
    this->lengths[offset] = length;
    this->stored = this->stored > offset ? this->stored : offset+1;
    return this->packets[offset];
}

void packet_store::clear(){
    this->stored = 0;
}

uint8_t* packet_store::get(uint8_t offset, uint8_t& length_out){
    if(offset < this->stored){
//        log_print("o = ");
//        log_println(offset, 10);
        length_out = this->lengths[offset];

//        unsigned volatile long i;
//        for (i = 0; i < 10000; ++i) {
//
//        }

        return this->packets[offset];
    }

    return nullptr;
}