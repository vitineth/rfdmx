//
// Created by ryan on 4/19/23.
//

#ifndef NODE_PACKET_STORE_H
#define NODE_PACKET_STORE_H

#ifdef __AVR_ARCH__
#include <Arduino.h>
#else
#include <cstdint>
#include <cstring>
#endif

/**
 * Contains a stash of up to 23 packets in a multipart flow to be used for resend requests.
 */
struct packet_store{
protected:
    /**
     * The raw byte contents of the packets that were sent on the last send
     */
    uint8_t packets[23][32]{};
    /**
     * The length of each of the packets within {@link #packets}
     */
    uint8_t lengths[32]{};
    /**
     * The amount of packets in the store
     */
    uint8_t stored = 0;
public:
    /**
     * Constructs a new empty store initialised with 0 stored packets
     */
    packet_store();

    /**
     * Used to store a packet into this store. Provide the offset of the packet being stored and the length of the
     * packet and it will return a pointer to a memory block into which you can write the packet contents. This pointer
     * will be valid until either another store call with the same offset is done; clear() is called; or the underlying
     * memory of this store is freed. You should generally avoid using memcpy or the like to place packets into the
     * returned pointer and instead serialise directly to the returned pointer as your buffer.
     * @param offset the offset of this packet in the flow, maximum 22
     * @param length the length of the packet, maximum 32
     * @return the pointer to a memory block for this packet to be written or nullptr if the above constraints are
     *         violated
     */
    uint8_t* store(uint8_t offset, uint8_t length);

    /**
     * Returns a pointer to a block of memory for the packet at the given offset. Or nullptr if none has been saved in
     * that slot. length_out should be a pointer to where the length of the packet should be written.
     * @param offset the offset of the packet to look up
     * @param length_out where the length should be written
     * @return the pointer to the location if valid or nullptr if not
     */
    uint8_t* get(uint8_t offset, uint8_t& length_out);

    /**
     * Deletes all packets currently contained within the store
     */
    void clear();

};

#endif //NODE_PACKET_STORE_H
