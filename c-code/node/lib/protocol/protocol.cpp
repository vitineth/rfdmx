//
// Created by ryan on 2/22/23.
//
#include "protocol.h"

// given 0xAABB
//   UINT16_BE_FIRST_BYTE = 0xAA
//   UINT16_BE_SECOND_BYTE = 0xBB
//      -> BIG ENDIAN

#define UINT16_LE_FIRST_BYTE(value) (value & 0xFF)
#define UINT16_LE_SECOND_BYTE(value) ((value >> 8) & 0xFF)
#define UINT16_LE_BUILD(b1, b2) ((b2 << 8) | b1)
#define UINT16_BE_FIRST_BYTE(value) ((value >> 8) & 0xFF)
#define UINT16_BE_SECOND_BYTE(value) (value & 0xFF)
#define UINT16_BE_BUILD(b1, b2) ((b1 << 8) | b2)

rf_root_t::rf_root_t(uint8_t deviceId, packet_type_t type, uint8_t sequence, uint8_t flags) : device_id(deviceId),
packet_type(type),
sequence(sequence),
flags(flags) {}

size_t rf_root_t::required_size() const {
    return 4;
}

size_t rf_root_t::serialize(uint8_t *output) const {
    output[0] = this->device_id;
    output[1] = this->packet_type;
    output[2] = this->sequence;
    output[3] = this->flags;
    return 4;
}

rf_root_t *rf_root_t::deserialize(uint8_t *input, size_t size) {
    if (size < 4) return nullptr;
    return new rf_root_t(input[0], static_cast<packet_type_t>(input[1]), input[2], input[3]);
}

// RF_DMX_T

size_t rf_dmx_t::required_size() const {
    return rf_root_t::required_size() + 4 + this->size;
}

size_t rf_dmx_t::serialize(uint8_t *output) const {
    uint16_t pointer = rf_root_t::serialize(output);

    output[pointer] = compression_type | (this->offset << 2);
    output[pointer + 1] = (this->size >> 8) & 0xFF;
    output[pointer + 2] = this->size & 0xFF;
    output[pointer + 3] = this->universe;
    if (this->size != 0) memcpy(output + (pointer + 4), this->data, this->size);

    return pointer + 4 + this->size;
}

rf_dmx_t *rf_dmx_t::deserialize(uint8_t *input, size_t size) {
    if (size < 6) return nullptr;
    rf_root_t *root = rf_root_t::deserialize(input, size);
    size_t pointer = root->required_size();
    auto compress = static_cast<compression_type_t>(input[pointer] & 0b00000011);
    uint8_t offset = (input[pointer] & 0b11111100) >> 2;
    uint16_t packet_size = (input[pointer + 1] << 8) | input[pointer + 2];
    uint8_t universe = input[pointer + 3];
    if (packet_size + 4 > size) return nullptr;

    char *dmx = new char[packet_size];
    memcpy(dmx, input + pointer + 4, packet_size);
    auto v = new rf_dmx_t(root->device_id, root->sequence, root->flags, compress, offset, universe, packet_size, dmx);
    v->deserialized = true; // mark that data should be deleted in the destructor

    delete root;
    return v;
}

rf_dmx_t *rf_dmx_t::deserialize_with_root(const rf_root_t &root, uint8_t *input, size_t size) {
    if (size < 6) return nullptr;
    size_t pointer = root.required_size();
    auto compress = static_cast<compression_type_t>(input[pointer] & 0b00000011);
    uint8_t offset = (input[pointer] & 0b11111100) >> 2;
    uint16_t packet_size = (input[pointer + 1] << 8) | input[pointer + 2];
    uint8_t universe = input[pointer + 3];
    if (packet_size + 4 > size) return nullptr;

    char *dmx = new char[packet_size];
    memcpy(dmx, input + pointer + 4, packet_size);
    auto v = new rf_dmx_t(root.device_id, root.sequence, root.flags, compress, offset, universe, packet_size, dmx);
    v->deserialized = true; // mark that data should be deleted in the destructor

    return v;
}

size_t rf_dmx_t::compress(dmx_store *store, uint8_t data[512]) {
    size_t pointer = 0;
    uint8_t temp_copy[512];
    bool writing = false;
    uint8_t write_count = 0;
    bool fail_out = false;

//    Serial.print(F("We have "));
//    Serial.print(store->used());
//    Serial.println(F(" in the queue"));

    for (size_t data_pointer = 0; data_pointer < 512; data_pointer++) {
        // If compressing it will be bigger than the original packet, there's no point
        if (pointer >= 512) {
            fail_out = true;
            break;
        }
        bool has_changed = store->used() == 0 || store->changed(data[data_pointer], data_pointer);

        if (writing) {
            if (!has_changed && data_pointer < 511) {
                bool next_has_changed = store->used() == 0 || store->changed(data[data_pointer + 1], data_pointer + 1);

                if (!next_has_changed) {
                    // Two bytes have not changed, end this block
                    writing = false;
                    temp_copy[pointer - (write_count + 1)] = write_count;
                    write_count = 0;
                    continue;
                }
            }
            if (write_count >= __UINT8_MAX__) {
                // Cannot add anymore to this run block, need to start a new block
                // Update the size on the existing block
                // address
                // | |  size
                // | | |   pointer
                // | | |   |
                // 0 0 0 0 _
                temp_copy[pointer - (write_count + 1)] = write_count;
                write_count = 0;
                temp_copy[pointer] = UINT16_BE_FIRST_BYTE(data_pointer);
                temp_copy[pointer + 1] = UINT16_BE_SECOND_BYTE(data_pointer);
                temp_copy[pointer + 2] = 0; // Size, to be completed later
                pointer += 3;
            }
            temp_copy[pointer] = data[data_pointer];
            write_count++;
            pointer++;
        } else {
            if (has_changed) {
                writing = true;
                write_count = 1;
                temp_copy[pointer] = UINT16_BE_FIRST_BYTE(data_pointer);
                temp_copy[pointer + 1] = UINT16_BE_SECOND_BYTE(data_pointer);
                temp_copy[pointer + 2] = 0; // Size, to be completed later
                temp_copy[pointer + 3] = data[data_pointer];
                pointer += 4;
            }
        }
    }

    if (fail_out) {
        return SIZE_MAX;
    }

    memcpy(data, temp_copy, pointer);
    return pointer;
}

bool rf_dmx_t::expand(uint8_t last_frame[512], const uint8_t *incoming, size_t data_size) {
    size_t pointer = 0;

    while (pointer < data_size - 3) {
        uint16_t address = UINT16_BE_BUILD(incoming[pointer], incoming[pointer + 1]);
        uint8_t size = incoming[pointer + 2];
        if (data_size < pointer + 2 + size) {
            return false;
        }

        for (int i = 0; i < size; ++i) {
            last_frame[address + i] = incoming[pointer + 3 + i];
        }
        pointer += 3 + size;
    }

    return true;
}

void rf_dmx_t::set_continuation_flag() {
    this->flags = this->flags | this->CONTINUATION_MASK;
}

void rf_dmx_t::set_start_flag() {
    this->flags = this->flags | this->START_MASK;
}

void rf_dmx_t::set_termination_flag() {
    this->flags = this->flags | this->TERMINATION_MASK;
}

void rf_dmx_t::set_resend_flag() {
    this->flags = this->flags | this->RESEND_MASK;
}

bool rf_dmx_t::has_continuation_flag() const {
    return (this->flags & this->CONTINUATION_MASK) != 0;
}

bool rf_dmx_t::has_start_flag() const {
    return (this->flags & this->START_MASK) != 0;
}

bool rf_dmx_t::has_termination_flag() const {
    return (this->flags & this->TERMINATION_MASK) != 0;
}

bool rf_dmx_t::has_resend_flag() const {
    return (this->flags & this->RESEND_MASK) != 0;
}

compression_type_t rf_dmx_t::get_compression_type() const {
    return static_cast<compression_type_t>(this->compression_type & 0b00000011);
}

void rf_dmx_t::set_resend_flag_on_serialised(uint8_t* data){
    const size_t flags_index = 3;
    data[flags_index] |= RESEND_MASK;
}

void rf_dmx_t::ensure_not_deserialized() {
    this->deserialized = false;
}

// RF_CHECKIN_T

size_t rf_checkin_t::required_size() const {
    return rf_root_t::required_size() + 1;
}

size_t rf_checkin_t::serialize(uint8_t *output) const {
    uint16_t pointer = rf_root_t::serialize(output);
    output[pointer] = this->universe;
    return pointer + 1;
}

rf_checkin_t *rf_checkin_t::deserialize(uint8_t *input, size_t size) {
    if (size < 5) return nullptr;
    rf_root_t *root = rf_root_t::deserialize(input, size);
    size_t pointer = root->required_size();
    uint8_t universe = input[pointer];

    auto v = new rf_checkin_t(root->device_id, root->sequence, root->flags, universe);
    delete root;

    return v;
}

rf_checkin_t *rf_checkin_t::deserialize_with_root(const rf_root_t &root, uint8_t *input, size_t size) {
    if (size < 5) return nullptr;
    size_t pointer = root.required_size();
    uint8_t universe = input[pointer];

    auto v = new rf_checkin_t(root.device_id, root.sequence, root.flags, universe);
    return v;
};

// RF_REQUEST_T

size_t rf_request_t::required_size() const {
    return rf_root_t::required_size() + 2;
}

size_t rf_request_t::serialize(uint8_t *output) const {
    uint16_t pointer = rf_root_t::serialize(output);
    output[pointer] = this->offset;
    output[pointer + 1] = this->sequence;
    return pointer + 2;
}

rf_request_t *rf_request_t::deserialize(uint8_t *input, size_t size) {
    if (size < 5) return nullptr;
    rf_root_t *root = rf_root_t::deserialize(input, size);
    size_t pointer = root->required_size();
    uint8_t offset = input[pointer];
    uint8_t sequence = input[pointer + 1];

    auto v = new rf_request_t(root->device_id, root->sequence, root->flags, offset, sequence);
    delete root;

    return v;
}

rf_request_t *rf_request_t::deserialize_with_root(const rf_root_t &root, uint8_t *input, size_t size) {
    if (size < 5) return nullptr;
    size_t pointer = root.required_size();
    uint8_t offset = input[pointer];
    uint8_t sequence = input[pointer + 1];

    auto v = new rf_request_t(root.device_id, root.sequence, root.flags, offset, sequence);

    return v;
}

