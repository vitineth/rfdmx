//
// Created by ryan on 3/15/23.
//
#include "protocol.h"
#include "logging.h"

#ifndef __AVR_ARCH__

#ifndef F
#define F(s) s
#endif

const uint8_t HIGH = 255;
void digitalWrite(uint8_t, uint8_t){}

uint64_t millis() {
    return std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::system_clock::now().time_since_epoch()).count();
}

#endif

#define MAX(a, b) a > b ? a : b

void rf_proto_handler::_parse_dmx(const rf_root_t &root, uint8_t *buffer, size_t size) {
    auto dmx = rf_dmx_t::deserialize_with_root(root, buffer, size);
    last_seen_offset = dmx->offset > last_seen_offset ? dmx->offset : last_seen_offset;

//    log_print(F("[proto:_parse_dmx]   sequence = "));
//    log_println(dmx->sequence, 10);
    log_print(F("[proto:_parse_dmx]   offset = "));
    log_print(dmx->offset, 10);
    if (dmx->has_start_flag()) log_print("S");
    if (dmx->has_continuation_flag()) log_print("C");
    if (dmx->has_termination_flag()) log_print("T");
    if (dmx->has_resend_flag()) log_print("R");
    log_println("");

//    log_print("last_seen_offset=");
//    log_println(last_seen_offset, 10);
//
//    log_print("last_seen_sequence = ");
//    log_println(last_seen_sequence, 10);
//
//    log_print("last_seen_offset = ");
//    log_println(last_seen_offset, 10);
//
//    log_print("multipart_terminate_offset = ");
//    log_println(multipart_terminate_offset, 10);
//
//    log_print("multipart_in_flight = ");
//    log_println(multipart_in_flight ? 100 : 0, 10);
//
//    log_print("multipart_data_size = ");
//    log_println(multipart_data_size, 10);
//
//    log_print("requested_resent_markers = ");
//    log_println(requested_resent_markers, 10);
//
//    log_print("multipart_start_sequence = ");
//    log_println(multipart_start_sequence, 10);

    if (dmx->universe != this->universe_filter) {
        delete dmx;
        return;
    }
    uint8_t start_index = dmx->sequence - dmx->offset;
    if (dmx->has_resend_flag()) {
        log_print("si = ");
        log_println(start_index, 10);

        // If this sequence has already been requested, ignore it
        if ((this->seen_starting_sequences_valid[0] && this->seen_starting_sequences[0] == start_index)
            || (this->seen_starting_sequences_valid[1] && this->seen_starting_sequences[1] == start_index)
            || (this->seen_starting_sequences_valid[2] && this->seen_starting_sequences[2] == start_index)
                ) {
            delete dmx;
            return;
        }

        digitalWrite(8, HIGH);
        log_print("before = ");
        log_println(requested_resent_markers, 2);
        // This was resent, so we can flip down the bit on the resent mask
        requested_resent_markers &= ~((uint32_t) 0x01 << dmx->offset);
        log_print("after  = ");
        log_println(requested_resent_markers, 2);

        // If this packet has already been received, then its worth just skipping it rather than trying to do
        // the processing
        if((reconstruction_receive_markers & ((uint32_t) 0b01 << dmx->offset)) != 0){
            log_println("skipping as already seen");
            delete dmx;
            return;
        }
//        log_print("mask   = ");
//        log_println(((uint32_t)0x01 << dmx->offset), 2);
//        log_print("pmask  = ");
//        log_println(~(0x01 << dmx->offset), 2);

//        log_print(F("[proto:_parse_dmx] was resend, this flips down "));
//        log_print(~(0x01 << dmx->offset), 2);
//        log_print(F("and leaves "));
//        log_println(requested_resent_markers, 2);

        log_print("current receive flags = ");
        log_print(reconstruction_receive_markers, 2);
        uint32_t copy = reconstruction_receive_markers;
        uint8_t n = 0;
        while (copy > 0) {
            n++;
            copy >>= 1;
        }
        log_print("(");
        log_print(n, 10);
        log_println(")");
        log_print("terminate offset = ");
        log_println(multipart_terminate_offset, 10);

        // If the mask is now clear, it means that all bits of this packet have been received and it needs
        // handling as if it was complete. This only works when we've had the terminate and can guarantee that all
        // packets are received
        if (requested_resent_markers == 0 && multipart_terminate_offset != UINT8_MAX) {
            log_println(F("[proto:_parse_dmx] packet is now complete, resolved"));
            // This is a little hacky but it will parse this packet as if it was the last in the sequence.
            // As all offsets are done via dmx->offset this should be safe
            dmx->set_termination_flag();

        }
    }

    if (multipart_in_flight) {
        // We should mark this packet as received immediately so it doesn't get flagged on the next receive
        reconstruction_receive_markers |= (uint32_t)0x01 << dmx->offset;
        // If the multipart is in flight, we can detect missed packets here!
        if (dmx->offset > 0 && (reconstruction_receive_markers & ((uint32_t)0x01 << (dmx->offset - 1))) == 0) {
//            log_print(F("[proto:_parse_dmx] detected a missing packet in the sequence, re-requesting"));
//            log_print(dmx->sequence - 1, 10);
//            log_print(F("."));
//            log_println(dmx->offset - 1, 10);

            log_println("r:missing");
            for (uint8_t i = 0; i < dmx->offset; ++i) {
                if ((reconstruction_receive_markers & ((uint32_t)0x01 << (i))) == 0) {
                    requested_resent_markers |= (uint32_t)0x01 << (i);
                    this->_safe_request_resent(start_index+i, i, false);
                }
            }

            // This means the previous packet has not been marked as received so we can pre-emptively request
            // a resent before parsing it. The rest of the checks try to detect bits of packets which would mean
            // multipart_in_flight would not be set when it should be
        }
    }

    if (dmx->has_start_flag()) {
        if (this->seen_starting_sequences_valid[0] && this->seen_starting_sequences[0] == dmx->sequence)
            this->seen_starting_sequences_valid[0] = false;
        if (this->seen_starting_sequences_valid[1] && this->seen_starting_sequences[1] == dmx->sequence)
            this->seen_starting_sequences_valid[1] = false;
        if (this->seen_starting_sequences_valid[2] && this->seen_starting_sequences[2] == dmx->sequence)
            this->seen_starting_sequences_valid[2] = false;

        if (multipart_in_flight) {
            // Termination of the last packet was missed - at this point there's probably not point requesting
            // a resent of that as we already have a new packet in flight.
            //   TODO: under what conditions should it request a resent when it already has the first segment
            //         of a new packet - this is very hard to manage with low memory
            log_println(F("[warn:_parse_dmx]: multipart_in_flight was true on a dmx->has_start_flag() check"));
        }

        reconstruction_receive_markers = 0;
        multipart_terminate_offset = UINT8_MAX;
        multipart_in_flight = false;
        multipart_data_size = 0;
        requested_resent_markers = 0;
        multipart_start_sequence = 0;
        multipart_last_time_requested = 0;
        reconstruction_receive_markers |= 0x01;
        resend_request_total = 0;

//        log_println(F("[proto:_parse_dmx] Cleared data"));

        if (!dmx->has_termination_flag()) {
            // If it is a combined start+end then its not a multipart, otherwise mark that we are expecting more
            // packets in this multipart
            multipart_start_sequence = dmx->sequence;
            multipart_in_flight = true;
            multipart_last_seen_time = millis();
        }

//        log_print(F("[proto:_parse_dmx] Copying "));
//        log_print(dmx->size, 10);
//        log_println(F("bytes from dmx data to reconstruction buffer"));

        memcpy(reconstruction_buffer, dmx->data, dmx->size);
        multipart_data_size = MAX(multipart_data_size, dmx->size);

//        log_print(F("[proto:_parse_dmx] is start, multipart_data_size = "));
//        log_println(multipart_data_size, 10);
    }
    if (dmx->has_continuation_flag()) {
        if (!multipart_in_flight) {
//            log_print(F("[proto:_parse_dmx] start was not seen, need to re-request missing packets "));
            multipart_in_flight = true;
            reconstruction_receive_markers |= (uint32_t)0x01 << dmx->offset;
            // This likely means that the start packet was missed. Using offset we can figure out how far off
            // we are and request every packet that isn't in the store, and then mark that we are part of a
            // multipart flow
            log_println("r:missc");
            for (int i = dmx->offset; i >= 0; i--) {
                // if dmx->offset = 2 and dmx->sequence=200
                // this would run i = 2 1 0
                // requesting a resent for sequence
                // TODO: confirm that this check works as expected
                if ((reconstruction_receive_markers & ((uint32_t)0x01 << i)) == 0) {
                    requested_resent_markers |= 0x01 << (i);

//                    log_print(F(" "));
//                    log_print(dmx->sequence - (dmx->offset - i), 10);
//                    log_print(F("."));
//                    log_print(i, 10);

                    this->_safe_request_resent(dmx->sequence - (dmx->offset - i), i, false);
                }
            }
        }

//        log_print(F("[proto:_parse_dmx] Copying "));
//        log_print(dmx->size, 10);
//        log_print(F("bytes from dmx data to reconstruction_buffer["));
//        log_print(dmx->offset * rf_dmx_t::PAYLOAD_SIZE, 10);
//        log_println(F("]"));

        multipart_last_seen_time = millis();
        memcpy(reconstruction_buffer + (dmx->offset * rf_dmx_t::PAYLOAD_SIZE), dmx->data, dmx->size);
        reconstruction_receive_markers |= (uint32_t)0x01 << dmx->offset;
        multipart_data_size = MAX(multipart_data_size, (dmx->offset * rf_dmx_t::PAYLOAD_SIZE) + dmx->size);

//        log_print(F("[proto:_parse_dmx] is continue, multipart_data_size = "));
//        log_println(multipart_data_size, 10);
    }
    if (dmx->has_termination_flag()) {
        if (!dmx->has_start_flag() && !multipart_in_flight) {
//            log_print(F("[proto:_parse_dmx] packet is not complete, need to re-request missing packets "));
            // This is the end of a multipart flow but we are missing the starting one(s).
            // This will request a resend but with low optimism,
            log_println("r:misst");
            for (uint8_t i = dmx->offset; i > 0; ++i) {
                // if dmx->offset = 2 and dmx->sequence=200
                // this would run i = 2 1
                // requesting a resent for sequence 198 199
                // TODO: confirm that this check works as expected
                if ((reconstruction_receive_markers & ((uint32_t)0x01 << i)) == 0) {
                    requested_resent_markers |= (uint32_t)0x01 << (i - 1);

//                    log_print(F(" "));
//                    log_print(dmx->sequence - (dmx->offset - i), 10);
//                    log_print(F("."));
//                    log_print(i, 10);

                    this->_safe_request_resent(dmx->sequence - i, i - 1, false);
                }
            }

            delete dmx;
            return;
        }

//        log_print(F("[proto:_parse_dmx] Copying "));
//        log_print(dmx->size, 10);
//        log_print(F("bytes from dmx data to reconstruction_buffer["));
//        log_print(dmx->offset * rf_dmx_t::PAYLOAD_SIZE, 10);
//        log_println(F("]"));

        memcpy(reconstruction_buffer + (dmx->offset * rf_dmx_t::PAYLOAD_SIZE), dmx->data, dmx->size);
        reconstruction_receive_markers |= (uint32_t)0x01 << dmx->offset;
        multipart_data_size = MAX(multipart_data_size, (dmx->offset * rf_dmx_t::PAYLOAD_SIZE) + dmx->size);

        log_print(F("[proto:_parse_dmx] is terminate, multipart_data_size = "));
        log_print(multipart_data_size, 10);

        if (multipart_in_flight) {
            log_println(F(" (is multipart)"));
        } else {
            log_println(F(" (no multipart)"));
        }

        // Eg. one packet fell through the cracks
        // dmx->offset                      = 5
        // requested_resent_markers         = 0b00010101
        // reconstruction_receiver_markers  = 0b00001000
        //                                | = 0b00011101
        //                                ~ = 0b11100010
        //         (1 << dmx->offset) - 1 & = 0b00000010
        // Eg. all correct
        // dmx->offset                      = 5
        // requested_resent_markers         = 0b00010111
        // reconstruction_receiver_markers  = 0b00001000
        //                                | = 0b00011111
        //                                ~ = 0b11100000
        //         (1 << dmx->offset) - 1 & = 0b00000000
        if (multipart_terminate_offset != UINT8_MAX &&
            (((1 << multipart_terminate_offset) & (~(requested_resent_markers | reconstruction_receive_markers))) !=
             0)) {
            // (0b001 << 2) & (~(0 | 7)) == 0
            // 0b100 & ~(0b000 | 0b111) == 0
            // 0b100 & ~(0b111) == 0
            // 0b100 & 0b000 == 0
            // 0
            log_print(F("multipart_terminate_offset = 0b"));
            log_println((int) multipart_terminate_offset, 2);
            log_print(F("requested_resent_markers = 0b"));
            log_println((int) requested_resent_markers, 2);
            log_print(F("reconstruction_receive_markers = 0b"));
            log_println((int) reconstruction_receive_markers, 2);

            // Alert as we should _not_ get to this point with all the other checks
            this->panic(__LINE__, this);
            return;
        }

        if (multipart_terminate_offset == UINT8_MAX) {
            multipart_terminate_offset = dmx->offset;
            uint32_t mask = ~((uint32_t)0);
            mask >>= dmx->offset;
            mask <<= dmx->offset;

            // This makes a mask like 111100000
            // but it needs to be like 000011111
            mask = ~mask;

            // This will clear any pending requests for things beyond the end of the packet
            requested_resent_markers &= mask;
        }

        if (requested_resent_markers != 0) {
            log_print(F("[proto:_parse_dmx] requested resend = "));
            log_println(requested_resent_markers, 2);
            // At least one packet has been requested for resent from the master node. This means we should not
            // process the end of this packet as the buffer is only partially filled. On the whole packet being
            // received, dmx->has_termination_flag() will be set to true to re-trigger this parsing
            // TODO: this is bad code style but the comment is left in for clarity
        } else {

            if (multipart_in_flight) {
                // This DMX packet is only partially valid as it does not contain all the data from the sequential
                // packets. Needs to create a new packet with all the combined data and then send that.
                // As a compromise on the conflicting information ,this will use the data parsed from the most recent
                // packet
                auto combined = rf_dmx_t(
                        dmx->device_id,
                        dmx->sequence,
                        0,
                        dmx->compression_type,
                        0,
                        dmx->universe,
                        multipart_data_size,
                        reinterpret_cast<char *>(reconstruction_buffer)
                );
                this->handle_dmx(&combined, this);

                log_print("marking = ");
                log_println(multipart_start_sequence, 10);
                this->seen_starting_sequences_valid[this->seen_sequence_pointer] = true;
                this->seen_starting_sequences[this->seen_sequence_pointer] = multipart_start_sequence;
                if (++this->seen_sequence_pointer >= 3) this->seen_sequence_pointer = 0;
            } else {
                this->handle_dmx(dmx, this);
            }

            // Once done - reset all the properties
            reconstruction_receive_markers = 0;
            multipart_terminate_offset = UINT8_MAX;
            multipart_in_flight = false;
            requested_resent_markers = 0;
            multipart_last_seen_time = 0;
            multipart_data_size = 0;

            log_println(F("[proto:_parse_dmx]: packet is complete"));


            // TODO: heavy operation
            memset(reconstruction_buffer, 0, 550);

        }
    }

    delete dmx;
}

void rf_proto_handler::_parse_checkin(const rf_root_t &root, uint8_t *buffer, size_t size) {
    if (this->handle_checkin == nullptr) return;

    auto v = rf_checkin_t::deserialize_with_root(root, buffer, size);
    this->handle_checkin(v, this);
    delete v;
}

void rf_proto_handler::_parse_request(const rf_root_t &root, uint8_t *buffer, size_t size) {
    if (this->handle_request == nullptr) return;

    auto v = rf_request_t::deserialize_with_root(root, buffer, size);
    this->handle_request(v, this);
    delete v;
}

void rf_proto_handler::parse(uint8_t *buffer, size_t size) {
//    log_println("Incoming message, parsing");
    // TODO:
    //   9 detecting missing termination packets
    //      this can either be done on timing or on the next packet arriving, timing might be easier?

    // Now that this is a protocol packet
    // Continuation DMX packets have to be the full packet size so can be offset in the reconstruction buffer
    auto root = rf_root_t::deserialize(buffer, size);
    if (this->on_root_parse != nullptr) this->on_root_parse(root, this);

    // Ignore messages not from the root
    if (this->device_id_filter != UINT8_MAX) {
        if (root->device_id != this->device_id_filter) {
            delete root;
            return;
        }
    }

    // Warn on packets that are too new
    if (has_seen_any && last_seen_sequence + 1 != root->sequence) {
        log_print(F("[proto:parse] Missed a packet in the sequence, got "));
        log_print(root->sequence, 10);
        log_print(F(" but expected "));
        log_println(last_seen_sequence + 1, 10);
    }

    has_seen_any = true;

    last_seen_sequence = root->sequence;
//    last_seen_sequence = root->sequence > last_seen_sequence ? root->sequence : last_seen_sequence;

//    log_print(F("[proto:parse] rf packet received ["));
//    log_print(root->sequence, 10);
//    log_print(F(" @ "));
//    log_print(root->device_id, 10);
//    log_println(F("]"));

    switch (root->packet_type) {
        case DMX:
            this->_parse_dmx(*root, buffer, size);
            break;
        case CHECKIN:
            this->_parse_checkin(*root, buffer, size);
            break;
        case REQUEST:
            digitalWrite(22, HIGH);
            this->_parse_request(*root, buffer, size);
            break;
    };
    delete root;
}

void rf_proto_handler::tick() {
    if (!multipart_in_flight) return;
    if (multipart_last_seen_time == 0) return;
    if (resend_request_total > max_resend_requests) return;
    if (millis() - multipart_last_seen_time > multipart_time_between_assume_dropped) {
        // Assume a packet has been dropped and assume that it is the next one in the sequence...
        // Check that it has not been re-requested already and that it was not the terminate packet! If it is then we've
        // reached the end of the flow but are missing intermediary packets.
        if ((requested_resent_markers & ((uint32_t) 0x01 << (last_seen_offset + 1))) == 0 &&
            multipart_terminate_offset != last_seen_offset) {

//            log_print(
//                    F("[proto:tick] assuming a packet has been dropped in the sequence based on timing - requesting "));
//            log_print(last_seen_sequence + 1, 10);
//            log_print(F("."));
//            log_print(last_seen_offset + 1, 10);
//            log_print(F(" (multipart_terminate_offset="));
//            log_print(multipart_terminate_offset, 10);
//            log_print(F(", last_seen_offset="));
//            log_print(last_seen_offset, 10);
//            log_println(F(")"));
            log_print("r:time:");
            log_println(last_seen_offset + 1, 10);
            log_println(requested_resent_markers, 10);
            log_print("test = ");
//            log_println(, 10);
            requested_resent_markers |= ((uint32_t) 0x01) << ((uint32_t) last_seen_offset + 1);
            log_println(requested_resent_markers, 10);
            resend_request_total++;
            this->_safe_request_resent(last_seen_sequence + 1, last_seen_offset + 1, true);
        }
        // Check the time since the last re-request and if its over the time, re-request _all_ the currently missing
        // intermediary packets
        if (millis() - multipart_last_time_requested > multipart_time_between_assume_dropped) {
            uint32_t pending = requested_resent_markers;
            uint8_t offset = 0;
            while (offset < 32) {
                // Packet has been requested for resend but not received
                if ((pending & 0b01) == 1) {
//                    log_print(
//                            F("[proto:tick] packet has been requested for resent but has not been received - requesting "));
//                    log_print(last_seen_sequence + 1, 10);
//                    log_print(F("."));
//                    log_println(last_seen_offset + 1, 10);

                    log_print("r:pending:");
                    log_println(offset, 10);
                    resend_request_total++;
                    this->_safe_request_resent(multipart_start_sequence + offset, offset, true);
                }
                offset++;
                pending >>= 1;
            }
        }
    }
}

void rf_proto_handler::_safe_request_resent(uint8_t a, uint8_t b, bool enforce_delay) {
    if (enforce_delay && millis() - multipart_last_time_requested < delay_between_requests) return;
    multipart_last_time_requested = millis();
    if (this->request_resend != nullptr) this->request_resend(a, b, this);
}