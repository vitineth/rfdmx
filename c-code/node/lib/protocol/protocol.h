//
// Created by ryan on 2/22/23.
//

#ifndef NODE_PROTOCOL_H
#define NODE_PROTOCOL_H

#ifdef __AVR_ARCH__
#include <Arduino.h>
#else

#include <cstdint>
#include <cstring>
#include <chrono>

typedef std::size_t size_t;
#endif

#ifdef INCLUDE_ABSOLUTE
#include "dmx-store/dmx_store.h"
#else

#include "dmx_store.h"

#endif

/**
 * Indicates the compression type of a DMX packet which determines expanding methodology on the receiver. This is
 * encoded within 2 bits. One value has been reserved to indicate compression type is encoded in the first byte of
 * the data payload to allow for expanded compression methods.
 */
enum compression_type_t {
    /**
     * The DMX data in the packet is not compressed/encoded in any way and can be copied directly as valid data
     */
    FULL = 0,
    /**
     * The DMX data in the packet is represented using differential encoding and needs to be expanded
     */
    DIFF = 1,
    /**
     * The compression type is contained within the first byte of the data payload and that value should be used
     * instead. This is not supported within this implementation but is designed for future expansion
     */
    USE_FIRST_BYTE = 3,
};

/**
 * Represents the type of packet being delivered. Encoded in uint8_t
 */
enum packet_type_t {
    /**
     * This packet is from master to node, containing DMX data for display
     */
    DMX = 0,
    /**
     * This packet is from node to master, checkin in that they are still active and are listening for a universe
     */
    CHECKIN = 1,
    /**
     * This packet is from node to master, indicating that a packet has been lost and needs to be resent if possible by the master
     */
    REQUEST = 2,
};

/**
 * This is the base packet type from which all others should extend. This has no meanings on its own and is only used
 * in the context of extending packets. Contains essential data shared by the different packet types
 */
struct rf_root_t {
public:
    /**
     * The unique identifier for the device that is sending this message. Convention dictates that this will be 0 on the
     * master and nodes will have IDs >0. Each node should have a unique ID within the entire network, pre-assigned by
     * the administrator
     */
    uint8_t device_id{};
    /**
     * The packet type, as described in the struct, encoded as one byte
     */
    packet_type_t packet_type;
    /**
     * The sequence number of the packet from this client. This will wrap cyclically
     */
    uint8_t sequence{};
    /**
     * A byte reserved for flags to be used by sub packet types
     */
    uint8_t flags{};

    /**
     * Default constructor for this packet which defines all fields
     * @param deviceId the unique device ID of the sender
     * @param type the type of packet
     * @param sequence the sequence number from this client
     * @param flags any packet specific flags
     */
    rf_root_t(uint8_t deviceId, packet_type_t type, uint8_t sequence, uint8_t flags);

    rf_root_t(rf_root_t const &instance) = default;

    virtual ~rf_root_t() = default;

    /**
     * Returns the amount of bytes of a buffer that would need to be allocated to serialise this packet. This is not
     * static to allow variable packet sizes
     * @return the size of buffer required for serialisation
     */
    [[nodiscard]] virtual size_t required_size() const;

    /**
     * Serialises this packet into the given buffer. This assumes that the buffer is correctly sized (using
     * required_size) and returns the number of bytes used of the buffer (used for inheritance based serialisation)
     * @param output the buffer into which it should be serialised
     * @return the amount of bytes from the buffer in use
     */
    virtual size_t serialize(uint8_t *output) const;

    /**
     * Warning: this allocates memory which will need to be freed
     * @param input the array input received from the network
     * @param size the size of input
     * @return a pointer to an allocated instance of this class
     */
    static rf_root_t *deserialize(uint8_t *input, size_t size);
};

/**
 * A checkin packet which a node uses to inform a master they are present and the universe they are listening for
 */
struct rf_checkin_t : rf_root_t {
public:
    /**
     * The universe being listened for. This is currently represented as a uint8_t as a limitation of the protocol
     * which only allows universes up to this limit but this is addressed for future changes
     */
    const uint8_t universe{};

    rf_checkin_t(uint8_t deviceId, uint8_t sequence, uint8_t flags, const uint8_t universe)
            : rf_root_t(deviceId, CHECKIN, sequence, flags), universe(universe) {}

    ~rf_checkin_t() override = default;

    [[nodiscard]] size_t required_size() const override;

    size_t serialize(uint8_t *output) const override;

    static rf_checkin_t *deserialize(uint8_t *input, size_t size);

    /**
     * A version of deserialize that skips deserializing the root and instead uses the provided values.
     * @param root the pre-parsed root node from which to copy values
     * @param input the raw buffer to parse
     * @param size the size of the raw buffer
     * @return an instance of rf_checkin_t (needs deleting) or nullptr if the buffer is invalid
     */
    static rf_checkin_t *deserialize_with_root(const rf_root_t &root, uint8_t *input, size_t size);

};

/**
 * A request packet indicating that a node has missed a packet and needs it resent from the master if possible
 */
struct rf_request_t : rf_root_t {
public:
    /**
     * The offset of the missed packet within the multipart flow
     */
    uint8_t offset{};
    /**
     * The sequence number of the packet within the multipart flow. This should be used to check that the packet being
     * resent is the same as requested
     */
    uint8_t sequence{};

    rf_request_t(uint8_t deviceId, uint8_t sequence, uint8_t flags, uint8_t offset,
                 uint8_t sequence1) : rf_root_t(deviceId, REQUEST, sequence, flags), offset(offset),
                                      sequence(sequence1) {}

    [[nodiscard]] size_t required_size() const override;

    size_t serialize(uint8_t *output) const override;

    static rf_request_t *deserialize(uint8_t *input, size_t size);

    /**
     * A version of deserialize that skips deserializing the root and instead uses the provided values.
     * @param root the pre-parsed root node from which to copy values
     * @param input the raw buffer to parse
     * @param size the size of the raw buffer
     * @return an instance of rf_request_t (needs deleting) or nullptr if the buffer is invalid
     */
    static rf_request_t *deserialize_with_root(const rf_root_t &root, uint8_t *input, size_t size);
};

/**
 * A packet containing some portion of DMX data being transmitted from master to node
 */
struct rf_dmx_t : rf_root_t {
protected:
    /**
     * Mask for the flag indicating that this is a start packet in a multipart sequence
     */
    static const uint8_t START_MASK = 0b10000000;
    /**
     * Mask for the flag indicating that this is a continuation packet in a multipart sequence
     */
    static const uint8_t CONTINUATION_MASK = 0b01000000;
    /**
     * Mask for the flag indicating that this is a termination packet in a multipart sequence
     */
    static const uint8_t TERMINATION_MASK = 0b00100000;
    /**
     * Mask for the flag indicating that this is a packet that has been resent
     */
    static const uint8_t RESEND_MASK = 0b00010000;
    /**
     * Indicates if this packet was deserialised and therefore if the data pointer needs to be deleted during
     * destruction
     */
    bool deserialized;
public:
    /**
     * Remaining size for data
     *    32           - 4    - 1           - 2    - 1           = 23
     *   (packet max) (root) (compression) (size) (universe)    (remaining)
     */
    static const uint8_t PAYLOAD_SIZE = 23;

    /**
     * The type of compression used for this packet. This field is merged with #offset
     */
    compression_type_t compression_type;
    /**
     * The size of the data contained in the payload of this packet in bytes
     */
    uint16_t size{};
    /**
     * The universe for which this packet carries data
     */
    uint8_t universe{};
    /**
     * The raw dmx data for this packet
     */
    char *data;
    /**
     * The offset of this packet within the larger flow, if this is a multipart flow
     */
    uint8_t offset{};

    rf_dmx_t(
            uint8_t deviceId,
            uint8_t sequence,
            uint8_t flags,
            compression_type_t compressionType,
            uint8_t offset,
            uint8_t universe,
            uint16_t size,
            char *data
    )
            : rf_root_t(deviceId, DMX, sequence, flags),
              compression_type(compressionType),
              size(size),
              universe(universe),
              data(data),
              offset(offset),
              deserialized(false)
    {}

    ~rf_dmx_t() override {
        if (deserialized) {
            delete[] data;
        }
    }

    [[nodiscard]] size_t required_size() const override;

    size_t serialize(uint8_t *output) const override;

    /**
     * Utility function for flipping on the START flag
     */
    void set_start_flag();

    /**
     * Utility function for flipping on the CONTINUE flag
     */
    void set_continuation_flag();

    /**
     * Utility function for flipping on the TERMINATE flag
     */
    void set_termination_flag();

    /**
     * Utility function for flipping on the RESENT flag
     */
    void set_resend_flag();

    /**
     * Utility function
     * @return if this packet has the START bit set in the flag byte
     */
    [[nodiscard]] bool has_start_flag() const;

    /**
     * Utility function
     * @return if this packet has the CONTINUE bit set in the flag byte
     */
    [[nodiscard]] bool has_continuation_flag() const;

    /**
     * Utility function
     * @return if this packet has the TERMINATE bit set in the flag byte
     */
    [[nodiscard]] bool has_termination_flag() const;

    /**
     * Utility function
     * @return if this packet has the RESEND bit set in the flag byte
     */
    [[nodiscard]] bool has_resend_flag() const;

    /**
     * @return the compression type this packet is encoded with which should determine expansion process, if required
     */
    [[nodiscard]] compression_type_t get_compression_type() const;

    static rf_dmx_t *deserialize(uint8_t *input, size_t size);

    /**
     * This will allocate a new rf_dmx_t with an internal data buffer cloned from the input. This will not perform a new
     * parse for the root and will instead copy the values from the provided instance - note that this does not edit or
     * free the passed instance
     * @param root the root from which to copy values rather than parsing
     * @param input the input data buffer
     * @param size  the size of the data buffer
     * @return an rf_dmx_t packet with a new allocated internal buffer
     */
    static rf_dmx_t *deserialize_with_root(const rf_root_t &root, uint8_t *input, size_t size);

    /**
     * Compresses DMX data down using the protocol encoding method. This will overwrite the contents of data and return
     * the newly used length
     * @param data the data to compress and overwrite
     * @param size the size of data
     * @return  the new size of the data
     */
    static size_t compress(dmx_store *store, uint8_t data[512]);

    /**
     * When the packet has been encoded using DIFF, this expands it into the last_frame array.
     * @param last_frame the last full frame on to which the data should be written
     * @param incoming the most DIFF encoded data
     * @param data_size the size of incoming
     * @return if the expansion was successful
     */
    static bool expand(uint8_t last_frame[512], const uint8_t incoming[], size_t data_size);

    /**
     * Utility function to set the RESENT flag on an already serialised packet
     * @param data the data on which to set the resent flag
     */
    static void set_resend_flag_on_serialised(uint8_t *data);

    void ensure_not_deserialized();
};

/**
 * A general protocol handler which will automatically deal with deserialising packets and resent requests for
 * multipart DMX sequences
 */
struct rf_proto_handler {
private:
    /**
     * The maximum amount of resends that should be requested before the node gives up
     */
    static const uint8_t max_resend_requests = 232;
    /**
     * The amount of time in milliseconds that should at least be delayed between each resend request as not to
     * overwhelm the network or the master
     */
    static const uint8_t delay_between_requests = 40;
    /**
     * Buffer used for reconstructing multi-part packets in memory
     */
    uint8_t reconstruction_buffer[550]{};
    /**
     * The pointer to the next free space in the reconstruction_buffer buffer, will be 0
     * if there are no packets currently partially being processed
     */
    uint32_t reconstruction_receive_markers = 0;
    /**
     * The last seen sequence ID from the master node
     */
    uint8_t last_seen_sequence = 0;
    /**
     * If this protocol handler has seen any packets yet, this is used to suppress the initial out of order packet
     * warning
     */
    bool has_seen_any = false;
    /**
     * The last offset that was seen during a multipart flow, used to detect missing transmissions
     */
    uint8_t last_seen_offset = 0;
    /**
     * The offset of the terminate packet in the sequence if identified, UINT8_MAX if not
     */
    uint8_t multipart_terminate_offset = UINT8_MAX;
    /**
     * If the handler is currently in the process of handling a multipart DMX flow
     */
    bool multipart_in_flight = false;
    /**
     * The current accumulative size of the data received
     */
    size_t multipart_data_size = 0;
    /**
     * A bitset representing which packets have been requested for resending to detect when packets are done being
     * parsed. Used in the form 0b1 << offset
     */
    uint32_t requested_resent_markers = 0;
    /**
     * The sequence number of the first packet in a multipart flow
     */
    uint8_t multipart_start_sequence = 0;
    /**
     * The last time (millis()) a packet was seen by the protocol parser during a multipart flow, used to detect
     * missing packets based on time
     */
    uint64_t multipart_last_seen_time = 0;
    /**
     * The last time (millis()) a packet was requested to be resent to enable max frequencies of resends
     * (delay_between_requests)
     */
    uint64_t multipart_last_time_requested = 0;
    /**
     * The amount of time to pass between packets in a multipart flow before a dropped packet is assumed
     */
    uint64_t multipart_time_between_assume_dropped = 200;
    /**
     * The amount of total resent requests sent so far
     */
    uint8_t resend_request_total = 0;
    /**
     * The last three starting sequences of multipart flows which can be used to reject repeated resends if required
     */
    uint8_t seen_starting_sequences[3] = {0, 0, 0};
    /**
     * Containing which starting sequences have been set and which are uninitialised
     */
    bool seen_starting_sequences_valid[3] = {false, false, false};
    /**
     * The pointer to the next slot in the seen_starting_sequences array
     */
    uint8_t seen_sequence_pointer =0 ;
protected:
    /**
     * The handler to call when a packet needs to be resent
     * @param sequence_id the sequence ID to request
     * @param offset the offset to request
     * @param self  this
     */
    void (*request_resend)(uint8_t sequence_id, uint8_t offset, rf_proto_handler *self);

    /**
     * The handler function for received DMX packets. In the event of a multipart flow, this will be called once with
     * the complete set of data
     * @param dmx the received packets
     * @param self ths
     */
    void (*handle_dmx)(rf_dmx_t *dmx, rf_proto_handler *self);

    /**
     * The handler to be called when a checkin packet is received
     * @param checkin the packet received
     * @param self this
     */
    void (*handle_checkin)(rf_checkin_t *checkin, rf_proto_handler *self);

    /**
     * Handler to be called any time a root packet is parsed. This will be fired multiple times in a multipart flow
     * @param root the root parsed
     * @param self this
     */
    void (*on_root_parse)(rf_root_t *root, rf_proto_handler *self);

    /**
     * The handler to be called on a resend request being received
     * @param request the received request
     * @param self this
     */
    void (*handle_request)(rf_request_t *request, rf_proto_handler *self);

    /**
     * The panic function called when something serious goes wrong
     * @param line the line on which the panic occurred
     * @param self this
     */
    void (*panic)(int line, rf_proto_handler *self);

    /**
     * The universe for which DMX packets should be filtered
     */
    uint8_t universe_filter{};

    /**
     * The device ID filter to be applied to messages or UINT8_MAX if not being applied
     */
    uint8_t device_id_filter{};

    /**
     * Utility to parse DMX packets
     * @param root the root of this packet
     * @param buffer the buffer containing the packet
     * @param size the size of the buffer
     */
    void _parse_dmx(const rf_root_t &root, uint8_t *buffer, size_t size);

    /**
     * Utility to parse checkins
     * @param root the root of this packet
     * @param buffer the buffer containing the packet
     * @param size the size of the buffer
     */
    void _parse_checkin(const rf_root_t &root, uint8_t *buffer, size_t size);

    /**
     * Utility to parse request packets
     * @param root the root of this packet
     * @param buffer the buffer containing the packet
     * @param size the size of the buffer
     */
    void _parse_request(const rf_root_t &root, uint8_t *buffer, size_t size);

    /**
     * Wrapper around request_resend that handles the case of nullptr
     */
    void _safe_request_resent(uint8_t, uint8_t, bool);

public:
    rf_proto_handler(void (*requestResend)(uint8_t, uint8_t, rf_proto_handler *self),
                     void (*handleDmx)(rf_dmx_t *, rf_proto_handler *self),
                     void (*handleCheckin)(rf_checkin_t *, rf_proto_handler *self),
                     void (*onRootParse)(rf_root_t *, rf_proto_handler *self),
                     void (*onRequest)(rf_request_t *request, rf_proto_handler *self),
                     void(*panic)(int line, rf_proto_handler *self),
                     uint8_t universe_filter,
                     uint8_t device_id_filter
    )
            : request_resend(requestResend),
              handle_dmx(handleDmx),
              handle_checkin(handleCheckin),
              on_root_parse(onRootParse),
              handle_request(onRequest),
              panic(panic),
              universe_filter(universe_filter),
              device_id_filter(device_id_filter) {}

   /**
    * Parse an incoming buffer
    * @param buffer the buffer received over the network
    * @param size the size of the buffer
    */
    void parse(uint8_t *buffer, size_t size);

    /**
     * A tick to call each loop to handle time based detection
     */
    void tick();
};

#endif //NODE_PROTOCOL_H
