//
// Created by ryan on 3/6/23.
//

#include "rf.h"
#include "spi/spi.h"
#include "printf.h"
#include <Arduino.h>

const int RF_CE_PIN = 9;
const int RF_CS_PIN = 5;
RF24 node(RF_CE_PIN, RF_CS_PIN);

#define RECEIVER 0
#define TRANSMITTER 1
#define UNKNOWN 2

uint8_t state = UNKNOWN;

void configure_rf() {
    pinMode(RF_CS_PIN, OUTPUT);
    digitalWrite(RF_CS_PIN, HIGH);

    spi_select_rf();
    printf_begin();

    while (!node.begin(RF_CE_PIN, RF_CS_PIN)) {
        Serial.println(F("Radio hardware is not responding."));
        delay(1000);
    }

#ifdef RF_CHANNEL
    Serial.print("Assigning self to channel ");
    Serial.println(RF_CHANNEL, 10);
    node.setChannel(RF_CHANNEL);
#endif

    node.setPALevel(RF24_PA_LOW);
}

void set_as_receiver(bool silent) {
    if(state == RECEIVER) return;
    node.openReadingPipe(1, 0xF0F0F0F0A1);
    if (!silent)Serial.println(F("Pipe 1 has been opened for reading on address \"0xF0F0F0F0A1\""));

    node.startListening();
    if (!silent)Serial.println(F("Node has started listening"));

    state = RECEIVER;
}

void set_as_transmitter(bool silent) {
    if(state == TRANSMITTER) return;
    node.enableDynamicAck();
    node.stopListening();
    if (!silent) Serial.println(F("Node has stopped listening"));

    node.openWritingPipe(0xF0F0F0F0A1);
    if (!silent)Serial.println(F("Pipe has been opened for writing on address \"0xF0F0F0F0A1\""));

    state = TRANSMITTER;
}

void log_rf_debugging_info() {
    node.printPrettyDetails();
}

RF24* rf_node() {
    return &node;
}