//
// Created by ryan on 3/6/23.
//

#ifndef NODE_RF_H
#define NODE_RF_H

#include "RF24.h"

/**
 * Sets up any required configuration of the RF module including setting initial modes, ensuring hardware is present
 * and printing diagnostics
 */
void configure_rf();

/**
 * Sets the RF module as a receiver, disabling the sending pipe
 * @param silent determines if a message should be logged
 */
void set_as_receiver(bool silent);

/**
 * Sets the RF module as a transmitter, disabling the reading pipe
 * @param silent determines if a message should be logged
 */
void set_as_transmitter(bool silent);

/**
 * Print diagnostic information about the RF module
 */
void log_rf_debugging_info();

/**
 * Returns a pointer to a singleton instance of the RF module
 * @return the RF module which should always be defined
 */
RF24* rf_node();

#endif //NODE_RF_H
