//
// Created by ryan on 3/6/23.
//

#include "spi.h"
#include <Arduino.h>

#define SPI_SWITCH_DELAY 10

void spi_select_ethernet() {
#ifdef RF_CS_PIN
    digitalWrite(RF_CS_PIN, HIGH);
#endif
#ifdef RFD_MASTER
    // A node should never call this but this is just put in as a check
#ifdef ETHERNET_CS_PIN
    digitalWrite(ETHERNET_CS_PIN, LOW);
#endif
#endif
    delayMicroseconds(SPI_SWITCH_DELAY);
}

void spi_select_rf() {
    // The only module which needs to actively select the RF module is the master
    // so if this is running on the master this function can be a NOP
    // Otherwise if its not a master, actually perform the actions
#ifdef RFD_NODE
#ifdef RF_CS_PIN
    digitalWrite(RF_CS_PIN, LOW);
#endif
#ifdef ETHERNET_CS_PIN
    digitalWrite(ETHERNET_CS_PIN, HIGH);
#endif
    delayMicroseconds(SPI_SWITCH_DELAY);
#endif
}
