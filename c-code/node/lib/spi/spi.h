//
// Created by ryan on 3/6/23.
//

#ifndef NODE_SPI_H
#define NODE_SPI_H

/**
 * On master nodes will toggle the chip select pins to enable the ethernet module on the SPI bus
 */
void spi_select_ethernet();

/**
 * On master nodes will toggle the chip select pins to enable the rf module on the SPI bus
 */
void spi_select_rf();

#endif //NODE_SPI_H
