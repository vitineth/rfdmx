//
// Created by ryan on 3/12/23.
//

#ifndef NODE_NODE_STORE_H
#define NODE_NODE_STORE_H

#ifdef __AVR_ARCH__
#include <Arduino.h>
#else
#include <cstdint>
#include <cstring>
#endif

/**
 * A single node within the node checkin linked list. Contains the checkin time, node ID and universe of a single seen
 * node, as well as the pointer to the next node in the list
 */
struct store_node {
    /**
     * Constructs a new node with the provided properties
     * @param lastCheckin the time of the last checkin of this node as returned by millis()
     * @param nodeId the device id of this node as delivered in the checkin packet
     * @param universe the universe this node is listening on as delivered in the checkin packet
     * @param next the next node in the sequence or nullptr if there is no following node
     */
    store_node(uint16_t lastCheckin, uint8_t nodeId, uint8_t universe, store_node *next);

public:
    /**
     * The time of the last checkin as recorded by millis()
     */
    uint16_t last_checkin;
    /**
     * The unique device ID of this node that uniquely identified this entry
     */
    uint8_t node_id;
    /**
     * The universe the node is listening for
     */
    uint8_t universe;

    /**
     * The next node in the sequence or nullptr
     */
    store_node *next;
};

/**
 * The root of the node storage linked list which provides a usable interface
 */
struct node_storage {
private:
    /**
     * The root of the list of nullptr if there are no nodes in the list
     */
    store_node *root;
public:
    /**
     * The amount of nodes that are currently stored in this list
     */
    size_t length;

    /**
     * Default constructor which initialises this to an empty list
     */
    node_storage() = default;

    /**
     * Record a node checkin. This will either create a new entry in the list, or if one already exists for the node ID,
     * it will update it with the current time and universe data.
     * @param node_id the unique ID for the node to be used for lookup
     * @param universe the universe this node is listening for
     */
    void checkin(uint8_t node_id, uint8_t universe);

    /**
     * Removes a node from the list given its ID
     * @param node_id the unique ID of the node to be removed
     */
    void remove(uint8_t node_id);

    /**
     * If a node with the given ID is found, its last checkin time will be written out to the output and true will be
     * returned. If the node could not be found, it will return false and nothing in output will change
     * @param node_id the unique device ID of the node to lookup
     * @param output the pointer into which the last checkin time will be written
     * @return if the node was found and the last checkin date written to output
     */
    bool get_last_checkin(uint8_t node_id, uint16_t *output);

    /**
     * Prunes the list of all nodes that have not been seen in 'duration' milliseconds
     * @param duration the number of milliseconds after which a node should be removed
     */
    void prune(uint16_t duration);

    /**
     * Returns a `new` allocated array of 'output' length containing all universes currently being listened on by nodes.
     * @param output a pointer to a location into which the size of the array will be written
     * @return an array of unique universe values
     */
    uint8_t *get_universes(size_t *output);
};


#endif //NODE_NODE_STORE_H
