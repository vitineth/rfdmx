//
// Created by ryan on 3/12/23.
//

#include "node_store.h"

#ifdef __AVR_ARCH__
#define MILLIS() millis()
#else
#include <chrono>
#include <iostream>
uint64_t start_time = 0;
uint16_t _millis(){
    uint64_t  now = std::chrono::system_clock::now().time_since_epoch().count();
    if(start_time == 0) start_time = now;
    return (uint16_t) now - start_time;
}
#define MILLIS() _millis()
#endif

void node_storage::checkin(uint8_t node_id, uint8_t universe) {
    store_node *active = this->root;
    if (active == nullptr) {
        this->root = new store_node(MILLIS(), node_id, universe, nullptr);
        this->length++;
        return;
    }

    while (active != nullptr) {
        if (active->node_id == node_id) {
            active->last_checkin = MILLIS();
            active->universe = universe;
            return;
        }

        if (active->next == nullptr) {
            active->next = new store_node(MILLIS(), node_id, universe, nullptr);
            this->length++;
            return;
        }

        active = active->next;
    }
}

bool node_storage::get_last_checkin(uint8_t node_id, uint16_t *output) {
    store_node *active = this->root;
    if (active == nullptr) {
        return false;
    }

    while (active != nullptr) {
        if (active->node_id == node_id) {
            *output = active->last_checkin;
            return true;
        }

        active = active->next;
    }

    return false;
}

void node_storage::remove(uint8_t node_id) {
    store_node *active = this->root;
    store_node *tail = nullptr;
    if (active == nullptr) {
        return;
    }


    while (active != nullptr) {
        if (active->node_id == node_id) {
            if (tail == nullptr) {
                this->root = active->next;
                delete active;
            } else {
                tail->next = active->next;
                delete active;
            }
            this->length--;
            return;
        }

        tail = active;
        active = active->next;
    }
}

void node_storage::prune(uint16_t duration) {
    store_node *active = this->root;
    store_node *tail = nullptr;
    if (active == nullptr) {
        return;
    }

    while (active != nullptr) {
        if (MILLIS() - active->last_checkin > duration) {
            if (tail == nullptr) {
                auto next = active->next;
                this->root = next;

                delete active;
                active = next;
                tail = nullptr;
            } else {
                tail->next = active->next;
                delete active;

                active = tail->next;
            }

            this->length--;
        }else {
            tail = active;
            active = active->next;
        }
    }
}

uint8_t *node_storage::get_universes(size_t *output) {
    uint8_t *active_set;
    size_t allocated = 32;
    size_t used = 0;

    active_set = new uint8_t [allocated];

    auto active = this->root;
    while (active != nullptr) {
        if (used == allocated) {
            allocated = allocated + (allocated / 2);
            auto temp = new uint8_t [allocated];
            memcpy(temp, active_set, used);
            delete active_set;
            active_set = temp;
        }

        bool found = false;
        for (size_t i = 0; i < used; ++i) {
            if (active_set[i] == active->universe) {
                found = true;
                break;
            }
        }

        if (!found) {
            active_set[used] = active->universe;
            used++;
        }

        active = active->next;
    }

    auto returned = new uint8_t[used];
    for (size_t i = 0; i < used; ++i) {
        returned[i] = active_set[i];
    }

    delete active_set;

    *output = used;
    return returned;
}

store_node::store_node(uint16_t lastCheckin, uint8_t nodeId, uint8_t universe, store_node *next) : last_checkin(
        lastCheckin), node_id(nodeId), universe(universe), next(next) {}
