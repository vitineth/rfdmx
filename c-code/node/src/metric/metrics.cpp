//
// Created by ryan on 4/19/23.
//

// NODE
// 0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15
// [magic|nid] [   ID   ]  [timestamp] [   ID-1  ]
// 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31
// [   ID-2  ] [   ID-3  ] [   ID-4  ] [   ID-5  ]

#include <Arduino.h>
#include "rf.h"
#include "global/_global.h"
#include "spi.h"
#include "logging.h"

#define MASTER_MAGIC 0x5417FACE
#define NODE_MAGIC 0x0017FACD

struct MasterSend {
    uint32_t magic = MASTER_MAGIC;
    uint32_t id;
    uint32_t timestamp;
};
struct NodeAck {
    uint32_t magic_nid = NODE_MAGIC;
    uint32_t id;
    uint32_t timestamp;
    uint32_t seen[5];
};

//#define METRIC_NODE
#ifdef METRIC_NODE

// A metric node should receive packets, send back a single ack packet which contains
//   * ID of the incoming packet
//   * Original timestamp
//   * Last N seen packets
void setup() {
    global_setup();
    configure_rf();
    log_rf_debugging_info();
}

uint32_t last_seen_ids[5];

void loop() {
    set_as_receiver(true);
    uint8_t pipe;
    if (rf_node()->available(&pipe)) {
        uint8_t buffer[rf_node()->getPayloadSize()];
        rf_node()->read(&buffer, rf_node()->getPayloadSize());

        // Try the cast to the packet
        auto *na = reinterpret_cast<MasterSend *>(&buffer);
        if (na->magic == MASTER_MAGIC) {
            NodeAck ack;

            log_print("[ack][");
            log_print(NODE_ID, 10);
            log_print("] ");
            log_println(na->id, 10);

            ack.id = na->id;
            ack.timestamp = na->timestamp;
            ack.magic_nid = (((uint32_t) NODE_ID) << 24) | NODE_MAGIC;
            for (int i = 0; i < 5; ++i) {
                ack.seen[i] = last_seen_ids[i];
            }

            size_t length = sizeof(ack);
            auto *output = reinterpret_cast<uint8_t *>(&ack);

            set_as_transmitter(true);
            rf_node()->write(output, length, true);
            set_as_receiver(true);

            last_seen_ids[0] = last_seen_ids[1];
            last_seen_ids[1] = last_seen_ids[2];
            last_seen_ids[2] = last_seen_ids[3];
            last_seen_ids[3] = last_seen_ids[4];
            last_seen_ids[4] = na->id;
        }
    }
}

#endif

// MASTER
// 0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15
// [   ID   ]  [timestamp]
// 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31

//#define METRIC_MASTER
#ifdef METRIC_MASTER

#define ETHERNET_CS_PIN 10

#include <Ethernet.h>

struct RTTPoint {
    uint32_t timestamp;
    uint32_t rtt;

    RTTPoint(uint32_t timestamp, uint32_t rtt) : timestamp(timestamp), rtt(rtt) {}
};

class NodeMonitoringEntry {
private:
    static const uint8_t MAX_RTT_VALUES = 32;

    const uint8_t node_id;
    RTTPoint *rtt_values[MAX_RTT_VALUES] = {};
    uint32_t rtt_values_used = 0;

    // How does this work?
    //   for every 0 value it means that a packet was _sent_ but never received by the client
    //   and every 1 means that a packet was sent and received by the client, and that an ack was sent by the client and
    //      that either the ack was received by the master _or_ a subsequent ack has marked it as received
    //   the numbering is from left to right with the leftmost value representing the status of
    //   packet_matching_bitset_start_offset:
    //                               0b1101011100000000|00000000|00000000
    //                                                 ^        |       ^
    //               packet_matching_bitset_start_offset        |       packet_matching_bitset_start_offset + 16
    //                                                          used_bits = 8
    //
    //   used_bits will define how many of the bits are actually in use and used to indicate when the bitset is full
    //   and needs cleared. we use the later 16 in the set and shift it by that on each send. this means that if acks
    //   are received later they can still be marked and the server can integrate the data
    uint32_t packet_matching_bitset_start_offset = 0;
    uint32_t packet_matching_bitset = 0;
    uint8_t used_bits = 0;
    uint32_t drops_since_last_send = 0;
public:
    explicit NodeMonitoringEntry(uint8_t node_id, uint32_t packetMatchingBitsetStartOffset)
            : packet_matching_bitset_start_offset(
            packetMatchingBitsetStartOffset), node_id(node_id) {}

    [[nodiscard]] bool needs_rtt_send() const {
        return this->rtt_values_used >= NodeMonitoringEntry::MAX_RTT_VALUES;
    }

    [[nodiscard]] bool needs_drop_send() const {
        return this->used_bits >= 16;
    }

    [[nodiscard]] uint8_t *rtt_as_packet(size_t &length) {
        const size_t bytes_per_record = ((2 * 4) + 3);
        const size_t buffer_size = 1 + rtt_values_used * bytes_per_record;
        auto *buffer = new uint8_t[buffer_size];
        buffer[0] = 0xAE;

        for (uint32_t i = 0; i < rtt_values_used; ++i) {
            size_t start_index = 1 + (i * 11);
            buffer[start_index + 0] = (uint8_t) ((rtt_values[i]->timestamp >> 24) & ((uint32_t) 0xFF));
            buffer[start_index + 1] = (uint8_t) ((rtt_values[i]->timestamp >> 16) & ((uint32_t) 0xFF));
            buffer[start_index + 2] = (uint8_t) ((rtt_values[i]->timestamp >> 8) & ((uint32_t) 0xFF));
            buffer[start_index + 3] = (uint8_t) (rtt_values[i]->timestamp & ((uint32_t) 0xFF));
            buffer[start_index + 4] = 0x00;
            buffer[start_index + 5] = (uint8_t) ((rtt_values[i]->rtt >> 24) & ((uint32_t) 0xFF));
            buffer[start_index + 6] = (uint8_t) ((rtt_values[i]->rtt >> 16) & ((uint32_t) 0xFF));
            buffer[start_index + 7] = (uint8_t) ((rtt_values[i]->rtt >> 8) & ((uint32_t) 0xFF));
            buffer[start_index + 8] = (uint8_t) (rtt_values[i]->rtt & ((uint32_t) 0xFF));
            buffer[start_index + 9] = 0x00;
            buffer[start_index + 10] = 0x00;

            delete rtt_values[i];
        }

        rtt_values_used = 0;
        length = buffer_size;

        return buffer;
    }

    void node_checkin(NodeAck ack) {
        if (this->rtt_values_used >= 32) {
            log_println(
                    "[CRITICAL] Failed to save an RTT value, buffer is full but not been sent? Most recent value has been dropped");
            return;
        }

        this->rtt_values[this->rtt_values_used++] = new RTTPoint(ack.timestamp, millis() - ack.timestamp);

        // This is the offset from the start of the bitset
        uint8_t offset = (ack.id - this->packet_matching_bitset_start_offset) + 16;
        if (offset < 0 || offset > 32) {
            log_print("[CRITICAL] ACK for a packet outside the bitset = ");
            log_println(offset, 10);
            return;
        }

        uint32_t mask = ((uint32_t) 0x80000000) >> offset;
        this->packet_matching_bitset |= mask;

        for (int i = 0; i < 5; ++i) {
            uint8_t this_offset = offset - (i + 1);
            if (this_offset < 0 || this_offset > 32) {
                continue;
            }

            mask = ((uint32_t) 0x80000000) >> this_offset;

            if ((this->packet_matching_bitset & mask) == 0) {
                // This means that the ACK for the packet was missed and this packet was dropped
                this->drops_since_last_send++;
            }

            this->packet_matching_bitset |= mask;
        }
    }

    uint8_t *dropset_to_packet(size_t &length) {
        uint8_t *buffer = new uint8_t[4 + 1 + 1 + 4 + 4 + 1];
        length = 4 + 1 + 1 + 4 + 4 + 1;
        buffer[0] = 0xFA;
        buffer[1] = used_bits;

        buffer[2] = (uint8_t) ((this->packet_matching_bitset >> 24) & ((uint32_t) 0xFF));
        buffer[3] = (uint8_t) ((this->packet_matching_bitset >> 16) & ((uint32_t) 0xFF));
        buffer[4] = (uint8_t) ((this->packet_matching_bitset >> 8) & ((uint32_t) 0xFF));
        buffer[5] = (uint8_t) (this->packet_matching_bitset & ((uint32_t) 0xFF));

        buffer[6] = (uint8_t) ((this->drops_since_last_send >> 24) & ((uint32_t) 0xFF));
        buffer[7] = (uint8_t) ((this->drops_since_last_send >> 16) & ((uint32_t) 0xFF));
        buffer[8] = (uint8_t) ((this->drops_since_last_send >> 8) & ((uint32_t) 0xFF));
        buffer[9] = (uint8_t) (this->drops_since_last_send & ((uint32_t) 0xFF));

        // Was used_bits invalid
        buffer[10] = 0;

        buffer[11] = (uint8_t) ((this->packet_matching_bitset_start_offset >> 24) & ((uint32_t) 0xFF));
        buffer[12] = (uint8_t) ((this->packet_matching_bitset_start_offset >> 16) & ((uint32_t) 0xFF));
        buffer[13] = (uint8_t) ((this->packet_matching_bitset_start_offset >> 8) & ((uint32_t) 0xFF));
        buffer[14] = (uint8_t) (this->packet_matching_bitset_start_offset & ((uint32_t) 0xFF));

        this->drops_since_last_send = 0;
        this->packet_matching_bitset_start_offset += this->used_bits;
        this->packet_matching_bitset <<= this->used_bits;
        this->used_bits = 0;

        if ((this->packet_matching_bitset & 0xFFFF) != 0) {
            log_println("[CRITICAL] used_bits was not correct! shifting left by used_bits didn't make it 0");
            log_println("  this means that the start offset may not be right but continuing anyway...");
            this->packet_matching_bitset &= 0xFFFF0000;
            buffer[10] = 1;
        }

        return buffer;
    }

    void has_sent() {
        this->used_bits++;
    }

};

/**
 * MAC address for the master node to assign itself
 */
byte mac[] = {DEVICE_MAC_ADDRESS};
/**
 * IP Address for the master node to assign itself
 * TODO: how should this be configured and should it be through DHCP?
 * TODO: this includes the broadcast IP
 */
IPAddress ip(DEVICE_IP_ADDRESS);
EthernetUDP udp;

IPAddress metric_target(RECEIVER_IP);
uint16_t metric_port = RECEIVER_PORT;

uint32_t last_send_millis;
uint32_t current_id;
const uint32_t time_between_sends = 500;

NodeMonitoringEntry *entries[128];

// A master node should send packets containing
//   * ID
//   * Timestamp
// And upon receiving a packet should calculate
//   * Drop rate
//   * Round trip time
//  Drop rate can be inferred from how many the MASTER has not seen, and how many the NODE has
//    not seen (based on the last N packets).
void setup() {
    global_setup();
    configure_rf();
    log_rf_debugging_info();

    pinMode(ETHERNET_CS_PIN, OUTPUT);
    digitalWrite(ETHERNET_CS_PIN, HIGH);

    spi_select_ethernet();
    EthernetClass::begin(mac, ip);
    if (EthernetClass::hardwareStatus() == EthernetNoHardware) {
        log_println(F("Failed to initialise - there is no ethernet hardware attached on the assigned pins"));
        global_panic();
    }

    metric_target.printTo(Serial);
    udp.begin(9018);

    log_print(F("Configured node with IP address "));
    log_print(ip[0], 10);
    log_print(F("."));
    log_print(ip[1], 10);
    log_print(F("."));
    log_print(ip[2], 10);
    log_print(F("."));
    log_print(ip[3], 10);
    log_print(F(" # "));
    log_print(mac[0], 16);
    log_print(F(":"));
    log_print(mac[1], 16);
    log_print(F(":"));
    log_print(mac[2], 16);
    log_print(F(":"));
    log_print(mac[3], 16);
    log_print(F(":"));
    log_print(mac[4], 16);
    log_print(F(":"));
    log_println(mac[5], 16);
}

void send() {
    MasterSend ms;
    ms.id = current_id++;
    ms.timestamp = millis();

    log_print("[send] id=");
    log_print(ms.id, 10);
    log_print(" @ ");
    log_println(ms.timestamp, 10);

    char *ms_bytes = reinterpret_cast<char *>(&ms);

    set_as_transmitter(true);
    rf_node()->write(ms_bytes, sizeof(ms), true);
    set_as_receiver(true);
}

void receive() {
    set_as_receiver(true);
    uint8_t pipe;
    if (rf_node()->available(&pipe)) {
        uint8_t buffer[rf_node()->getPayloadSize()];
        rf_node()->read(&buffer, rf_node()->getPayloadSize());

        // Try the cast to the packet
        auto *na = reinterpret_cast<NodeAck *>(&buffer);
        if ((na->magic_nid & 0x00FFFFFF) == NODE_MAGIC) {
            uint8_t node_id = (na->magic_nid >> 24) & 0xFF;

            log_print("[receive][");
            log_print(node_id, 10);
            log_print("] ack ");
            log_println(na->id, 10);

            if (entries[node_id] == nullptr) {
                entries[node_id] = new NodeMonitoringEntry(node_id, na->id);
                entries[node_id]->node_checkin(*na);
            } else {
                entries[node_id]->node_checkin(*na);
            }
        } else {
            log_println("[receive] invalid magic");
        }
    }
}

void drop_send(NodeMonitoringEntry *entry) {
    size_t length;
    uint8_t *buffer = entry->dropset_to_packet(length);

    spi_select_ethernet();
    udp.beginPacket(metric_target, metric_port);
    udp.write(buffer, length);
    udp.endPacket();
    spi_select_rf();

    delete buffer;
}

void rtt_send(NodeMonitoringEntry *entry) {
    size_t length;
    uint8_t *buffer = entry->rtt_as_packet(length);

    spi_select_ethernet();
    udp.beginPacket(metric_target, metric_port);
    udp.write(buffer, length);
    udp.endPacket();
    spi_select_rf();

    delete buffer;
}

void loop() {
    receive();

    for (auto &entrie: entries) {
        if (entrie != nullptr) {
            if (entrie->needs_drop_send()) drop_send(entrie);
            if (entrie->needs_rtt_send()) rtt_send(entrie);
        }
    }

    if (millis() - last_send_millis > time_between_sends) {
        send();
        last_send_millis = millis();

        for (auto &entrie: entries) {
            if (entrie != nullptr) {
                entrie->has_sent();
            }
        }
    }
}

#endif