//
// Created by ryan on 3/6/23.
//
#include "global/_global.h"

#ifdef RFD_NODE

#include "rf/rf.h"
#include "dmx-interface/dmx_interface.h"
#include "spi/spi.h"
#include "logging.h"
#include "protocol.h"

const uint8_t LISTEN_UNIVERSE = UNIVERSE_FILTER;

const int WRITE_PIN = 3;
const int CLOCK_PIN = 5;
uint32_t received_packets_since_write = 0;
uint64_t last_write_time = 0;
const uint64_t write_frequency = 10000;

unsigned long checking_transmit_delay = 10000;
unsigned long last_checkin_transmit = 0;
uint8_t sequence_id = 0;

unsigned long dmx_transmit_delay = 50;
/**
 * The time in millis() of the last DMX transmission which can be used to make
 * it transmit at a frequency
 */
unsigned long last_dmx_transmit = 0;
/**
 * The active DMX values which are being transmitted on the line
 */
uint8_t active_output[512];

rf_proto_handler *handler;

void checkin_transmit() {
    auto packet = rf_checkin_t(DEVICE_ID, sequence_id++, 0, LISTEN_UNIVERSE);
    uint8_t buffer[packet.required_size()];
    size_t length = packet.serialize(buffer);
    rf_node()->write(buffer, length);
    log_println(F("Transmitted checkin packet"));
}

void request_send(uint8_t sequence, uint8_t offset, rf_proto_handler *self) {
    set_as_transmitter(true);
    auto packet = rf_request_t(DEVICE_ID, sequence_id++, 0, offset, sequence);
    uint8_t buffer[packet.required_size()];
    size_t length = packet.serialize(buffer);
    rf_node()->write(buffer, length);
    set_as_receiver(true);

    log_print(F("Transmitted request for "));
    log_print(sequence, 10);
    log_print(F("."));
    log_print(offset, 10);
    log_println(F(" to master"));
}

void handle_dmx(rf_dmx_t *dmx, rf_proto_handler *self) {
    if (dmx->size == 0) {
        log_println(F("  0 size - no changes made"));
    } else {
        if (dmx->compression_type == DIFF) {
            rf_dmx_t::expand(active_output, reinterpret_cast<const uint8_t *>(dmx->data), dmx->size);
        } else {
            memcpy(active_output, dmx->data, dmx->size);
        }

        dmx_transmit(active_output);
    }

    log_println(F("Active Output updated = "));
    dump_hex(active_output, 10);
}

void panic(int line, rf_proto_handler *self) {
    log_print(F("The RF protocol handler requested a global panic! This happened on line "));
    log_print(line, 10);
    log_println(F(" - calling global panic"));
    global_panic();
}

void setup() {
    global_setup();

    pinMode(CLOCK_PIN, OUTPUT);
    pinMode(WRITE_PIN, OUTPUT);

    memset(active_output, 0, 512);

    log_println(F("Running on a dmx node - configuring rf only"));
    configure_rf();

    set_as_receiver(false);
    log_rf_debugging_info();
    delay(10);
    dmx_configure();

    handler = new rf_proto_handler(
            request_send,
            handle_dmx,
            nullptr,
            nullptr,
            nullptr,
            panic,
            LISTEN_UNIVERSE,
            0
    );

}

void receive_rf() {
    uint8_t pipe;
    byte buffer[rf_node()->getPayloadSize()];
    if (rf_node()->available(&pipe)) {
        uint8_t payload = rf_node()->getPayloadSize();
        rf_node()->read(&buffer, payload);

        handler->parse(buffer, payload);
    }
}

void loop() {
    spi_select_rf();
    set_as_receiver(true);
    receive_rf();
    handler->tick();

    if (millis() - last_dmx_transmit > dmx_transmit_delay) {
        last_dmx_transmit = millis();
        dmx_transmit(active_output);
    }

    if (millis() - last_checkin_transmit > checking_transmit_delay) {
        last_checkin_transmit = millis();
        set_as_transmitter(true);
        checkin_transmit();
    }

    if (millis() - last_write_time > write_frequency){
        last_write_time = millis();

        // Writing a 32byte number, so 4 writes
        shiftOut(WRITE_PIN, CLOCK_PIN, MSBFIRST, (received_packets_since_write >> 24));
        shiftOut(WRITE_PIN, CLOCK_PIN, MSBFIRST, (received_packets_since_write >>16));
        shiftOut(WRITE_PIN, CLOCK_PIN, MSBFIRST, (received_packets_since_write >>8));
        shiftOut(WRITE_PIN, CLOCK_PIN, MSBFIRST, (received_packets_since_write >>0));
    }
}

#endif