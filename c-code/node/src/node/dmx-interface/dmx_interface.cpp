//
// Created by ryan on 3/1/23.
//
#include "dmx_interface.h"

#ifdef RFD_NODE
#ifdef DMX_OUTPUT_ENABLED
#include <RS485.h>
#endif
#endif

void dmx_configure() {
#ifdef RFD_NODE
#ifdef DMX_OUTPUT_ENABLED
    // DMX is 1 start bit, 8 bit value, 2 stop bits
    RS485.begin(250000, SERIAL_8N2);
#endif
#endif
}

void dmx_transmit(uint8_t data[512]) {
#ifdef RFD_NODE
#ifdef DMX_OUTPUT_ENABLED
    RS485.beginTransmission();
    RS485.sendBreakMicroseconds(88);
    delayMicroseconds(12);
    RS485.write((uint8_t) 0); // Start code
    for(int i = 0; i < 512; i++){
        RS485.write(data[i]);
    }
    RS485.endTransmission();
#endif
#endif
}