//
// Created by ryan on 3/1/23.
//

#ifndef NODE_DMX_INTERFACE_H
#define NODE_DMX_INTERFACE_H

#include <Arduino.h>
#include "global/_global.h"

/**
 * Sets up any necessary configuration needed to output raw dmx data. This must be called before any calls to
 * dmx_transmit to get expected behaviour
 */
void dmx_configure();

/**
 * Writes a new set of data to the active dmx output
 * @param data the 512 channels to write to the live dmx output if present
 */
void dmx_transmit(uint8_t data[512]);

#endif //NODE_DMX_INTERFACE_H
