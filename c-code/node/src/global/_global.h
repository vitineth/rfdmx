//
// Created by ryan on 2/22/23.
//

#ifndef NODE__GLOBAL_H
#define NODE__GLOBAL_H

#ifndef __AVR_ARCH__
#define F(s) s
#endif

#include <HardwareSerial.h>

#define UINT16_LE_FIRST_BYTE(value) (value & 0xFF)
#define UINT16_LE_SECOND_BYTE(value) ((value >> 8) & 0xFF)
#define UINT16_LE_BUILD(b1, b2) ((b2 << 8) | b1)
#define UINT16_BE_FIRST_BYTE(value) ((value >> 8) & 0xFF)
#define UINT16_BE_SECOND_BYTE(value) (value & 0xFF)
#define UINT16_BE_BUILD(b1, b2) ((b1 << 8) | b2)

#define UINT32_BE_FIRST_BYTE(value) ((value >> 24) & 0xFF)
#define UINT32_BE_SECOND_BYTE(value) ((value >> 16) & 0xFF)
#define UINT32_BE_THIRD_BYTE(value) ((value >> 8) & 0xFF)
#define UINT32_BE_FOURTH_BYTE(value) (value & 0xFF)

// TODO: find a way to uniquely define this per arduino
#define HEX_WIDTH 20

#ifndef PSTR
#define PSTR(n) n
#endif

/**
 * A global initialisation function to be called regardless of the purpose. This does script independent setup
 * such as serial initialisiation
 */
inline void global_setup() {
    Serial.begin(9600);
    Serial.println(F("INIT executed"));
}


// Use this to toggle between modes
#define DEBUG_MASTER_OVER_UDP
#define DISABLE_HEX_DUMP
//#define DMX_OUTPUT_ENABLED
#define TRY_BLOCKING_SERIAL

/**
 * A simple panic function designed to cease execution entirely. This just wraps a while(true) with some logging
 */
[[noreturn]] inline void global_panic() {
    if (Serial) {
        Serial.println("[A function has called global_panic - halting execution]");
    }
    while (true);
}

/**
 * Prints the given values of length provided as a hex dump with a width of HEX_WIDTH (currently 20). This does not do
 * any conversion to ascii like some hex dumps do, just prints the raw values in a table
 * @param values the values to write to serial output
 * @param length the length of the values buffer
 */
inline void dump_hex(uint8_t *values, size_t length) {
#ifndef DISABLE_HEX_DUMP
#ifndef DMX_OUTPUT_ENABLED
    char buffer[4];
    size_t current_line = 0;
    for (size_t i = 0; i < length; ++i) {
        if (current_line % HEX_WIDTH == 0){
            if (current_line != 0) Serial.println();
            sprintf(buffer, "%02i", current_line);
            buffer[3] = 0;
            Serial.print(buffer);
            Serial.print(" | ");
        }
        sprintf(buffer, "%02X ", values[i]);
        buffer[3] = '\0';
        Serial.print(buffer);
        current_line++;
    }
    Serial.println();
#endif
#endif
}

#ifdef __arm__
// should use uinstd.h to define sbrk but Due causes a conflict
extern "C" char* sbrk(int incr);
#else  // __ARM__
extern char *__brkval;
#endif  // __arm__

/**
 * Attempts to return the free memory remaining on the device. This is sourced directly from
 * https://github.com/mpflaga/Arduino-MemoryFree and is not my own work
 * @return the amount of free memory in bytes
 */
inline int freeMemory() {
    char top;
#ifdef __arm__
    return &top - reinterpret_cast<char*>(sbrk(0));
#elif defined(CORE_TEENSY) || (ARDUINO > 103 && ARDUINO != 151)
    return &top - __brkval;
#else  // __arm__
    return __brkval ? &top - __brkval : &top - __malloc_heap_start;
#endif  // __arm__
}

#endif //NODE__GLOBAL_H
