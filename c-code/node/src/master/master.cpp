//
// Created by ryan on 3/6/23.
//
#include "global/_global.h"
#include "array_map.h"
#include "artnet-constants.h"

#ifdef RFD_MASTER

//#define MAXPROF 10
//#define PROFILING 1
//#define PROFILING_MAIN 1

#include "spi.h"
#include "dmx_store.h"
#include "rf.h"
#include "dmx-interface/dmx_interface.h"
#include "logging.h"
#include "protocol.h"
#include "node_store.h"
#include "packet_store.h"

#include <Ethernet.h>

#undef UDP_TX_PACKET_MAX_SIZE
#define UDP_TX_PACKET_MAX_SIZE 550

#define OEM_CODE 0x2B09
#define ARTNET_PORT 6454
// 7 Bits
#define ARTNET_SUBNET 0
// 4 Bits
#define ARTNET_NET 0
#define ETHERNET_CS_PIN 10

// {0}{            Net           } {   Sub-Net   } {   Universe  }
// 15  14  13  12  11  10   9   8   7   6   5   4   3   2   1   0
const uint16_t ARTNET_PORT_ADDRESS_BASE = 0x7fff & (((ARTNET_NET & 0x7f) << 8) | ((ARTNET_SUBNET & 0x0f) << 4));

uint8_t universe_filter[] = {UNIVERSE_FILTER};
/**
 * The next available packet number in the sequence, will auto loop at 255
 */
uint8_t packet_sequence_no = 0;
/**
 * The current storage of dmx messages for generating differential data
 */
//dmx_store store;
array_map *dmx_store_map;
/**
 * Buffer into which UDP packets can be received for processing
 */
char udp_buffer[UDP_TX_PACKET_MAX_SIZE];

uint8_t packets_since_synchronization = 0;
const uint8_t synchronization_frequency = 60;

/**
 * MAC address for the master node to assign itself
 */
byte mac[] = {DEVICE_MAC_ADDRESS};
/**
 * IP Address for the master node to assign itself
 * TODO: how should this be configured and should it be through DHCP?
 * TODO: this includes the broadcast IP
 */
IPAddress ip(DEVICE_IP_ADDRESS);

rf_proto_handler *handler;

unsigned long parsed_packets = 0;

/**
 * Number of packets that have been re-requested since the last metrics send, this is a proxy for loss
 */
uint32_t requested_packets_since_metrics = 0;
/**
 * If the value in requested_packets_since_metrics has overflowed while adding and is no longer fully reliable
 */
bool rpsm_overflowed = false;
/**
 * The time to wait between metrics
 */
const uint32_t millis_between_metrics = 120000;
/**
 * The time (millis()) that the last metrics were sent
 */
uint64_t last_metrics_push = 0;
/**
 * The UDP client through which packets should be received
 */
EthernetUDP udp;

node_storage storage;
packet_store packets;

#ifdef DEBUG_MASTER_OVER_UDP
IPAddress desktop(METRICS_IP_ADDRESS);
uint8_t debugging_buffer[1024];
#endif

void configure_ethernet() {
    pinMode(ETHERNET_CS_PIN, OUTPUT);
    digitalWrite(ETHERNET_CS_PIN, HIGH);

    spi_select_ethernet();
    EthernetClass::begin(mac, ip);
    if (EthernetClass::hardwareStatus() == EthernetNoHardware) {
        log_println(F("Failed to initialise - there is no ethernet hardware attached on the assigned pins"));
        global_panic();
    }

    if (udp.begin(ARTNET_PORT) == 0) {
        log_println(F("Failed to begin UDP receiver - no sockets are available"));
        global_panic();
    }

    log_print(F("Configured node with IP address "));
    log_print(ip[0], 10);
    log_print(F("."));
    log_print(ip[1], 10);
    log_print(F("."));
    log_print(ip[2], 10);
    log_print(F("."));
    log_print(ip[3], 10);
    log_print(F(" # "));
    log_print(mac[0], 16);
    log_print(F(":"));
    log_print(mac[1], 16);
    log_print(F(":"));
    log_print(mac[2], 16);
    log_print(F(":"));
    log_print(mac[3], 16);
    log_print(F(":"));
    log_print(mac[4], 16);
    log_print(F(":"));
    log_println(mac[5], 16);
}

void handle_op_dmx(char buffer[], int packet) {
    parsed_packets++;
    // Extract the universe
    uint16_t universe = (buffer[14] << 0) | (buffer[15] << 8);
    if (universe > UINT8_MAX) return;

    bool found = false;
    auto *store = static_cast<dmx_store *>(dmx_store_map->get(universe, &found));
    if (!found) {
        // Ignore out of scope universes
        return;
    }

    // Effectively blank the store
    packets.clear();

    // Copy all data into the 512 array, this pads out the packet if it didn't contain the full universe
    uint8_t static_full[512];
    store->export_last(static_full);
    memcpy(static_full, buffer + 18, min(512, packet - 18));

    log_println(F("Pushing to store"));
    // If we move it into the store early, then we can go and trash the static_full array rather than having to make a
    // new copy - saves 1 copy and 512 bytes in RAM
    store->assign(static_full);


    bool forced_full = false;
    size_t new_size;
    bool is_full = false;
    if (packets_since_synchronization++ > synchronization_frequency) {
        forced_full = true;
        packets_since_synchronization = 0;
        new_size = 512;
        is_full = true;
        log_println(F("Full synchronisation packet required"));
    } else {
        new_size = rf_dmx_t::compress(store, static_full);
        if (new_size == SIZE_MAX) {
            new_size = 512;
            is_full = true;
            log_println(F("Full packet"));
        } else {
            log_print(F("Packet compressed from 512 to "));
            log_print(new_size, 10);
            log_print(F(" giving a compression percentage of "));
            log_print(((512 - new_size) / 512.0) * 100.0, 3);
            log_println(F("%"));
        }
    }

    // Then work out how many packets this needs to go into
    size_t packet_count = ceil(new_size / (double) rf_dmx_t::PAYLOAD_SIZE);
    log_print(F("  Need to send "));
    log_print(packet_count, 10);
    log_print(F(" packets for universe "));
    log_println(universe, 10);

    spi_select_rf();
    set_as_transmitter(true);
    size_t debugging_pointer = 0;

    if (new_size == 0) {
        auto new_packet = rf_dmx_t(
                DEVICE_ID,
                packet_sequence_no++,
                0,
                DIFF,
                0,
                universe,
                0,
                nullptr
        );

        new_packet.set_start_flag();
        new_packet.set_termination_flag();

        size_t required_size = new_packet.required_size();

        uint8_t send_buffer[required_size];
        new_packet.serialize(send_buffer);
        if (!rf_node()->write(send_buffer, required_size, true)) {
            log_print(F("  [timed out]"));
        } else {
            log_print(F("."));
        }

#ifdef DEBUG_MASTER_OVER_UDP
        memcpy(debugging_buffer + debugging_pointer, send_buffer, required_size);
        debugging_pointer += required_size;
        debugging_buffer[debugging_pointer++] = '\xEF';
#endif

        log_print(F("  Packet 0 (nsz) with length "));
        log_print(required_size, 10);
        log_println(F(" transmitted"));
        dump_hex(send_buffer, required_size);
    } else {
        // Then run through the data and transmit
        size_t pointer = 0;
        for (size_t i = 0; i < packet_count; ++i) {
            size_t this_size = min(rf_dmx_t::PAYLOAD_SIZE, new_size - pointer);
            auto new_packet = rf_dmx_t(
                    DEVICE_ID,
                    packet_sequence_no++,
                    0,
                    is_full ? FULL : DIFF,
                    i,
                    universe,
                    this_size,
                    reinterpret_cast<char *>(static_full + pointer)
            );
            new_packet.ensure_not_deserialized();
            pointer += this_size;

            log_print(F("  Flags  "));
            if (i == 0) {
                new_packet.set_start_flag();
                log_print(F("S"));
            }
            if (i == packet_count - 1) {
                new_packet.set_termination_flag();
                log_print(F("T"));
            }
            if (i != 0 && i != packet_count - 1) {
                new_packet.set_continuation_flag();
                log_print(F("C"));
            }
            log_println("");

            size_t required_size = new_packet.required_size();

            uint8_t *target = packets.store(i, required_size);
            new_packet.serialize(target);

            auto t = rf_node();

            if (!t->write(target, required_size, true)) {
                log_print(F("  [timed out]"));
            } else {
                log_print(F(">"));
            }

#ifdef DEBUG_MASTER_OVER_UDP
            memcpy(debugging_buffer + debugging_pointer, target, required_size);
            debugging_pointer += required_size;
            debugging_buffer[debugging_pointer++] = '\xEF';
#endif

            log_print(F("  Packet "));
            log_print(i, 10);
            log_print(F(" with length "));
            log_print(this_size, 10);
            log_println(F(" transmitted"));
            log_print("of");
            log_println(packet_count, 10);
            log_print("fm");
            log_println(freeMemory(), 10);
        }
    }


#ifdef DEBUG_MASTER_OVER_UDP
    spi_select_ethernet();
    udp.beginPacket(desktop, METRICS_TARGET_PORT);
    udp.write(debugging_buffer, debugging_pointer);
    udp.endPacket();
#endif
}

void handle_op_poll(char buffer[], int length, IPAddress address) {
    size_t universe_count = 0;
    uint8_t *universes = storage.get_universes(&universe_count);

    for (size_t i = 0; i < universe_count; i += 4) {
        uint8_t span = min(4, universe_count - i);
        uint8_t write_buffer[239];

        // Header
        strcpy(reinterpret_cast<char *>(write_buffer), "Art-Net\0");

        // OpCode
        write_buffer[8] = UINT16_LE_FIRST_BYTE(ARTNET_OP_POLLREPLY);
        write_buffer[9] = UINT16_LE_SECOND_BYTE(ARTNET_OP_POLLREPLY);

        // IpAddress
        write_buffer[10] = ip[0];
        write_buffer[11] = ip[1];
        write_buffer[12] = ip[2];
        write_buffer[13] = ip[3];

        // Port
        write_buffer[14] = UINT16_LE_FIRST_BYTE(ARTNET_CONST_PORT);
        write_buffer[15] = UINT16_LE_SECOND_BYTE(ARTNET_CONST_PORT);

        // VersInfo
        write_buffer[16] = 0;
        write_buffer[17] = 0;

        // NetSwitch
        write_buffer[18] = (ARTNET_PORT_ADDRESS_BASE >> 8) & 0b01111111;

        // SubSwitch
        write_buffer[19] = (ARTNET_PORT_ADDRESS_BASE >> 4) & 0b00001111;

        // Oem
        write_buffer[20] = UINT16_BE_FIRST_BYTE(OEM_CODE);
        write_buffer[21] = UINT16_BE_SECOND_BYTE(OEM_CODE);

        // Ubea Version
        write_buffer[22] = 0;

        // Status1
        //     ┌┌----------- Indicators In Normal Mode
        //     || ┌┌-------- All Port-Address Set By Front Panel Controls
        //     || || ┌------ Not Implemented, Do Not Test
        //     || || | ┌---- Normal Firmware Boot
        //     || || | | ┌-- Not Capable of Remote Device Management (RDM)
        //     || || | | | ┌ UBEA Not Present or Corrupt
        // 0 b 11 01 0 0 0 0
        //   TODO: Swap when RDM is enabled
        write_buffer[23] = 0b11010000;

        // EstaMan
        write_buffer[24] = UINT16_BE_FIRST_BYTE(0x7FF0);
        write_buffer[25] = UINT16_BE_SECOND_BYTE(0x7FF0);

        // Short Name
        memset(write_buffer + 26, 0, 18);
        strcpy(reinterpret_cast<char *>(write_buffer + 26), "RFD Master Node\0");

        // Long Name
        memset(write_buffer + 44, 0, 64);
        strcpy(reinterpret_cast<char *>(write_buffer + 44), "RFD Master Node\0");

        // Node Report
        memset(write_buffer + 108, 0, 64);
        // TODO: any debug data here

        // Num Ports
        write_buffer[172] = 0;
        write_buffer[173] = span;

        // Port Types
        for (int j = 0; j < span; ++j) {
            write_buffer[174 + j] = 0b10000000;
        }

        // Good Input
        //   TODO: Can transmit receive errors per node via this field
        //   TODO: Make dynamic
        for (int j = 0; j < span; ++j) {
            write_buffer[178 + j] = 0b10000000;
        }

        // Good Output
        for (int j = 0; j < span; ++j) {
            write_buffer[182 + j] = 0b10000000;
        }

        // SwIn
        memset(write_buffer + 186, 0, 4);
        for (int j = 0; j < span; ++j) {
            write_buffer[186 + j] = universes[i + j] & 0x0F;
        }

        // SwOut
        memset(write_buffer + 190, 0, 4);
        for (int j = 0; j < span; ++j) {
            write_buffer[190 + j] = universes[i] & 0x0F;
        }

        // AcnPriority
        write_buffer[194] = 0;

        // SwMacro
        write_buffer[195] = 0;

        // SwRemote
        write_buffer[196] = 0;

        // Spare
        memset(write_buffer + 197, 0, 3);

        // Style
        write_buffer[200] = ARTNET_STYLE_NODE;

        // MAC
        for (int j = 0; j < 6; ++j) {
            write_buffer[201 + j] = mac[j];
        }

        // Bind IP
        for (int j = 0; j < 4; ++j) {
            write_buffer[207 + j] = ip[j];
        }

        // Bind Index
        write_buffer[211] = 1;

        // Status2
        //     ┌-------------- Node does not support control of RDM using ArtCommand
        //     | ┌------------ Node does not support switching of output style using ArtCommand
        //     | | ┌---------- Not squawking
        //     | | | ┌-------- Node cannot swap between Art-Net and sACN
        //     | | | | ┌------ Node supports 15 bit port-addresses
        //     | | | | | ┌---- Node does not support DHCP
        //     | | | | | | ┌-- Node IP is set manually
        //     | | | | | | | ┌ Product does not support web browser config
        // 0 b 0 0 0 0 1 0 0 0
        //   TODO: Swap when RDM is enabled
        write_buffer[212] = 0b00001000;

        // Good Output
        for (int j = 0; j < 4; ++j) {
            //     ┌-------------- RDM is disabled
            //     | ┌------------ Output style is delta
            //     | |      ┌----- Not used, set to zero
            //     | | {         }
            // 0 b 0 1 0 0 0 0 0 0
            //   TODO: Swap when RDM is enabled
            write_buffer[213 + j] = 0b01000000;
        }

        // Status 3
        //     ┌┌------------ Failsafe state is Hold Last State
        //     || ┌---------- Node does not support fail-over
        //     || | ┌-------- Node does not support LLRP
        //     || | | ┌------ Node does not support switching port direction
        //     || | | |   ┌-- Not used, set to zero
        //     || | | | {   }
        // 0 b 00 0 0 1 0 0 0
        write_buffer[217] = 0b00000000;

        // Default Responder UID + Filler
        memset(write_buffer + 218, 0, 21);

        // TODO: broadcast is not allowed
        udp.beginPacket(address, ARTNET_PORT);
        udp.write(write_buffer, 239);
        udp.endPacket();
    }
}

void handle_artnet(char buffer[], int packet, IPAddress address) {
    uint16_t opcode = (buffer[8] << 0) | (buffer[9] << 8);
    switch (opcode) {
        case ARTNET_OP_DMX:
            handle_op_dmx(buffer, packet);
            break;
        case ARTNET_OP_POLL:
            handle_op_poll(buffer, packet, address);
            break;
    }
}

void handle_udp() {
    delay(15);
    spi_select_ethernet();
    udp.parsePacket();
    if (udp.available() > 0) {
        int packet = udp.read(udp_buffer, UDP_TX_PACKET_MAX_SIZE);
        IPAddress address = udp.remoteIP();
        if (packet == 0) return;
        if (8 >= strlen(udp_buffer) && (strncmp("Art-Net\0", udp_buffer, 8) == 0)) {
            handle_artnet(udp_buffer, packet, address);
        }
    }
}

void handle_rf() {
    uint8_t pipe;
    byte buffer[rf_node()->getPayloadSize()];
    if (rf_node()->available(&pipe)) {
        uint8_t payload = rf_node()->getPayloadSize();
        rf_node()->read(&buffer, payload);

        handler->parse(buffer, payload);
    }
}

void handle_checkin(rf_checkin_t *checkin, rf_proto_handler *) {
    // TODO: actually do the checkin
    storage.checkin(checkin->device_id, checkin->universe);
}

void handle_request(rf_request_t *request, rf_proto_handler *self) {
    if (requested_packets_since_metrics == UINT32_MAX) rpsm_overflowed = true;
    requested_packets_since_metrics++;

    uint8_t length = 0;
    uint8_t *data = packets.get(request->offset, length);
    log_print(F("request for offset "));
    log_println(request->offset, 10);
    if (data != nullptr) {
        rf_dmx_t::set_resend_flag_on_serialised(data);

        set_as_transmitter(true);
        rf_node()->write(data, length, true);
        set_as_receiver(true);
    }

}

void panic(int line, rf_proto_handler *self) {
    log_print(F("The RF protocol handler requested a global panic! This happened on line "));
    log_print(line, 10);
    log_println(F(" - calling global panic"));
    global_panic();
}

void setup() {
    global_setup();

    log_println(F("Running on a master node - configuring ethernet and rf"));
    configure_ethernet();
    configure_rf();
    set_as_transmitter(false);
    log_rf_debugging_info();

    dmx_store_map = new array_map(5);
    for (int i = 0; i < sizeof(universe_filter); ++i) {
        auto *store = new dmx_store();
        dmx_store_map->set(universe_filter[i], reinterpret_cast<void *>(store));
    }

    handler = new rf_proto_handler(
            nullptr,
            nullptr,
            handle_checkin,
            nullptr,
            handle_request,
            panic,
            0,
            UINT8_MAX
    );

    log_println("Setup is complete - beginning loop");
}

/**
 * Sends dropped packet count, overflow status and number of nodes to the metrics IP and then resets the values.
 * If metrics are disabled it will only reset the values
 */
void send_metrics() {
#ifndef DISABLE_METRICS
    uint8_t nodes = storage.length;
    uint8_t data[6];
    data[0] = UINT32_BE_FIRST_BYTE(requested_packets_since_metrics);
    data[1] = UINT32_BE_SECOND_BYTE(requested_packets_since_metrics);
    data[2] = UINT32_BE_THIRD_BYTE(requested_packets_since_metrics);
    data[3] = UINT32_BE_FOURTH_BYTE(requested_packets_since_metrics);
    data[4] = rpsm_overflowed ? 1 : 0;
    data[5] = nodes;

    spi_select_ethernet();
    udp.beginPacket(desktop, METRICS_TARGET_PORT);
    udp.write(data, 6);
    udp.endPacket();
    spi_select_rf();

#endif
    requested_packets_since_metrics = 0;
    rpsm_overflowed = false;
}

void loop() {
    handle_udp();

    spi_select_rf();
    set_as_receiver(true);
    handle_rf();
    handler->tick();

    uint64_t now = millis();
    if (now - last_metrics_push > millis_between_metrics) {
        send_metrics();
        last_metrics_push = now;
    }
}

#endif
