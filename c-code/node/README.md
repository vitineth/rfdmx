# Hardware Implementation

This is a basic, proof of concept implementation on cheap, consumer grade hardware. This project has been tested only with an Arduino Mega2560 as the master and Arduion Uno R3 as the node. Due to the memory limitations, this is an incredibly limited implementation and therefore is missing some aspects which should be implemented in a real system. Full detail on this implementation should be derived from the report itself, primarily in the implementation section. 

## Documentation

Doxygen is partially supported - it will generate docs for shared libraries but struggles with the actual implementation code. Just run `doxygen` from the root folder and it will generate pages in `_docs`. It does however struggle with primary entrypoints so may not generate files for master.cpp etc. However comments are included in the files themselves if needed. 

## Building and Running

This uses PlatformIO and relies heavily on build flags. It is recommended not to use this directly, but interact with it through the rfdmx command line tool which is just a wrapper over PlatformIO that does useful stuff like setting build flags automatically with and required conversions. However, for clarity - there are 5 environments defined in platformio.ini:

* uno-node: the standard receiver implementation
* uno-master: the standard transmitter implementation
* metrics-master: the transmitter in the metrics gathering system which will push data
* metrics-node: the receiver/ack-er in the metrics gathering system
* test: a native based test of some specific aspects of the project

## lib

Lib contains general purpose libraries not dedicated to a single implementation mode

| Folder         | Purpose                                                                                                                                                                                                                                                  |
|----------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `array-map`    | A naive implementation of a map which stores keys and values in two expanding arrays. This can be used for general number to pointer mapping, in this case mapping universes to their dmx-store instances.  |
| `dmx-store`    | A store for multiple frames of DMX data which can be compared and tested to detect changes                                                                                                                                                               |
| `logging`      | General purpose logging via `Serial.print(ln)` on embedded hardware and using `std::cout` on native systems. This contains some customisation as well depending on system such as `DISABLE_MASTER_LOGGING` and `TRY_BLOCKING_SERIAL` for different modes |
| `node-store`   | Basic linked list implementation that holds node ids, universes and checkin times for storing and monitoring nodes on the network                                                                                                                        |
| `packet-store` | Simple fixed length packet storage which allows storing and resending packets and a capacity safe way rather than just a raw array                                                                                                                       |
| `protocol`     | The actual protocol implementation including the protocol definitions and utility parser used by the actual implementations                                                                                                                              |
| `rf`           | Contains general interfaces to the rf interface providing general functions for swapping modes and printing diagnostics                                                                                                                                  |
| `spi`          | Utiltiy functions for the master to swap SPI devices                                                                                                                                                                                                     |

## src

| Folder               | Purpose                                                                        |
|----------------------|--------------------------------------------------------------------------------|
| `global`             | Shared code for the implementations that provides some general setup and tools |
| `master`             | Code for the master transmitter                                                |
| `metric`             | Shared code for the metric system, master and node                             |
| `node`               | Code for the node receiver                                                     |
| `artnet-constants.h` | Some shared constants from the artnet specification                            |

## test

Test is a little bit disorganised but contains individual tests for simulating the protocol, the node storage and the protocol in general. More detail about these tests and their shortcomings are given in the report and will not be repeated here.