import * as crypto from "crypto";
import * as fs from "fs";
import * as tty from "tty";

/**
 * The time at which this was first imported, used to produce offsets in the logger
 */
let launch = performance.now();

// These are alias types that are meant just for indication, they do not actually change any behaviours

export type Uint8 = number;
export type Uint16 = number;
export type Uint32 = number;
export type Uint64 = bigint;

// Constants!

export const Uint8Min: Uint8 = 0;
export const Uint16Min: Uint16 = 0;
export const Uint32Min: Uint32 = 0;
export const Uint64Min: Uint64 = BigInt(0);
export const Uint8Max: Uint8 = (2 ** 8) - 1;
export const Uint16Max: Uint16 = (2 ** 16) - 1;
export const Uint32Max: Uint32 = (2 ** 32) - 1;
export const Uint64Max: Uint64 = (BigInt(2) ** BigInt(8)) - BigInt(1);

/**
 * Generate a number in range 0 - {@link Uint8Max}
 */
export function genUint8(): Uint8 {
    return Math.round(Math.random() * Uint8Max);
}

/**
 * Generate a number in range 0 - {@link Uint16Max}
 */
export function genUint16(): Uint16 {
    return Math.round(Math.random() * Uint16Max);
}

/**
 * Generate a number in range 0 - {@link Uint32Max}
 */
export function genUint32(): Uint32 {
    return Math.round(Math.random() * Uint32Max);
}

/**
 * Generate a number in range 0 - {@link Uint64Max}
 */
export function genUint64(): Uint64 {
    // This might be less bad
    return BigInt(BigInt(genUint32()) << BigInt(32)) & BigInt(genUint32());
}

/**
 * Returns a random buffer of given number of bytes generated via crypto
 * @param length the length of the buffer to return
 */
export function genBuffer(length: number) {
    return crypto.randomBytes(length);
}

/**
 * A simple logger that is annotated with time offset and a label. This is controlled via environment variables and
 * either NOPs, writes to console or to a file
 */
export class Logger {
    /**
     * The unique ID for the device that is logging through this, recommended to just use 0 for any not needed
     * @private
     */
    private readonly _deviceID: number;
    /**
     * The label of the logger which will be included in all messages
     * @private
     */
    private readonly _name: string;
    /**
     * The writer through which log messages will actually be sent
     * @private
     */
    private readonly _writer: fs.WriteStream | tty.WriteStream | null;

    /**
     * Creates a new logger with the provided annotations. Where this logger will go will depend on the state of
     * environment variables at this call. If USE_NULL is set, this writer will do nothing on logging calls. If not set,
     * then if USE_LOG_FILE is set, it will create an fs writer to the string contained in USE_LOG_FILE. If not, it will
     * use process.stdout
     * @param deviceID the device ID to include in all messages
     * @param name the name to include in all messsages
     */
    constructor(deviceID: number, name: string) {
        this._deviceID = deviceID;
        this._name = name;

        if (process.env.USE_NULL) {
            this._writer = null;
        } else {
            if (process.env.USE_LOG_FILE) {
                this._writer = fs.createWriteStream(process.env.USE_LOG_FILE, {flags: 'a'});
            } else {
                this._writer = process.stdout;
            }
        }
    }

    /**
     * Logs a message in the form [time s][name][device id]: message
     * @param message the message to log
     * @param newLine if a new line should be written at the end of this line
     */
    public log(message: string, newLine: boolean = true) {
        this.raw(`[${Math.round((performance.now() - launch) / 1000)}s][${this._name}][${this._deviceID}]: ${message}${newLine ? '\n' : ''}`);
    }

    /**
     * This writes the message given without any decoration or new lines to the output. This is good to appending to a line
     * @param message the message to write
     */
    public raw(message: string) {
        if (this._writer !== null) {
            if (this._writer instanceof fs.WriteStream) this._writer.write(message);
            else this._writer.write(message);
        }
    }
}
