import {testRFCheckin, testRFDmx, testRFRequest, testRFRoot} from "./protocol";
import {isMainThread, Worker, workerData, parentPort} from "worker_threads";
import {Simulator} from "./simulator";
import {RandomProvider} from "./simulator/provider";
import * as fs from "fs";
import prompt from "prompt";
import {clearInterval} from "timers";

function test() {
    testRFRoot();
    testRFCheckin();
    testRFRequest();
    testRFDmx();
}

test();

function mkdir(path: string) {
    try {
        fs.mkdirSync(path);
    } catch (e) {
        console.warn(`Didn't create ${path} - ${(e as any).message ?? 'Unknown'}`);
    }
}

if (isMainThread) {
    // Running on the main thread, this means it needs to dispatch work to the subthreads

    // Define how many samples should be taken of FPS between what range
    const SAMPLES = 20;
    const MIN = 1;
    const MAX = 200;
    const STEP = (MAX - MIN) / (SAMPLES + 1);

    // Ensure required folders are present, these will print messages if they already exist but thats fine
    mkdir('./sims');
    mkdir('./sim');
    mkdir('./sim/data');
    mkdir('./sim/logs');

    // The number of threads that have resolved
    let complete = 0;
    // If the entire simulation has finished
    let done = false;
    // The number of threads created
    let created = 0;

    for (let i = MIN; i < MAX; i = Math.round(i + STEP)) {
        created++;

        // Create a worker with this file which will take the other branch of this containing if statement
        const worker = new Worker(__filename, {
            workerData: i,
            stdout: false,
            stderr: true,
            stdin: false,
        });

        // A message is received only when the processing is done and contains the generated result file and log file. This is just printed out as
        // additional processing is done when the worker exits
        worker.on('message', ({resultFile, logs}: { resultFile: string, logs: string }) => {
            console.log(`Processing has completed for FPS ${i}, written to = ${resultFile}, logs written to ${logs}`);
        });

        // Errors are just logged, this may need to be handled differently. However if the error kills to worker, it will still be marked as complete
        worker.on('error', (err) => {
            console.error(`Processing failed for FPS ${i}`, err);
        });

        // On exit, print a note and if all workers have ended but we aren't marked done, get the name of the test and move all the files. Then finally be done and terminate
        worker.on('exit', (code) => {
            if (code !== 0) console.error(`Processing failed for FPS ${i}, exit code ${code}`);
            if (++complete >= created && !done) {
                done = true;
                prompt.start();
                prompt.get(['target']).then((d) => {
                    fs.renameSync('./sim', `./sims/${d.target}`);
                    prompt.stop();
                })
            }
        });

        // It begins!
        console.log(`Spawned worker for FPS ${i}`);
    }

    // Print a # every time a test _should have completed_. This may not always be right but its an indicator that time
    // is passing and this hasn't just stalled (ish). This is definitely not a progress bar
    let n = 0;
    process.stdout.write('|');
    const thisInterval = setInterval(() => {
        n++;
        process.stdout.write('#');
        if (n > (100 / 5)) clearInterval(thisInterval);
    }, 15000);
} else {
    // Executing in a worker node, therefore run the actual test
    const resultFile = `./sim/data/fps-${workerData}.csv`;

    // IF YOU WANT LOGGING, SWAP THE COMMENTS HERE
    // process.env.USE_LOG_FILE = `./sim/logs/fps-${workerData}.log`;
    process.env.USE_NULL = 'yes';

    (async () => {
        const lossMap: Record<number, number> = {};

        const writer = fs.createWriteStream(resultFile);
        writer.write("FPS,LP,LR\n");

        // For each loss from 0-1 with 2.5% stops, run a complete simulation using randomised data at the frame rate
        // this worker was started with
        for (let loss = 0; loss < 1; loss += 0.025) {
            const simulator = new Simulator(loss);
            simulator.registerMaster(new RandomProvider(workerData));
            simulator.registerNode(1);
            const losses = await simulator.launch();

            const average = losses.reduce((a, b) => a + b, 0) / losses.length;
            lossMap[loss] = average;
            writer.write(`${workerData},${loss},${average}\n`);
        }

        writer.close();

        // When done write to the parent port and be done! Woop woop!
        parentPort?.postMessage({
            resultFile: resultFile,
            logs: process.env.USE_LOG_FILE,
        });

    })();
}
