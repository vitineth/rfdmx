import {Uint8, Uint16, Uint32, Uint64, genUint8, genUint32, genUint16, genBuffer, Logger} from "../utils";
import {off} from "node-notifier";
import {start} from "repl";
import {PercentageDualSignal} from "../simulator/signals";

// Note:
// =====
// Only some portions of this file will be commented, as this is generally a straight copy from
// the C++ implementation, it is assumed that you can follow the comments in that file instead!

/**
 * Represents a class that is wire serializable
 */
export abstract class Serializable {
    /**
     * Convert a packet to a buffer ready to be transmitted on the wire
     */
    abstract serialize(): Buffer;
}

export enum PacketType {
    DMX = 0,
    CHECKIN = 1,
    REQUEST = 2,
}

export enum CompressionType {
    FULL = 0,
    DIFF = 1,
    USE_FIRST_BYTE = 3,
}

/**
 * Packets are simply serializable entities with a determinable size
 */
abstract class Packet extends Serializable {
    abstract requiredSize(): Uint64;
}

export class RFRoot extends Packet {
    protected _deviceID: Uint8;
    protected _packetType: PacketType;
    protected _sequence: Uint8;
    protected _flags: Uint8;

    constructor(deviceID: Uint8, packetType: PacketType, sequence: Uint8, flags: Uint8) {
        super();
        this._deviceID = deviceID;
        this._packetType = packetType;
        this._sequence = sequence;
        this._flags = flags;
    }

    requiredSize(): Uint64 {
        return BigInt(4);
    };

    override serialize(): Buffer {
        const b = Buffer.alloc(4);
        b.writeUint8(this._deviceID, 0);
        b.writeUint8(this._packetType, 1);
        b.writeUint8(this._sequence, 2);
        b.writeUint8(this._flags, 3);
        return b;
    };

    static deserialize(b: Buffer): RFRoot {
        return new RFRoot(
            b.readUint8(0),
            b.readUint8(1),
            b.readUint8(2),
            b.readUint8(3),
        )
    }


    get deviceID(): Uint8 {
        return this._deviceID;
    }

    get packetType(): PacketType {
        return this._packetType;
    }

    get sequence(): Uint8 {
        return this._sequence;
    }

    get flags(): Uint8 {
        return this._flags;
    }

    equals(rf: RFRoot): boolean {
        return rf._flags === this._flags && rf._deviceID === this._deviceID && rf._packetType === this._packetType && rf._sequence === this._sequence;
    }
}

/**
 * Asserts that randomly constructed RFRoot packets will serialise and deserialize into the same data
 */
export function testRFRoot() {
    for (let i = 0; i < 1000; i++) {
        const construct = new RFRoot(
            genUint8(),
            genUint8(),
            genUint8(),
            genUint8(),
        );

        const serial = construct.serialize();
        const destruct = RFRoot.deserialize(serial);

        if (!destruct.equals(construct)) {
            console.log('Destruct = ', destruct);
            console.log('Construct = ', construct);
            throw new Error('Invalid - failed comparison');
        }
        if (!serial.equals(destruct.serialize())) throw new Error('Invalid - failed buffer comparison');
    }
}

export class RFCheckin extends RFRoot {
    private readonly _universe: Uint8;

    constructor(deviceID: Uint8, sequence: Uint8, flags: Uint8, universe: Uint8) {
        super(deviceID, PacketType.CHECKIN, sequence, flags);
        this._universe = universe;
    }


    override requiredSize(): Uint64 {
        return super.requiredSize() + BigInt(1);
    }

    override serialize(): Buffer {
        return Buffer.concat([super.serialize(), Buffer.of(this._universe)]);
    }

    static override deserialize(b: Buffer, root?: RFRoot): RFCheckin {
        let rt = root;
        if (!rt) {
            rt = RFRoot.deserialize(b.subarray(0, 4));
        }

        return new RFCheckin(
            rt.deviceID,
            rt.sequence,
            rt.flags,
            b.readUint8(4),
        );
    }


    get universe(): Uint8 {
        return this._universe;
    }

    override equals(rf: RFCheckin): boolean {
        return super.equals(rf) && rf._universe === this._universe;
    }
}

/**
 * Asserts that randomly constructed RFRoot packets will serialise and deserialize into the same data
 */
export function testRFCheckin() {
    for (let i = 0; i < 1000; i++) {
        const construct = new RFCheckin(
            genUint8(),
            genUint8(),
            genUint8(),
            genUint8(),
        );

        const serial = construct.serialize();
        const destruct = RFCheckin.deserialize(serial);

        if (!destruct.equals(construct)) {
            console.log('Destruct = ', destruct);
            console.log('Construct = ', construct);
            throw new Error('Invalid - failed comparison');
        }
        if (!serial.equals(destruct.serialize())) throw new Error('Invalid - failed buffer comparison');
    }
}

export class RFRequest extends RFRoot {
    private readonly _requestedOffset: Uint8;
    private readonly _requestedSequence: Uint8;

    constructor(deviceID: Uint8, sequence: Uint8, flags: Uint8, requestedOffset: Uint8, requestedSequence: Uint8) {
        super(deviceID, PacketType.REQUEST, sequence, flags);
        this._requestedOffset = requestedOffset;
        this._requestedSequence = requestedSequence;
    }

    override requiredSize(): Uint64 {
        return super.requiredSize() + BigInt(2);
    }

    override serialize(): Buffer {
        return Buffer.concat([super.serialize(), Buffer.of(this._requestedOffset, this._requestedSequence)]);
    }

    static override deserialize(b: Buffer, root?: RFRoot): RFRequest {
        let rt = root;
        if (!rt) {
            rt = RFRoot.deserialize(b.subarray(0, 4));
        }

        return new RFRequest(
            rt.deviceID,
            rt.sequence,
            rt.flags,
            b.readUint8(4),
            b.readUint8(5),
        );
    }

    get requestedOffset(): Uint8 {
        return this._requestedOffset;
    }

    get requestedSequence(): Uint8 {
        return this._requestedSequence;
    }

    override equals(rf: RFRequest): boolean {
        return super.equals(rf) && rf._requestedSequence === this._requestedSequence && rf._requestedOffset === this._requestedOffset;
    }
}

/**
 * Asserts that randomly constructed RFRoot packets will serialise and deserialize into the same data
 */
export function testRFRequest() {
    for (let i = 0; i < 1000; i++) {
        const construct = new RFRequest(
            genUint8(),
            genUint8(),
            genUint8(),
            genUint8(),
            genUint8(),
        );

        const serial = construct.serialize();
        const destruct = RFRequest.deserialize(serial);

        if (!destruct.equals(construct)) {
            console.log('Destruct = ', destruct);
            console.log('Construct = ', construct);
            throw new Error('Invalid - failed comparison');
        }
        if (!serial.equals(destruct.serialize())) throw new Error('Invalid - failed buffer comparison');
    }
}

export class RFDmx extends RFRoot {
    private static readonly _START_MASK = 0b10000000;
    private static readonly _CONTINUATION_MASK = 0b01000000;
    private static readonly _TERMINATION_MASK = 0b00100000;
    private static readonly _RESEND_MASK = 0b00010000;
    private static readonly _PAYLOAD_SIZE = 23;

    private readonly _compressionType: CompressionType;
    private readonly _size: Uint16;
    private readonly _universe: Uint8;
    private readonly _data: Buffer;
    private readonly _offset: Uint8;


    constructor(deviceID: Uint8, sequence: Uint8, flags: Uint8, compressionType: CompressionType, size: Uint16, universe: Uint8, data: Buffer, offset: Uint8) {
        super(deviceID, PacketType.DMX, sequence, flags);
        this._compressionType = compressionType;
        this._size = size;
        this._universe = universe;
        this._data = data;
        this._offset = offset;
    }

    setStartFlag() {
        this._flags |= RFDmx._START_MASK;
    }

    setContinueFlag() {
        this._flags |= RFDmx._CONTINUATION_MASK;
    }

    setTerminateFlag() {
        this._flags |= RFDmx._TERMINATION_MASK;
    }

    setResentFlag() {
        this._flags |= RFDmx._RESEND_MASK;
    }

    hasStartFlag(): boolean {
        return (this._flags & RFDmx._START_MASK) != 0;
    }

    hasContinueFlag(): boolean {
        return (this._flags & RFDmx._CONTINUATION_MASK) != 0;
    }

    hasTerminateFlag(): boolean {
        return (this._flags & RFDmx._TERMINATION_MASK) != 0;
    }

    hasResendFlag(): boolean {
        return (this._flags & RFDmx._RESEND_MASK) != 0;
    }

    static setResendOnBuffer(b: Buffer) {
        b.writeUint8(b.readUint8(3) | RFDmx._RESEND_MASK, 3);
    }

    override serialize(): Buffer {
        return Buffer.concat([
            super.serialize(),
            Buffer.of(
                (this._compressionType & 0b00000011) | ((this._offset << 2) & 0b11111100),
                (this._size >> 8) & 0xFF,
                this._size & 0xFF,
                this._universe,
            ),
            this._data,
        ]);
    }

    static override deserialize(b: Buffer, root?: RFRoot): RFDmx {
        let rt = root;
        if (!rt) {
            rt = RFRoot.deserialize(b.subarray(0, 4));
        }

        /*
        ```
           │ 00 01 02 03 04 05 06 07 08 09 0a 0b 0c 0d 0e 0f
        ───┼─────────────────────────────────────────────────
        00 │ <   root  > CO SIZE  UN DATA...
                         └│ └│ └│ └│ └└─ DMX Data (uint8_t *)
                          |  |  |  └──── Universe (uint8_t)
                          |  └──└─────── Data Size (uint16_t BE)
                          └───────────── Compression and Offset
        ```
         */

        return new RFDmx(
            rt.deviceID,
            rt.sequence,
            rt.flags,
            b.readUint8(4) & 0b00000011,
            b.readUint16BE(5),
            b.readUint8(7),
            b.subarray(8),
            (b.readUint8(4) & 0b11111100) >> 2,
        );
    }

    static get PAYLOAD_SIZE(): number {
        return this._PAYLOAD_SIZE;
    }

    get compressionType(): CompressionType {
        return this._compressionType;
    }

    get size(): Uint16 {
        return this._size;
    }

    get universe(): Uint8 {
        return this._universe;
    }

    get data(): Buffer {
        return this._data;
    }

    get offset(): Uint8 {
        return this._offset;
    }

    override equals(rf: RFDmx): boolean {
        return super.equals(rf)
            && rf._data.equals(this._data)
            && rf._compressionType === this._compressionType
            && rf._size === this._size
            && rf._universe === this._universe
            && rf._offset === this._offset;
    }
}


/**
 * Asserts that randomly constructed RFRoot packets will serialise and deserialize into the same data
 */
export function testRFDmx() {
    for (let i = 0; i < 1000; i++) {
        const size = genUint16();
        const construct = new RFDmx(
            genUint8(),
            genUint8(),
            genUint8(),
            Math.round(Math.random() * 3),
            size,
            genUint8(),
            genBuffer(size),
            Math.min(genUint8(), (2**6)-1),
        );

        const serial = construct.serialize();
        const destruct = RFDmx.deserialize(serial);

        if (!destruct.equals(construct)) {
            console.log('Destruct = ', destruct);
            console.log('Construct = ', construct);
            throw new Error('Invalid - failed comparison');
        }
        if (!serial.equals(destruct.serialize())) throw new Error('Invalid - failed buffer comparison');
    }
}

/**
 * Rather than using an integer and setting/clearing bits as done in the C++ version, this exploits the
 * more plentiful memory available on the system this is running on and holds the bitset as an actual
 * set and provides some more user friendly interfaces to setting and clearing bits than working with
 * masks.
 */
class BitSet {
    /**
     * The set of indexes that are are currently active on this bitset
     * @private
     */
    private _map: Set<number> = new Set();

    /**
     * Sets the bit at index provided, idempotent
     * @param v the value to set
     */
    public set(v: number) {
        this._map.add(v);
    }

    /**
     * Unsets a bit at the index provided, this should be idempotent
     * @param v the value to clear if present
     */
    public clear(v: number) {
        this._map.delete(v);
    }

    /**
     * Returns if the requested bit has been set
     * @param v the value to lookup
     */
    public has(v: number): boolean {
        return this._map.has(v);
    }

    /**
     * Returns if any bits are set
     */
    public any(): boolean {
        return this._map.size !== 0;
    }

    /**
     * Unsets all bits currently stored in the set
     */
    public reset() {
        this._map.clear();
    }

    /**
     * Returns the indexes of all bits currently present in the set
     */
    public present(): number[] {
        return [...this._map];
    }

    /**
     * Returns the currently set bits as an array string
     */
    public toString(): string{
        return this.present().toString();
    }
}

export class ProtoHandler {
    protected static readonly MAX_RESEND_REQUESTS: Uint8 = 232;
    protected static readonly DELAY_BETWEEN_REQUESTS: Uint8 = 0;
    protected static readonly MULTIPART_TIME_BETWEEN_ASSUME_DROPPED: Uint32 = 2;

    private _logger: {log: (func: string, message: string) => void, inner: Logger};

    protected reconstructionBuffer: Buffer = Buffer.alloc(550);
    protected reconstructionReceiveMarkers: BitSet = new BitSet();

    protected lastSeenSequence: Uint8 = 0;
    protected lastSeenOffset: Uint8 = 0;

    protected hasSeenAny: boolean = false;

    protected multipartTerminateOffset: undefined | number = undefined;
    protected multipartInFlight: boolean = false;
    protected multipartDataSize: Uint32 = 0;
    protected multipartRequestedResentMarkers: BitSet = new BitSet();
    protected multipartStartSequence: Uint8 = 0;
    protected multipartLastSeenTime: Uint32 = 0;
    protected multipartLastTimeRequested: Uint32 = 0;
    protected multipartRequestedResendTotal: Uint8 = 0;
    protected seenStartingSequences: (undefined | number)[] = [undefined, undefined, undefined];
    protected seenStartingSequencesPointer: Uint8 = 0;

    protected requestResend?: (sequenceID: Uint8, offset: Uint8, self: ProtoHandler) => void;
    protected requestResends?: (start: Uint8, offsets: Uint8[], self: ProtoHandler) => void;
    protected handleDmx?: (dmx: RFDmx, self: ProtoHandler) => void;
    protected handleCheckin?: (checkin: RFCheckin, self: ProtoHandler) => void;
    protected handleRequest?: (request: RFRequest, self: ProtoHandler) => void;
    protected onRootParse?: (root: RFRoot, self: ProtoHandler) => void;
    protected panic: (self: ProtoHandler) => void;

    protected universeFilter: Uint8;
    protected deviceIDFilter?: Uint8;

    protected tracker?: PercentageDualSignal;

    constructor(
        logger: Logger,
        requestResend: ((sequenceID: Uint8, offset: Uint8, self: ProtoHandler) => void)|undefined,
        requestResends: ((start: Uint8, offsets: Uint8[], self: ProtoHandler) => void) | undefined,
        handleDmx: ((dmx: RFDmx, self: ProtoHandler) => void) | undefined,
        handleCheckin: ((checkin: RFCheckin, self: ProtoHandler) => void) | undefined,
        handleRequest: ((request: RFRequest, self: ProtoHandler) => void) | undefined,
        onRootParse: ((root: RFRoot, self: ProtoHandler) => void) | undefined,
        panic: (self: ProtoHandler) => void,
        universeFilter: Uint8,
        deviceIDFilter?: Uint8,
        tracker?: PercentageDualSignal,
    ) {
        this._logger = {log: (f, m) => logger.log(`[proto:${f}]: ${m}`), inner: logger}
        this.requestResend = requestResend;
        this.requestResends = requestResends;
        this.handleDmx = handleDmx;
        this.handleCheckin = handleCheckin;
        this.handleRequest = handleRequest;
        this.onRootParse = onRootParse;
        this.panic = panic;
        this.universeFilter = universeFilter;
        this.deviceIDFilter = deviceIDFilter;
        this.tracker = tracker;
    }

    private parseDMX(root: RFRoot, buffer: Buffer) {
        if (this.handleDmx === undefined) return;

        const dmx = RFDmx.deserialize(buffer);
        this.lastSeenOffset = dmx.offset;

        this._logger.inner.raw(`[parseDMX] offset = ${dmx.offset}  `);
        if (dmx.hasStartFlag()) this._logger.inner.raw('S');
        if (dmx.hasContinueFlag()) this._logger.inner.raw('C');
        if (dmx.hasTerminateFlag()) this._logger.inner.raw('T');
        if (dmx.hasResendFlag()) this._logger.inner.raw('R');
        this._logger.inner.raw("\n");

        if (dmx.universe != this.universeFilter) {
            this._logger.log("parseDMX", "failed universe filter");
            return;
        }

        let startIndex = dmx.sequence - dmx.offset;
        if (dmx.hasResendFlag()) {
            this._logger.log('parseDMX',`Start Index = ${startIndex}`);

            if (this.seenStartingSequences.some((e) => e!== undefined && e === startIndex)) {
                this._logger.log("parseDMX", `Ignoring as start index has been seen ${startIndex} ${this.seenStartingSequences}`)
                return;
            }

            this.multipartRequestedResentMarkers.clear(dmx.offset);
            if (this.reconstructionReceiveMarkers.has(dmx.offset)) {
                this._logger.log('parseDMX',`Skipping ${dmx.offset} as it has already been seen`);
                return;
            }

            this._logger.log('parseDMX',`Receive flags = ${this.reconstructionReceiveMarkers}`);
            this._logger.log('parseDMX',`Terminate Offset = ${this.multipartTerminateOffset}`);
            this._logger.log('parseDMX', `Resent Markers = ${this.multipartRequestedResentMarkers}`);

            if (!this.multipartRequestedResentMarkers.any() && this.multipartTerminateOffset !== undefined) {
                this._logger.log('parseDMX','Packet is now complete, resolved');
                dmx.setTerminateFlag();
            }
        }

        if (this.multipartInFlight) {
            this.reconstructionReceiveMarkers.set(dmx.offset);
            if (dmx.offset > 0 && !this.reconstructionReceiveMarkers.has(dmx.offset - 1)) {
                this._logger.inner.raw('[parseDmx] Missing packets, requesting: ');
                let o: number[] = [];
                for (let i = 0; i < dmx.offset; i++) {
                    if (!this.reconstructionReceiveMarkers.has(i) && !this.multipartRequestedResentMarkers.has(i)) {
                        this.multipartRequestedResentMarkers.set(i);
                        // this.safeResend(startIndex + i, i, false);
                        o.push(i);
                        this._logger.inner.raw(`${i}, `);
                    }
                }
                this.batchResend(startIndex, o, false);
                this._logger.inner.raw('');
            }
        }

        if (dmx.hasStartFlag()) {
            for (let i = 0; i < this.seenStartingSequences.length; i++) {
                if (this.seenStartingSequences[i] === startIndex) this.seenStartingSequences[i] = undefined;
            }

            if (this.multipartInFlight) {
                this.tracker?.numerator.inc();
                this._logger.log("parseDMX", `WARNING: multipart in flight was true on a start packet, received = ${this.reconstructionReceiveMarkers} - waiting ${this.multipartRequestedResentMarkers}`);
            }

            this.tracker?.denominator.inc();
            this.reconstructionReceiveMarkers.reset();
            this.multipartTerminateOffset = undefined;
            this.multipartInFlight = false;
            this.multipartDataSize = 0;
            this.multipartRequestedResentMarkers.reset();
            this.multipartStartSequence = 0;
            this.multipartLastTimeRequested = 0;
            this.multipartRequestedResendTotal = 0;
            this.reconstructionReceiveMarkers.set(0);

            if (!dmx.hasTerminateFlag()) {
                this.multipartStartSequence = dmx.sequence;
                this.multipartInFlight = true;
                this.multipartLastSeenTime = performance.now();
            }

            dmx.data.copy(this.reconstructionBuffer);
            this.multipartDataSize = Math.max(this.multipartDataSize, dmx.size);
        }

        if (dmx.hasContinueFlag()) {
            if (!this.multipartInFlight) {
                this.multipartInFlight = true;
                this.reconstructionReceiveMarkers.set(dmx.offset);
                this._logger.log('parseDMX', 'Missing packets, requesting: ');
                let o: number[] = [];
                for (let i = 0; i < dmx.offset; i++) {
                    if (!this.reconstructionReceiveMarkers.has(i) && !this.multipartRequestedResentMarkers.has(i)) {
                        this.multipartRequestedResentMarkers.set(dmx.offset);
                        o.push(i);
                        // this.safeResend(startIndex + i, i, false);
                    }
                }
                this.batchResend(startIndex, o, false);
            }

            this.multipartLastSeenTime = performance.now();
            dmx.data.copy(this.reconstructionBuffer, dmx.offset * RFDmx.PAYLOAD_SIZE);
            this.reconstructionReceiveMarkers.set(dmx.offset);
            this.multipartDataSize = Math.max(this.multipartDataSize, (dmx.offset * RFDmx.PAYLOAD_SIZE) + dmx.data.length);
        }

        if (dmx.hasTerminateFlag()) {
            if (!dmx.hasStartFlag() && !this.multipartInFlight) {
                this._logger.log('parseDMX','Missing packets, requesting: ');
                let o: number[] = [];
                for (let i = 0; i < dmx.offset; i++) {
                    if (!this.reconstructionReceiveMarkers.has(i) && !this.multipartRequestedResentMarkers.has(i)) {
                        this.multipartRequestedResentMarkers.set(dmx.offset);
                        o.push(i);
                        // this.safeResend(startIndex + i, i, false);
                    }
                }
                this.batchResend(startIndex, o, false);
            }

            this.multipartLastSeenTime = performance.now();
            dmx.data.copy(this.reconstructionBuffer, dmx.offset * RFDmx.PAYLOAD_SIZE);
            this.reconstructionReceiveMarkers.set(dmx.offset);
            this.multipartDataSize = Math.max(this.multipartDataSize, (dmx.offset * RFDmx.PAYLOAD_SIZE) + dmx.data.length);

            // TODO: this panic
            // if (this.multipartTerminateOffset !== undefined && this.multipartRequestedResentMarkers.any() || this.)
            if (this.multipartTerminateOffset === undefined) {
                this.multipartTerminateOffset = dmx.offset;
                // TODO: clear above the end packet?
            }

            if (!this.multipartRequestedResentMarkers.any()) {
                if (this.multipartInFlight) {
                    const combined = new RFDmx(
                        dmx.deviceID,
                        dmx.sequence,
                        0,
                        dmx.compressionType,
                        this.multipartDataSize,
                        dmx.universe,
                        this.reconstructionBuffer.subarray(0, this.multipartDataSize),
                        0
                    );
                    this.handleDmx(combined, this);

                    this.seenStartingSequences[this.seenStartingSequencesPointer] = startIndex;
                    if (++this.seenStartingSequencesPointer >= this.seenStartingSequences.length) this.seenStartingSequencesPointer = 0;
                } else {
                    this.handleDmx(dmx, this);
                }

                this.reconstructionReceiveMarkers.reset();
                this.multipartTerminateOffset = undefined;
                this.multipartInFlight = false;
                this.multipartRequestedResentMarkers.reset();
                this.multipartLastSeenTime = 0;
                this.multipartDataSize = 0;
                this.multipartRequestedResendTotal = 0;

                this.reconstructionBuffer.fill(0);
            }
        }
    }

    private parseCheckin(root: RFRoot, b: Buffer) {
        if (this.handleCheckin === undefined) return;
        this.handleCheckin(RFCheckin.deserialize(b, root), this);
    }

    private parseRequest(root: RFRoot, b: Buffer) {
        if (this.handleRequest === undefined) return;
        this.handleRequest(RFRequest.deserialize(b, root), this);
    }

    parse(b: Buffer) {
        const root = RFRoot.deserialize(b);
        if (this.onRootParse) this.onRootParse(root, this);

        if (this.deviceIDFilter !== undefined && root.deviceID !== this.deviceIDFilter) {
            this._logger.log("parse", `ignoring due to an invalid device ID - ${this.deviceIDFilter} !== ${root.deviceID}`);
            return;
        }
        if (this.hasSeenAny && ((this.lastSeenSequence + 1) & 0xFF) != root.sequence) {
            this._logger.log('parse',`Missed a packet in the sequence, got ${root.sequence} but expected ${this.lastSeenSequence + 1}`);
        }

        this.hasSeenAny = true;
        this.lastSeenSequence = root.sequence;

        switch (root.packetType) {
            case PacketType.CHECKIN:
                this.parseCheckin(root, b);
                break
            case PacketType.DMX:
                this.parseDMX(root, b);
                break;
            case PacketType.REQUEST:
                this.parseRequest(root, b);
                break;
        }
    }

    tick() {
        if (!this.multipartInFlight) return;
        if (this.multipartLastSeenTime === 0) return;
        if (this.multipartRequestedResendTotal > ProtoHandler.MAX_RESEND_REQUESTS) return;
        if (performance.now() - this.multipartLastSeenTime > ProtoHandler.MULTIPART_TIME_BETWEEN_ASSUME_DROPPED) {
            if (!this.multipartRequestedResentMarkers.has(this.lastSeenOffset + 1) && this.multipartTerminateOffset != this.lastSeenOffset) {
                this._logger.log('tick',`timeout ${this.lastSeenOffset + 1} / ${this.multipartRequestedResentMarkers}`);
                this.safeResend(this.lastSeenSequence + 1, this.lastSeenOffset + 1, true);
            }
        }

        if (performance.now() - this.multipartLastTimeRequested > ProtoHandler.MULTIPART_TIME_BETWEEN_ASSUME_DROPPED) {
            let o: number[] = [];
            for (const index of this.multipartRequestedResentMarkers.present()) {
                this._logger.log('tick',`Pending request ${this.multipartStartSequence}.${index}`);
                o.push(index);
                // this.safeResend(this.multipartStartSequence + index, index, true);
            }

            this.batchResend(this.multipartStartSequence, o, true);
        }
    }

    private batchResend(seq: Uint8, offs: Uint8[], enforce: boolean){
        if (enforce && performance.now() - this.multipartLastTimeRequested < ProtoHandler.DELAY_BETWEEN_REQUESTS) return;
        this.multipartLastTimeRequested = performance.now();
        if (this.requestResends) this.requestResends(seq, offs, this);
    }

    private safeResend(seq: Uint8, off: Uint8, enforce: boolean) {
        if (enforce && performance.now() - this.multipartLastTimeRequested < ProtoHandler.DELAY_BETWEEN_REQUESTS) return;
        this.multipartLastTimeRequested = performance.now();
        if (this.requestResend) this.requestResend(seq, off, this);
    }
}
