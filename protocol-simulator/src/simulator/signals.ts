/**
 * A simple signal counter which will only ever increase and can be queried
 */
export interface Signal {
    /**
     * Increment the signal
     */
    inc(): void;

    /**
     * Retrieve the value of the signal
     */
    val(): number;
}

/**
 * A specific type of signal used for producing percentages. Separate numerator and denominator signals which can be
 * incremented separately and a utility function for doing percentages
 */
export class PercentageDualSignal<T extends Signal = BasicSignal> {
    private readonly _numerator: T;
    private readonly _denominator: T;

    constructor(numerator: T, denominator: T) {
        this._numerator = numerator;
        this._denominator = denominator;
    }

    get numerator(): T {
        return this._numerator;
    }

    get denominator(): T {
        return this._denominator;
    }

    /**
     * Calculate numerator/denominator, this returns a float in the range 0-1
     */
    public percent(): number {
        return this._numerator.val() / this._denominator.val();
    }
}

/**
 * Simple implementation of Signal using just a single number
 */
export class BasicSignal implements Signal {
    private _v: number = 0;

    val(): number {
        return this._v;
    }

    inc() {
        this._v++;
    }
}
