import {Sequenceable, SimulatorEdge, SimulatorEntity} from "../base";
import {CompressionType, ProtoHandler, RFCheckin, RFDmx, RFRequest} from "../../protocol";
import {Logger, Uint8} from "../../utils";
import {Signal} from "../signals";
import {ArtDataProvider} from "../provider";
import {off} from "node-notifier";
import {threadId} from "worker_threads";

/**
 * A simple storage that associates packet buffers with their sequence IDs and offsets ready for resends.
 * It maintains the insertion order and will remove elements in order of insertion when it reaches the
 * max size of elements stored.
 */
class PacketStore {
    /**
     * The largest number of elements that can be in #_insertions / #_packets
     * @private
     */
    private readonly _maxSize: number;
    /**
     * The array of inserted keys of _packets. This should have elements removed with _shift and added with push to form
     * a queue structure
     * @private
     */
    private _insertions: string[] = [];
    /**
     * The packets associated with their sequence+offset pairs as a string in the form sequence.offset
     * @private
     */
    private _packets: Record<string, Buffer> = {};

    /**
     * Creates a new store, after maxSize entries are present, the oldest entries will be purged
     * @param maxSize the maximum number of packets that can be stored
     */
    constructor(maxSize: number = 23) {
        this._maxSize = maxSize;
    }

    /**
     * Remove any packets beyond the max count if there are any
     * @private
     */
    private cleanup() {
        if (this._insertions.length > this._maxSize) {
            const toRemove = this._insertions.shift()!;
            delete this._packets[toRemove];
        }
    }

    /**
     * Inserts a single packet into the storage and then runs cleanup. If the packet already exists, it is shuffled to the end of the queue as if it had been newly inserted
     * @param sequence the sequence number of the packet
     * @param offset the offset of the packet within the flow
     * @param packet the packet to insert
     */
    public insertSingle(sequence: number, offset: number, packet: Buffer) {
        const key = `${sequence}.${offset}`;
        const index = this._insertions.indexOf(key);
        if (index !== -1) {
            this._insertions.splice(index, 1);
        }
        this._insertions.push(key);
        this._packets[key] = packet;

        this.cleanup();
    }

    /**
     * Returns the requested packet if present, undefined if not
     * @param sequence the sequence to lookup
     * @param offset the offset to lookup
     */
    public get(sequence: number, offset: number): Buffer | undefined {
        return this._packets[`${sequence}.${offset}`];
    }
}

/**
 * Representation of the master node on the network. This receives DMX data and then outputs it as packets
 */
export class MasterEntity extends Sequenceable implements SimulatorEntity {
    /**
     * The logger through which all messages should be written
     * @private
     */
    private readonly _logger: Logger;
    /**
     * The store in which packets should be saved and looked up for resent requests
     * @private
     */
    private _store: PacketStore = new PacketStore();
    /**
     * The protocol handler which will parse incoming packets
     * @private
     */
    private _proto: ProtoHandler;
    /**
     * The connection to the simulator which acts as the RF module in the real implementation
     * @private
     */
    private _edge: SimulatorEdge;
    /**
     * A countable signal used to count the number of sequences transmitted
     * @private
     */
    private _signal: Signal;
    /**
     * The provider which will deliver DMX data to the master ready for transmission
     * @private
     */
    private _provider: ArtDataProvider;
    /**
     * The queue of packets which have been received from the provider that need to be processed in the main loop
     * @private
     */
    private _queue: { buffer: Buffer, universe: Uint8 }[] = [];


    /**
     * Creates a new master entity which will subscribe to data on the provider, queue it and process it normally as part
     * of the default sim function. Sent sequences will be counted in signal and messages transmitted / received through
     * the edge
     * @param edge the connection to the simulator
     * @param signal the signal through which transmitted sequences will be tabulated
     * @param provider the provider through which data will be received
     */
    constructor(edge: SimulatorEdge, signal: Signal, provider: ArtDataProvider) {
        super();

        this._edge = edge;
        this._logger = new Logger(0, "MasterEntity");
        this._signal = signal;
        this._proto = new ProtoHandler(
            this._logger,
            undefined,
            undefined,
            undefined,
            this.checkin.bind(this),
            this.request.bind(this),
            undefined,
            () => {
                console.error('PANIC!!!');
            },
            0,
            undefined,
        )
        this._provider = provider;

        // Subscribe to data but push it to the queue rather than processing immediately so the main loop makes more
        // sense
        this._provider.subscribe((data, universe) => {
            this._queue.push({buffer: data, universe});
        });
    }

    /**
     * Terminates the provider, note that this does it directly so a shared provider will terminate for all nodes
     */
    public close() {
        this._provider.close();
    }

    /**
     * Handles a checkin message from a node. Currently a NOP bar logging
     * @param checkin the packet received
     * @private
     */
    private checkin(checkin: RFCheckin) {
        this._logger.log(`Checkin from ${checkin.deviceID} listening on universe ${checkin.universe}`);
    }

    /**
     * Handles a request by looking it up in storage and sending it if present
     * @param request the request received
     * @private
     */
    private request(request: RFRequest) {
        const sequence = request.requestedSequence;
        const entry = this._store.get(sequence, request.requestedOffset);
        if (entry) {
            this._edge.send(entry);
            this._logger.log(`Sent resend for request ${sequence}.${request.requestedOffset}`);
        } else {
            this._logger.log(`Failed to answer request: ${sequence}.${request.requestedOffset} - entry was invalid`)
            console.log(`[${threadId}] CACHE MISS ${sequence}.${request.requestedOffset}`);
        }
    }

    /**
     * Attempts to read and then parse from the edge, then parse from the queue of the received data
     */
    sim() {
        if (this._edge.available()) {
            let b = this._edge.read();
            if (b !== undefined) {
                this._proto.parse(b);
            }
        }

        if (this._queue.length > 0) {
            const {buffer: data, universe} = this._queue.shift()!;
            this._logger.log("<");

            let offset = 0;
            for (let i = 0; i < data.length; i += RFDmx.PAYLOAD_SIZE) {
                const packetData = data.subarray(i * RFDmx.PAYLOAD_SIZE, (i + 1) * RFDmx.PAYLOAD_SIZE);
                const packet = new RFDmx(
                    0,
                    this.getAndIncrementSequenceID(),
                    0,
                    CompressionType.FULL,
                    packetData.length,
                    universe,
                    packetData,
                    offset,
                );

                if (offset === 0) {
                    this._logger.log(`_packetSequence = ${packet.sequence}`);
                    packet.setStartFlag();
                } else {
                    if (i + RFDmx.PAYLOAD_SIZE > data.length) {
                        packet.setTerminateFlag();
                    } else {
                        packet.setContinueFlag();
                    }
                }

                const ser = packet.serialize();
                this._edge.send(ser);

                const copy = Buffer.from(ser);

                RFDmx.setResendOnBuffer(copy);
                this._store.insertSingle(packet.sequence, packet.offset, copy);
                offset++;
            }

            this._signal.inc();
            this._logger.log(`Sent ${offset} packets`);
        }

        this._proto.tick();
    }
}
