import {Sequenceable, SimulatorEdge, SimulatorEntity} from "../base";
import {Logger, Uint8} from "../../utils";
import {ProtoHandler, RFDmx, RFRequest} from "../../protocol";
import {PercentageDualSignal, Signal} from "../signals";

/**
 * Represents a node on the network which receives data. This does no processing of the data, just counts it
 */
export class NodeEntity extends Sequenceable implements SimulatorEntity {
    /**
     * The unique ID of this node in the network, used for packet routing in the simulator
     * @private
     */
    private _deviceID: Uint8;
    /**
     * The connection to the simulator to act as the RF module
     * @private
     */
    private _edge: SimulatorEdge;
    /**
     * The protocol handler which will handle receiving and requesting packets
     * @private
     */
    private _proto: ProtoHandler;
    /**
     * The logger through which all messages should be outputted
     * @private
     */
    private readonly _logger: Logger;
    /**
     * The signal which should count dropped packets vs started processing packets
     * @private
     */
    private readonly _signal: PercentageDualSignal;


    /**
     * Creates a new node, this will initialise the protocol handler but not do any work beyond that
     * @param deviceID the device ID to use, this must be unique in the network or it will hurt
     * @param edge the connection to the simulator to use for transmitting/receiving
     * @param signal the signal used to count the two states of packet processing, this will be passed down into protocol handler
     */
    constructor(deviceID: Uint8, edge: SimulatorEdge, signal: PercentageDualSignal) {
        super();

        this._deviceID = deviceID;
        this._edge = edge;
        this._logger = new Logger(deviceID, "NodeEntity");
        this._signal = signal;

        this._proto = new ProtoHandler(
            this._logger,
            (sequenceID, offset) => {
                edge.send(new RFRequest(this._deviceID, this.getAndIncrementSequenceID(), 0, offset, sequenceID).serialize());
            },
            (sequenceID, offset) => {
                offset.forEach((e) => {
                    edge.send(new RFRequest(this._deviceID, this.getAndIncrementSequenceID(), 0, e, sequenceID + e).serialize());
                });
            },
            this.dmx.bind(this),
            undefined,
            undefined,
            undefined,
            () => {
                console.error('PANIC!!!!!');
            },
            0,
            0,
            this._signal,
        );
    }

    /**
     * On receive of DMX data, will just print out a message and nout more
     * @param dmx the data received from the network
     * @private
     */
    private dmx(dmx: RFDmx) {
        this._logger.log('DMX!!!');
    }

    /**
     * Tries to read from the simulator and parse it if available
     */
    public sim() {
        if (this._edge.available()) {
            let b = this._edge.read();
            if (b !== undefined) {
                this._proto.parse(b);
            }
        }

        this._proto.tick();
    }
}
