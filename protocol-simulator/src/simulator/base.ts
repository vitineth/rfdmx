import {Uint8, Uint8Max} from "../utils";

/**
 * Represents a class that operates in a linearly incrementing uint8_t counter
 */
export class Sequenceable {
    /**
     * The next available sequenceID
     * @private
     */
    private _sequenceID = 0;

    /**
     * This will return the currently available sequence ID and then increment it, wrapping at 255
     * @protected
     */
    protected getAndIncrementSequenceID() {
        const a = this._sequenceID;
        this._sequenceID = (this._sequenceID + 1) & 0xFF;
        return a;
    }

    /**
     * Utility function to add two numbers using uint8_t wrapping logic
     */
    static add(a: Uint8, b: Uint8): Uint8 {
        return (a + b) & 0xFF;
    }

    /**
     * Utility function to subtract two numbers using uint8_t wrapping logic
     */
    static sub(a: Uint8, b: Uint8): Uint8 {
        if (a - b < 0) {
            return Uint8Max + (a - b);
        }
        return a - b;
    }
}

/**
 * Represents the interface of the simulator available to nodes. This supports sending and receiving data in the style
 * of the RF chips used in the real implementation
 */
export interface SimulatorEdge {
    /**
     * Sends the given buffer to the network to experience loss and be delivered to nodes
     * @param buffer the raw packet to transmit
     */
    send(buffer: Buffer): void;

    /**
     * Returns if this chip has data in the queue, available for reading via #read()
     */
    available(): boolean;

    /**
     * Tries to read from this rx queue, if available it will return the buffer, otherwise return undefined
     */
    read(): Buffer | undefined;
}

/**
 * Represents an entity that can be simulated as part of the wider network (this basically means it can receive updates)
 */
export interface SimulatorEntity {
    /**
     * RUn one loop of simulation
     */
    sim(): void;
}
