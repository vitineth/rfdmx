import {Logger} from "../utils";
import {clearInterval} from "timers";
import {BasicSignal, PercentageDualSignal, Signal} from "./signals";
import {SimulatorEdge, SimulatorEntity} from "./base";
import {ArtDataProvider} from "./provider";
import {NodeEntity} from "./entities/node";
import {MasterEntity} from "./entities/master";

/**
 * Multiplier to turn a number into Hz
 */
const Hz = 1;
/**
 * Multiplier to turn a number into KHz
 */
const KHz = 1000;
/**
 * Multiplier to turn a number into MHz
 */
const MHz = 1000000;
/**
 * Multiplier to turn a number into GHz
 */
const GHz = 1e+9;

/**
 * The main simulator. This maintains a set of internal queues for each of the nodes which will deliver in order. This
 * will introduce a given percentage of loss onto the connections. Loss is experienced on the transmission side, not on
 * the receiving side. This means in a multinode setup, a lost packet will be lost for all nodes, not for individual nodes
 */
export class Simulator {
    /**
     * The devices registered as part of this simulator that will receive updates during execution
     * @private
     */
    private _devices: SimulatorEntity[];
    /**
     * The RX queues for each {@link NodeEntity} in the network. This maps device ID to the set of buffers which should
     * be treated as a queue using shift and push. Node entities are required to be unique because otherwise their queues
     * will be combined
     * @private
     */
    private readonly _rxQueues: Record<number, Buffer[]>;
    /**
     * If a master has been added to this simualtor, they are required so this is used to tell
     * @private
     */
    private _hasMaster = false;
    /**
     * The logger which should be used for simulation messages
     * @private
     */
    private _logger: Logger;
    /**
     * The percentage of packets which should be dropped during transmission
     * @private
     */
    private readonly _lossPercentage: number;
    /**
     * The number of packets sent
     * @private
     */
    private _sent = 0;
    /**
     * The number of packets dropped
     * @private
     */
    private _dropped = 0;
    /**
     * The map of signals from node device IDs to their tracking signals of loss and parsed
     * @private
     */
    private _rxSignals: Record<number, PercentageDualSignal> = {};
    /**
     * The master signal used for transmission counts
     * @private
     */
    private _txSignal: Signal = new BasicSignal();
    /**
     * The provider of the data to the master node
     * @private
     */
    private _provider: ArtDataProvider | undefined;

    /**
     * Create a new simualtor which will induce the given amount of loss into the network
     * @param lossPercentage The loss that should be introduced, this should be a float in the range 0-1
     */
    constructor(lossPercentage: number) {
        this._devices = [];
        this._rxQueues = {};
        this._logger = new Logger(0, "Simulator");
        this._lossPercentage = lossPercentage;
    }

    /**
     * Constructs a new edge for a device with the given ID. On send this will test whether to drop and if not deliver
     * it to every RX queue except the one for this device.
     * @param deviceID the device ID for which the edge should be constructed
     * @private
     */
    private makeEdge(deviceID: number) {
        const that = this;
        return {
            send(buffer: Buffer) {
                // Drop it like its hot
                if (Math.random() <= that._lossPercentage) {
                    that._dropped++;
                    return;
                }
                that._sent++;

                Object.keys(that._rxQueues).map((e) => Number(e)).forEach((e) => {
                    if (e === deviceID) return;
                    that._rxQueues[e]?.push(buffer);
                });
            },
            read(): Buffer | undefined {
                if (Object.hasOwn(that._rxQueues, deviceID) && that._rxQueues[deviceID]!.length > 0) {
                    return that._rxQueues[deviceID]!.shift();
                }

                return undefined;
            },
            available(): boolean {
                return Object.hasOwn(that._rxQueues, deviceID) && that._rxQueues[deviceID]!.length > 0;
            },
        }satisfies SimulatorEdge;
    }

    /**
     * Create a new node with the given device ID. This will inject a {@link NodeEntity} into the network with a new
     * signal and freshly generated edge. New rx queues will be provisioned proactively
     * @param deviceID the unique ID within the network
     */
    public registerNode(deviceID: number) {
        if (Object.hasOwn(this._rxSignals, deviceID)) throw new Error(`You have tried to create a node with an ID already consumed! ${deviceID} is taken`);
        if (deviceID === 0) throw new Error(`You have tried to create a node with ID 0 which is reserved for the master`);

        const signal = new PercentageDualSignal(new BasicSignal(), new BasicSignal());
        this._devices.push(new NodeEntity(deviceID, this.makeEdge(deviceID), signal));
        this._rxQueues[deviceID] = [];
        this._rxSignals[deviceID] = signal;
    }

    /**
     * Injects a new master into the network. If a master has already been defined, this will throw
     * @param provider the provider which should supply data to the network
     */
    public registerMaster(provider: ArtDataProvider) {
        if (this._hasMaster) {
            throw new Error(`You already have a master, you probably don't want to do that.`);
        }

        this._provider = provider;
        this._devices.push(new MasterEntity(this.makeEdge(0), this._txSignal, provider));
        this._rxQueues[0] = [];
    }


    /**
     * Launches the simulation. This will run it for 15 seconds and then calculate the loss of the nodes and return them
     * as an array.
     */
    public launch(): Promise<number[]> {
        // const MAX_TIME = 3000;
        const MAX_TIME = 0.25 * 60 * 1000;
        const FREQUENCY = 16 * GHz;
        const PRINT_FREQUENCY = 1000;

        this._provider?.start();

        const makeInterval = (device: SimulatorEntity) => {
            let resolve: ((value: (PromiseLike<void> | void)) => void) | undefined = undefined;
            let prm = new Promise<void>((res) => {
                resolve = res;
            });

            const startTime = performance.now();
            const interval = setInterval(() => {
                if (performance.now() - startTime > MAX_TIME) {
                    clearInterval(interval);
                    resolve!();
                }

                device.sim();
            }, 1000 / FREQUENCY);

            return prm;
        }

        const makeTracker = () => {
            const startTime = performance.now();
            let lastPrint = 0;

            let resolve: ((v: number[]) => void);
            const prm = new Promise<number[]>((res) => {
                resolve = res;
            });

            const interval = setInterval(() => {
                if (performance.now() - startTime > MAX_TIME) {
                    clearInterval(interval);
                    this._logger.log("Terminating simulation");
                    (<MasterEntity>this._devices.find((e) => e instanceof MasterEntity)).close();
                    this._logger.log(`At ${Math.round(this._lossPercentage * 10000) / 100}% loss, ${this._txSignal.val()} DMX sequences sent, nodes recieved RX DMX ${Object.entries(this._rxSignals).map(([k, v]) => `${k}=>${v.denominator.val()}/${v.numerator.val()}`).join(', ')}`);
                    this._logger.log(`Loss percentages: ${Object.entries(this._rxSignals).map(([k, v]) => `${k} => ${Math.round(v.percent() * 10000) / 100}%`).join(' ; ')}`)

                    resolve(Object.values(this._rxSignals).map((v) => v.percent()));
                    return;
                }
                if (performance.now() - lastPrint > PRINT_FREQUENCY) {
                    lastPrint = performance.now();
                    this._logger.log(`Dropped ${this._dropped} packets ; Delivered ${this._sent} packets ; TX DMX ${this._txSignal.val()} ; RX DMX ${Object.entries(this._rxSignals).map(([k, v]) => `${k}=>${v.denominator.val()}/${v.numerator.val()}`).join(', ')}`);
                }
            }, 1000 / FREQUENCY);

            return prm;
        }

        return Promise.all([
            makeTracker(),
            ...this._devices.map(makeInterval),
        ]).then((d) => d[0]);
    }
}
