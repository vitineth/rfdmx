import {genBuffer, Uint8} from "../utils";
import dgram from "dgram";
// import random from "random";
import {clearInterval} from "timers";

export type ProviderSubscriptionFunction = (data: Buffer, universe: Uint8) => void;

/**
 * An ArtData provider sends DMX data to subscribed nodes and can be started once and closed once
 */
export interface ArtDataProvider {
    /**
     * Subscribes to this data source, the provided callback will be called every time there is data available
     * @param callback the callback to execute on data
     */
    subscribe(callback: ProviderSubscriptionFunction): void;

    /**
     * This will destroy this provider and stop sending data
     */
    close(): void;

    /**
     * This will launch the provider, this is only guaranteed to work once
     */
    start(): void;
}

/**
 * Abstract implementation of a provider that handles subscription management only
 */
export abstract class AbstractProvider implements ArtDataProvider {
    /**
     * The set of callbacks to be executed when data is available
     * @private
     */
    private _subscriptions: (ProviderSubscriptionFunction)[] = [];

    abstract close(): void;

    abstract start(): void;

    subscribe(callback: ProviderSubscriptionFunction) {
        this._subscriptions.push(callback);
    }

    /**
     * Transmits data to every active subscription
     * @param data the data to transmit
     * @param universe the universe this data originates from
     * @protected
     */
    protected transmit(data: Buffer, universe: Uint8) {
        this._subscriptions.forEach((e) => e(data, universe));
    }
}

/**
 * This provider will wait for ArtDmx packets on the provided interface and port and forward them to subscribed
 * nodes. This allows for interfacing with actual lighting systems for simulation
 */
export class UDPProvider extends AbstractProvider {

    /**
     * The socket which is connected to the interface on which ArtDmx data will be received
     * @private
     */
    private _server: dgram.Socket;
    /**
     * The port on which this provider is listening
     * @private
     */
    private readonly _portFilter: Uint8;
    /**
     * The ip of the interface on which this provider is listening
     * @private
     */
    private readonly _interfaceIP: string;

    /**
     * Initialises a socket but does not start listening to it. This will try and create a new socket with the provided
     * ip (using reuseAddr) and will bind to the port on start
     * @param portFilter the port on which to bind
     * @param interfaceIP the ip address of the interface on which to listen
     */
    constructor(portFilter: Uint8 = 6454, interfaceIP: string = "127.0.0.1") {
        super();
        this._portFilter = portFilter;
        this._interfaceIP = interfaceIP;

        this._server = dgram.createSocket({type: 'udp4', reuseAddr: true});

        this._server.on('error', (err) => {
            console.error(`UDP server error: ${err.stack}`);
            this._server.close();
        });

        this._server.on('message', (msg) => {
            let universe = (msg.readUint8(14) << 0) | (msg.readUint8(15) << 8);
            const data = msg.subarray(18);
            this.transmit(data, universe);
        });

        this._server.on('listening', () => {
            const address = this._server.address();
            console.log(`UDP server listening on ${address.address}:${address.port}`);
        });

    }

    /**
     * Binds the socket to the provided interface and port which will start receiving data
     */
    override start() {
        this._server.bind(this._portFilter, this._interfaceIP);
    }

    /**
     * Closes the socket which destroys this provider permanently
     */
    override close() {
        this._server.close();
    }
}

/**
 * A provider which sends random data at a given frame rate
 */
export class RandomProvider extends AbstractProvider {

    /**
     * The interval being used to transmit data, will be defined when the provider has been started
     * @private
     */
    private _interval: NodeJS.Timeout | undefined;
    /**
     * The delay to have between intervals of sending data
     * @private
     */
    private readonly _delay: number;
    /**
     * The array of universes which are being transmitted on
     * @private
     */
    private readonly _universes: Uint8[];

    /**
     * Creates a new random transmitter
     * @param rate the rate at which messages should be sent - you can use the Hz, KHz, MHz and GHz multipliers for this
     * @param universes the universes which the sent data can be from
     */
    constructor(rate: number, universes: Uint8[] = [0]) {
        super();
        this._universes = universes;

        this._delay = 1000 / rate;
    }

    /**
     * Launches the interval sending data generated with {@link genBuffer}. The data will always be 512
     */
    override start() {
        this._interval = setInterval(() => {
            const data = genBuffer(512);
            this.transmit(data, this._universes[Math.floor(this._universes.length * Math.random())] ?? 0);
        }, this._delay);
    }

    /**
     * Terminates the interval if defined, in contrast to {@link UDPProvider}, this provider can be started again
     */
    override close() {
        if (this._interval)
            clearInterval(this._interval);
    }
}
