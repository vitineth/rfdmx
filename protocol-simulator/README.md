# rfdmx-simulator

This module allows you to simulate the protocol and measure the effect of FPS and loss combined on lost sequences.

A loss is counted when existing data has to be deleted because a new start packet is recevied (the node gives up on
completing it in favour of a new sequence). A loss percentage is the number of times data was given up over the number
of start packets processed.

This module is set up to do FPS vs Induced loss vs Measured Loss. The configuration at time of writing is 20 steps
between 0 and 200 FPS with every loss stop between 0-100% in steps of 2.5%. This will produce 920 data points per
simulation.

When the simulation has finished running, it will ask for a name for the test and move the results into `./sims/<name>`.
Data will be in `./sims/<name>/data` and logs (if enabled) will be in `./sims/<name>/logs` (==Warning: if logs are
enabled it is going to generate _a lot_, use with extreme caution==).

To view the results, run `npm run visualise`. This will launch a http server and print the address, go to the provided
url and open `vis.html`. This will render a 3D scatter plot for every simulation contained in `./sims`, one series per
test. This allows for comparisons of the returned data and to see your changes effects against benchmark runs. You will
need an active data connection for this to load `plotly.js` from a CDN

The simulation is run multithreaded using worker threads. This is all coordinated in `main.ts`. There is one thread run
PER STEP IN FPS. All tests for different loss percentages run within the thread for that FPS. This can all be tweaked
in `main.ts` and the code should be relatively easy to follow.
