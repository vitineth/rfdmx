const channels = 4;
const dmx = Array(channels).fill(0);
const controller = document.querySelector('#controller');
const marker = document.querySelector('#marker');
const marker2 = document.querySelector('#marker2');
const received = document.querySelector('#received');
const labels = {0: 'R', 1: 'G', 2: 'B', 3: 'W'};
const sliders = [];
controller.style.gridTemplateColumns = `repeat(${channels}, 1fr)`;

const esp32Server = document.querySelector('#esp32-server');
const midiController = document.querySelector('#midi-controller');

let esp32Connected = false;

navigator.requestMIDIAccess().then(
    (midi) => {
        window.midi = midi;
        console.log(midi);
        const device = midi.inputs.get('574F19031269723AB0982EBEA287D004A0730C2A7B40E5233187CFDF982B6713');
        if(device === undefined){
            console.warn('MIDI device not found');
            midiController.style.backgroundColor = '#ff7675';
            return;
        }

        const handle = (e) => {
            if(e.data[0] !== 176) return;

            const index = e.data[1] - 3;
            const value = Math.min(255, Math.max(0, Math.round((e.data[2] / 127) * 255)));
            
            if(index >= sliders.length) return;
            sliders[index][0].value = value;
            sliders[index][1].innerText = value + " (" + value.toString(16).padStart(2, '0') + ")";
            dmx[index] = value;
            update();
        }

        device.onmidimessage = handle;

        midiController.style.backgroundColor = '#55efc4';
        console.log('MIDI device bound');
    },
    (e) => {
        console.warn('Failed to bind MIDI', e);
        midiController.style.backgroundColor = '#ff7675';
    }
)

function update(){
    fetch('/send', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(dmx),
    }).then((r) => {
        if (r.status !== 200) console.error('Failed to update', r);
    }).catch((e) => {
        console.error('Failed to update', e);
    });
}

for (let index = 0; index < channels; index++) {
    const slider = document.createElement('input');
    slider.classList.add('input-range');
    slider.setAttribute('id', `channel-${index}`);
    slider.setAttribute('orient', 'vertical');
    slider.setAttribute('type', 'range');
    slider.setAttribute('step', '1');
    slider.setAttribute('value', '0');
    slider.setAttribute('min', '0');
    slider.setAttribute('max', '255');

    const name = document.createElement('span');
    name.innerText = labels[index] ?? '';

    const label = document.createElement('span');
    label.setAttribute('id', `value-channel-${index}`);
    label.innerText = '0';

    slider.addEventListener('input', () => {
        label.innerText = slider.value + " (" + slider.valueAsNumber.toString(16).padStart(2, '0') + ")";
        dmx[index] = slider.valueAsNumber;
        update();
    });
    controller.insertBefore(slider, marker);
    controller.insertBefore(name, marker2);
    controller.append(label);

    sliders.push([slider, label]);
}

marker2.remove();
marker.remove();

let active = [0, 0, 255];

const pad = (s) => s.length == 2 ? s : `0${s}`;
const c = document.querySelector("canvas");
const ctx = c.getContext("2d");

setTimeout(() => {
    if(!esp32Connected) {
        c.remove();
        received.remove();
    }
}, 5000);

// Create WebSocket connection.
const socket = new WebSocket('ws://192.168.8.150/ws');

// Connection opened
socket.addEventListener('open', (event) => {
    esp32Server.style.backgroundColor = '#55efc4';
    esp32Connected = true;
});

socket.addEventListener('close', (event) => {
    esp32Server.style.backgroundColor = '#ff7675';
    esp32Connected = false;
});

// Listen for messages
socket.addEventListener('message', (event) => {
    active = JSON.parse(event.data);
    received.innerText = event.data;
});

fetch('http://192.168.8.150/dmx').then((d) => d.json()).then((d) => {
    active = d;
    received.innerText = JSON.stringify(active);
});

// setInterval(async () => {
    // active = await fetch('http://192.168.8.150/dmx').then((d) => d.json())
// }, 100);

const withShadow = (f, w = 10, c = 'white') => {
    const sb = ctx.shadowBlur;
    const sc = ctx.shadowColor;
    ctx.shadowBlur = w;
    ctx.shadowColor = c;
    f();
    ctx.shadowBlur = sb;
    ctx.shadowColor = sc;
};

const withFill = (c, f) => {
    const fc = ctx.fillStyle;
    ctx.fillStyle = c;
    f();
    ctx.fillStyle = fc;
}

function render() {
    ctx.clearRect(0, 0, c.width, c.height);

    ctx.fillStyle = "#1a1a1a";
    ctx.fillRect(0, 0, c.width, c.height);

    ctx.strokeStyle = "black";
    ctx.lineWidth = 5;
    ctx.strokeRect(1, 1, c.width - 2, c.height - 2);

    ctx.fillStyle = `#${pad(active[0].toString(16))}${pad(active[1].toString(16))}${pad(active[2].toString(16))}`;

    // Left Outer 
    ctx.beginPath();
    ctx.ellipse(
        (c.width / 4) - (c.width * 0.06968641114982578),
        c.height / 2,
        c.height * 0.25,
        c.height * 0.25,
        0,
        0,
        2 * Math.PI,
    );
    ctx.stroke();
    ctx.closePath();

    // Left Inner
    ctx.beginPath();
    ctx.ellipse(
        ((c.width / 4) - (c.width * 0.06968641114982578)),
        (c.height / 2),
        (c.height * 0.25) - ctx.lineWidth,
        (c.height * 0.25) - ctx.lineWidth,
        0,
        0,
        2 * Math.PI,
    );
    withShadow(() => ctx.fill(), 80, ctx.fillStyle);
    ctx.closePath();

    // Center Outer
    ctx.beginPath();
    ctx.ellipse(
        (c.width / 4) * 2,
        c.height / 2,
        c.height * 0.25,
        c.height * 0.25,
        0,
        0,
        2 * Math.PI,
    );
    ctx.stroke();
    ctx.closePath();

    // Center Inner
    ctx.beginPath();
    ctx.ellipse(
        (c.width / 4) * 2,
        (c.height / 2),
        (c.height * 0.25) - ctx.lineWidth,
        (c.height * 0.25) - ctx.lineWidth,
        0,
        0,
        2 * Math.PI,
    );
    withShadow(() => ctx.fill(), 80, ctx.fillStyle);
    ctx.closePath();

    // Right Outer
    ctx.beginPath();
    ctx.ellipse(
        ((c.width / 4) * 3) + (c.width * 0.06968641114982578),
        c.height / 2,
        c.height * 0.25,
        c.height * 0.25,
        0,
        0,
        2 * Math.PI,
    );
    ctx.stroke();
    ctx.closePath();

    // Center Inner
    ctx.beginPath();
    ctx.ellipse(
        ((c.width / 4) * 3) + (c.width * 0.06968641114982578),
        c.height / 2,
        (c.height * 0.25) - ctx.lineWidth,
        (c.height * 0.25) - ctx.lineWidth,
        0,
        0,
        2 * Math.PI,
    );
    withShadow(() => ctx.fill(), 80, ctx.fillStyle);
    ctx.closePath();

    requestAnimationFrame(render);
}

render();