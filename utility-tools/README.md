# Utility Scripts

> Exceprt from primary report

A series of scripts and tools were built to assist with the debugging and development of this project but have also formed a useful way to evaluate hardware for its reactivity and handling of the data. These scripts are contained run via `npm`. Setup should be done via `pnpm i`. 

### `artnet-transceiver`

```bash
$ npm run artnet-transceiver
```

The ArtNet transceiver allows testing basic implementations without actual physical lights. It presents a bank of 4 faders that map to ArtNet channels 0-3 (inc). It displays their value in decimal and hex to aid with identifying values in packet dumps. The `index.js` file contains the IP address to which the artnet data will be transmitted. 

This also has support for two additional extensions. An ESP module can be connected to the hardware, reading from the serial output which will parse the DMX output into data and transmit it back to the page which will render it in the form of a virtual light. This can be used to check reaction time and test without an actual light present. 

The backing `artnet` library used also retransmits data every second which can be used to confirm correct function from the node on receiving identical data transmissions. 

### `resend-artnet`

```bash
$ npm run resend-artnet -f <file> [-d] [-s] [-i <ip address>]
```

The ArtNet resender takes in a packet capture PCAPng file (`-f`) and replays the extracted ArtNet packets in one of two ways. The raw DMX data can either be dumped to a `raw-buffer-dump.bin` (`-d`) which is used by the radio testing module in the core project to replicate real data, or it can resend the data to a given IP address (`-s` and `-i`). This script will read the timings of packets from the PCAPng file and recreate them with delays meaning that data will be received by the node at the exact speed it was previously running at. 

### `debug-over-udp`

Within the core codebase, there is a `DEBUG_MASTER_OVER_UDP` definition. If present, when RF packets are going to be sent, their content is also concatenated and transmitted on port `6453` to a broadcast address. This script will listen for these broadcasts and provide a breakdown of the received packets showing raw dumps of the bytes, the decoded meaning and the changes that it has to a DMX buffer. This effectively recreates what is happening on the receiver nodes but with easer to parse visualisations 
