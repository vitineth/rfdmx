import pcap from "pcap";
import d from "pcap/decode/index.js";
import u from "pcap/decode/udp.js";
import { inspect, parseArgs } from "node:util";
import { writeFileSync } from "node:fs";
import { Packet } from "sacn";
import microtime from "microtime";
import artnetInit from "artnet";
import dgram from "node:dgram";

const {
  values: { file, dumpToBin, sendToArtnet, artnetIP },
} = parseArgs({
  options: {
    file: {
      type: "string",
      short: "f",
      default: "/home/ryan/Downloads/BTTF_sACN_capture.pcapng",
    },
    dumpToBin: {
      type: "boolean",
      short: "d",
      default: false,
    },
    sendToArtnet: {
      type: "boolean",
      short: "s",
      default: false,
    },
    artnetIP: {
      type: "string",
      short: "i",
    },
  },
});

if (artnetIP === undefined && sendToArtnet) {
  console.error("Must specify an artnet IP address");
  process.exit(1);
}

const UNIVERSE_FILER = 2;

const delay = (ms) =>
  new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
const waitForBind = (client, port) =>
  new Promise((resolve, reject) => {
    client.bind(port, undefined, (err) => {
      if (err) reject(err);
      else resolve();
    });
  });

const client = dgram.createSocket("udp4");
await waitForBind(client, 6455);
if (artnetIP.endsWith(".255")) client.setBroadcast(true);

const artnet = sendToArtnet
  ? (() => {
      return (data) => {
        const temp = Buffer.alloc(18 + data.length);
        Buffer.from("Art-Net\0").copy(temp, 0, 0);
        temp.writeUint16LE(0x5000, 8);
        temp.writeUint8(0, 10); // ProtVer
        temp.writeUint8(14, 11); // ProtVet
        temp.writeUint8(0, 12); // Sequence
        temp.writeUint8(0, 13); // Physical
        temp.writeUint16LE(UNIVERSE_FILER, 14); // PortAddress
        temp.writeUint16BE(data.length, 16); // Length

        for (let i = 0; i < data.length; i++) {
          // Data
          temp.writeUint8(data[i], i + 18);
        }

        client.send(temp, 6454, artnetIP, (err) => {
            if(err) console.error(err);
        });
      };
    })()
  : undefined;
const session = pcap.createOfflineSession(file, {});

const SACN_BUFFER = Buffer.of(
  0x41,
  0x53,
  0x43,
  0x2d,
  0x45,
  0x31,
  0x2e,
  0x31,
  0x37,
  0x00,
  0x00,
  0x00
);
const EXTRACT_COUNT = 200;

let dump = Buffer.alloc(0);
let n = 0;
let i = 0;

let lastSeenTime = -1;
let lastSent = microtime.now();

const packetsToHandle = [];

session.on("complete", async () => {
  for (const [segments, packetTimeUS, sacnPacket] of packetsToHandle) {
    const flat = Array(512)
      .fill(0)
      .map((v, i) => Math.round(((sacnPacket.payload[i] ?? 0) / 100) * 255));

    if (dumpToBin) {
      const buffer = Buffer.alloc(512);
      flat.forEach((v, i) => buffer.writeUInt8(v, i));

      dump = Buffer.concat([dump, buffer]);
    }

    if (artnet) {
      if (lastSeenTime === -1) lastSeenTime = packetTimeUS;
      const delayBetweenPackets = packetTimeUS - lastSeenTime;
      const delayRequired = Math.max(
        delayBetweenPackets - (microtime.now() - lastSent),
        0
      );

      artnet(flat);
      console.log("delay required", delayRequired / 1000, "ms");
      await delay(delayRequired / 1000);

      lastSent = microtime.now();
      lastSeenTime = packetTimeUS;
    }
  }

  if (dumpToBin) {
    console.log(dump.length);
    writeFileSync("raw-buffer-dump.bin", dump);
  }

  console.log("All done!");
});

session.on("packet", (packet) => {
  n++;
  if (i < EXTRACT_COUNT) {
    const segments = pcap.decode.packet(packet);
    const packetTimeUS =
      segments.pcap_header.tv_sec * 1000000 + segments.pcap_header.tv_usec;

    if (!(segments.payload instanceof d.EthernetPacket)) return;
    if (!(segments.payload.payload instanceof d.IPv4Packet)) return;
    if (!(segments.payload.payload.payload instanceof u)) return;
    /**
     * @type {Buffer}
     */
    const data = segments.payload.payload.payload.data;
    if (
      SACN_BUFFER.compare(
        segments.payload.payload.payload.data,
        4,
        4 + SACN_BUFFER.length
      ) !== 0
    )
      return;
    if (data.readUInt32BE(18) !== 4) return;

    const sacnPacket = new Packet(segments.payload.payload.payload.data);
    if (sacnPacket.universe != UNIVERSE_FILER) return;

    packetsToHandle.push([segments, packetTimeUS, sacnPacket]);
    i++;
  }

  if (i === EXTRACT_COUNT) {
    i = 10000000;
  }
});
