import dgram from 'node:dgram';
import colors from 'colors';

const server = dgram.createSocket('udp4');
const universeData = {};
// let output = Array(512).fill(0);

server.on('error', (err) => {
    console.error(`server error:\n${err.stack}`);
    server.close();
});

const padHex = (s) => s.length == 2 ? s : `0${s}`;
const pad = (s, l) => s.length < l ? ' '.repeat(l - s.length) + s : s;
/**
 * @param {number[]} a 
 */
const hexDump = (a) => {
    console.log('    │  0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19'.gray);
    console.log('────┼────────────────────────────────────────────────────────────'.gray);
    for (let i = 0; i < a.length; i += 20) {
        process.stdout.write(pad(i.toString(10), 3).gray + ' │'.gray);
        for (let j = 0; j < 20 && (i + j) < a.length; j++) {
            process.stdout.write(' ' + padHex(a[i + j].toString(16)));
        }
        process.stdout.write('\n');
    }
}

function expand(last_frame, incoming) {
    // console.log(typeof(last_frame), Array.isArray(last_frame));
    let clone = [...last_frame];

    let pointer = 0;
    while (pointer < incoming.length - 3) {
        let address = (incoming[pointer] << 8) | incoming[pointer + 1];
        let size = incoming[pointer + 2];
        console.log(`@ ${address} = ${size} changes`);
        if (incoming.length < pointer + 2 + size) {
            return null;
        }
    
        for (let i = 0; i < size; ++i) {
            clone[address + i] = incoming[pointer + 3 + i];
        }
        pointer += 3 + size;
    }
    
    return clone;
}    

function parseAndPrintDMX(msg, flags) {
    const isStart = (flags & 0b10000000) != 0;
    const isContinuation = (flags & 0b01000000) != 0;
    const isTermination = (flags & 0b00100000) != 0;
    const offset = (msg.at(4) & 0b11111100) >> 2;
    const compressionType = (msg.at(4) & 0b00000011);
    const size = (msg.at(5) << 8) | msg.at(6);
    const universe = (msg.at(7));
    // const address = (msg.at(7) << 8) | msg.at(8);
    const data = msg.subarray(8, 8 + size);

    console.log((isStart ? colors.green : colors.gray)(`start  `) + (isContinuation ? colors.green : colors.gray)(`continue  `) + (isTermination ? colors.green : colors.gray)(`terminate  `));
    if(!(isStart && isTermination)){
        console.log(`    ${'Is Multipart'.bold}  ${'yes'.green} - offset ${offset}`);    
    }else{
        console.log(`    ${'Is Multipart'.bold}  ${'no'.red}`);    
    }
    console.log(`    ${'Compression'.bold}   ${compressionType === 0 ? 'FULL'.green : 'DIFF'.magenta} ` + `  0x${padHex(compressionType.toString(16))} ${compressionType}`.gray);
    console.log(`    ${'Size'.bold}          ` + `0x${padHex(size.toString(16))} `.gray + `${size}`);
    console.log(`    ${'Data'.bold}          ${[...data].map((e) => padHex(e.toString(16)) + ' ').join('')}`);
    console.log(`    ${'Universe'.bold}      ` + universe);
    console.log('');

    if(compressionType == 1){
        if(Object.hasOwn(universeData, universe)){
            universeData[universe] = expand(universeData[universe], data);
        }else{
            universeData[universe] = expand(new Array(512).fill(0), data);
        }
    }else{
        if(!Object.hasOwn(universeData, universe)) universeData[universe] = new Array(512).fill(0);
        for(let i = 0; i < data.length; i++) output[i] = data[i];
    }

    return 4 + size;
}

/**
 * 
 * @param {Buffer} msg 
 */
function parseAndPrint(msg) {
    const deviceID = msg.at(0);
    const packetType = msg.at(1);
    const sequence = msg.at(2);
    const flags = msg.at(3);

    console.log('  [dmx] received');
    console.log(`    ${'Device ID'.bold}     0x${padHex(deviceID.toString(16))} ` + `${deviceID}`.gray);
    console.log(`    ${'Packet Type'.bold}   0x${padHex(packetType.toString(16))} ` + `${packetType}`.gray);
    console.log(`    ${'Sequence'.bold}      0x${padHex(sequence.toString(16))} ` + `${sequence}`.gray);
    process.stdout.write(`    ${'Flags'.bold}         ` + `0x${padHex(flags.toString(16))} ${flags} `.gray);

    let added = 0;
    switch (packetType) {
        case 0:
            added = parseAndPrintDMX(msg, flags);
            break;
        default:
            console.log('\n  Unknown packet type');
    }

    return 4 + added;
}

server.on('message', (msg, rinfo) => {
    console.log(`<<-- ${rinfo.address}:${rinfo.port}`);

    console.log('');
    hexDump([...msg]);
    console.log('');

    let idx = 0;
    while (idx < msg.length) {
        idx += parseAndPrint(msg.subarray(idx));
        if (msg.at(idx) != 0xEF) {
            console.error('Failed to parse - didn\'t find divider - got ' + msg.at(idx) + ", " + idx);
            return;
        }
        idx++;
    }

    for(const [k, v] of Object.entries(universeData)){
        console.log(`Universe ${k}`);
        hexDump(v);
    }
});

server.on('listening', () => {
    const address = server.address();
    console.log(`server listening ${address.address}:${address.port}`);
});

server.bind(6453);