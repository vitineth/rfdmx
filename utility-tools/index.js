const express = require('express');
const bodyparser = require('body-parser');

const artnet = require('artnet')({ host: '169.254.80.205' });
// const artnet = require('artnet')({ host: '127.0.0.255' });
const app = express();

artnet.on('send', () => {
    console.log('  DMX Update transmitted')
});

app.use(express.static('serve'));
app.use(bodyparser.json({}));

app.post('/send', (req, res) => {
    if (!Array.isArray(req.body)) return res.sendStatus(400);
    for (let i = 0; i < req.body.length; i++) if (typeof (req.body[i]) !== 'number') return res.sendStatus(400);

    console.log(`Received update for ${req.body.length} channels: ${req.body}`);
    artnet.set(req.body);
    res.sendStatus(200);
});

app.listen(8080, () => console.log('Launched'));
