package main

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/dgryski/go-lttb"
	"github.com/mattn/go-sqlite3"
	_ "github.com/mattn/go-sqlite3"
	"net/http"
	"os"
)

// Source: dinedal/go-sqlite3-extension-functions

type entrypoint struct {
	lib  string
	proc string
}

var libNames = []entrypoint{
	{"/usr/local/lib/libgo-sqlite3-extension-functions", "sqlite3_extension_init"},
	//{"libgo-sqlite3-extension-functions.dylib", "sqlite3_extension_init"},
	//{"libgo-sqlite3-extension-functions.dll", "sqlite3_extension_init"},
}

func initDB() {
	sql.Register("sqlite3-extension-functions",
		&sqlite3.SQLiteDriver{
			ConnectHook: func(conn *sqlite3.SQLiteConn) error {
				for _, v := range libNames {
					if err := conn.LoadExtension(v.lib, v.proc); err == nil {
						return nil
					} else {
						Printfln("err = %+v", err)
					}
				}
				return errors.New("libgo-sqlite3-extension-functions not found")
			},
		})
}

// end source

// DropEntry represents the drop status of a single packet, it contains if the packet was lost on original transmission, recovered via trailing IDs, the ID of the packet and the timestamp it was added
type DropEntry struct {
	PacketID   uint32 `json:"packetID"`
	WasDropped bool   `json:"wasDropped"`
	Recovered  bool   `json:"recovered"`
	Timestamp  string `json:"timestamp"`
}

// dropData returns an HTTP handler, which when called will fetch the latest 3000 packets in reverse order with all fields
// The provided db must point to a valid database which can be queried or the request will 500.
func dropData(db *sql.DB) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		query, err := db.Query("SELECT * FROM drop_mapping ORDER BY added DESC LIMIT 3000;")
		if err != nil {
			w.WriteHeader(500)

			if d, e := json.Marshal(map[string]interface{}{
				"status":  "fail",
				"message": "Failed to query the drop data: " + err.Error(),
			}); e == nil {
				_, _ = w.Write(d)
				Printfln("Failed dropData: %+v", string(d))
			} else {
				Printfln("Failed to write error to http - original error %+v, json error %+v", err, e)
			}
			return
		}

		entries := make([]DropEntry, 0)
		for query.Next() {
			entry := DropEntry{}
			err := query.Scan(&entry.PacketID, &entry.WasDropped, &entry.Recovered, &entry.Timestamp)
			if err != nil {
				Printfln("[server]: failed to scan records: %+v", err)
				continue
			}
			entries = append(entries, entry)
		}

		if d, e := json.Marshal(map[string]interface{}{
			"status": "success",
			"data":   entries,
		}); e == nil {
			_, _ = w.Write(d)
		} else {
			Printfln("[server]: failed to write JSON data: %+v", e)
		}
	}
}

// rawRtt returns a http handler which will return the timestamp and rtt values for the last 20000 points, reduced down
// to 7000 points using the LTTB algorithm. These will be returned to the client as an array of {x:?,y:?} objects (within
// the default reply)
func rawRtt(db *sql.DB) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		query, err := db.Query("SELECT timestamp, rtt FROM rtt  ORDER BY added DESC LIMIT 20000;")
		if err != nil {
			w.WriteHeader(500)

			if d, e := json.Marshal(map[string]interface{}{
				"status":  "fail",
				"message": "Failed to query the RTT data: " + err.Error(),
			}); e == nil {
				_, _ = w.Write(d)
				Printfln("Failed rawRtt: %+v", string(d))
			} else {
				Printfln("Failed to write error to http - original error %+v, json error %+v", err, e)
			}
			return
		}
		entries := make([]lttb.Point[float64], 0)

		for query.Next() {
			var timestamp float64
			var rtt float64

			err := query.Scan(&timestamp, &rtt)
			if err != nil {
				Printfln("[server]: failed to scan records: %+v", err)
				continue
			}

			entries = append(entries, lttb.Point[float64]{
				X: timestamp,
				Y: rtt,
			})
		}

		entries = lttb.LTTB(entries, 7000)
		if d, e := json.Marshal(map[string]interface{}{
			"status": "success",
			"data":   entries,
		}); e == nil {
			_, _ = w.Write(d)
		} else {
			Printfln("[server]: failed to write JSON data: %+v", e)
		}
	}
}

// runtimeAPI returns an HTTP handler that will return the number of nodes in the last received update and the set of
// requested packet counts in the form {X:?,Y:?} for plotting
func runtimeAPI(db *sql.DB) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		query, err := db.Query("SELECT * FROM runtime ORDER BY added DESC LIMIT 3000;")
		if err != nil {
			w.WriteHeader(500)

			if d, e := json.Marshal(map[string]interface{}{
				"status":  "fail",
				"message": "Failed to query the runtime data: " + err.Error(),
			}); e == nil {
				_, _ = w.Write(d)
				Printfln("Failed runtime: %+v", string(d))
			} else {
				Printfln("Failed to write error to http - original error %+v, json error %+v", err, e)
			}
			return
		}

		type Plot struct {
			X string
			Y int
		}

		nodes := -1
		plot := make([]Plot, 0)
		for query.Next() {
			entry := StoredRuntimeMetric{}
			err := query.Scan(&entry.requestedSinceLastUpdate, &entry.hasOverflowed, &entry.nodeCount, &entry.added)
			if err != nil {
				Printfln("[server]: failed to scan records: %+v", err)
				continue
			}

			if nodes == -1 {
				nodes = int(entry.nodeCount)
			}

			if !entry.hasOverflowed {
				plot = append(plot, Plot{
					X: entry.added,
					Y: int(entry.requestedSinceLastUpdate),
				})
			}
		}

		if d, e := json.Marshal(map[string]interface{}{
			"status": "success",
			"nodes":  nodes,
			"plot":   plot,
		}); e == nil {
			_, _ = w.Write(d)
		} else {
			Printfln("[server]: failed to write JSON data: %+v", e)
		}
	}
}

// index returns an HTTP handler which will read and reply with _res/index.html
func index(w http.ResponseWriter, _ *http.Request) {
	file, err := os.ReadFile("_res/index.html")
	if err != nil {
		Printfln("Failed to read index file: %+v", err)
		w.WriteHeader(500)
		return
	}

	_, _ = w.Write(file)
}

// runtime returns an HTTP handler which will read and reply with _res/runtime.html
func runtime(w http.ResponseWriter, _ *http.Request) {
	file, err := os.ReadFile("_res/runtime.html")
	if err != nil {
		Printfln("Failed to read runtime file: %+v", err)
		w.WriteHeader(500)
		return
	}

	_, _ = w.Write(file)
}

// LaunchAggregateServer will create a connection to the provided DB with additional statistical features enabled and
// then bind all previously defined HTTP handlers. A link to the server will be printed to stdout
func LaunchAggregateServer(receiver CLIMonitorAggregateViewer) {
	initDB()
	db, err := sql.Open("sqlite3-extension-functions", config.DataFile)
	if err != nil {
		Printfln("[aggregate]: failed to open the database at %v: %+v", config.DataFile, err)
		return
	}
	defer func(db *sql.DB) {
		_ = db.Close()
	}(db)

	Printfln("[aggregate]: setting up the http server")

	http.HandleFunc("/rtt-raw", rawRtt(db))
	http.HandleFunc("/drop", dropData(db))
	http.HandleFunc("/runtime-api", runtimeAPI(db))
	http.HandleFunc("/runtime", runtime)
	http.HandleFunc("/", index)

	Printfln("[aggregate]: go see it at http://%v:%v", config.BindIP, config.BindPort)
	err = http.ListenAndServe(fmt.Sprintf("%v:%v", config.BindIP, config.BindPort), nil)
	if err != nil {
		Printfln("[server]: failed to launch the server due to error: %+v", err)
	}
}
