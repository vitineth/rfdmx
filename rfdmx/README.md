# rfdmx

This is a command line tool for deploying and monitoring a RFDmx node deployment. The script uses the source in the other folders and provides a nice wrapper around PlatformIO with some addtional functionality. It is split into two general deployments: a metric monitoring deployment and a real deployment. It is recommended that you start out with a metrics deployment to test your environment before moving on to a real deployment.

## Metrics Deployment

A metrics deployment is designed to gather the state of the network over time to ensure that they will function properly and not lead to unpleasant surprises. The general process for doing this is like so:

1. Launch an aggregate collection server (and optional viewer) to read and process data
1. Flash modules with a monitoring edition of the software
    1. In this mode, the master sends bursts and nodes reply. From this loss and delay is worked out on your network
    2. This happens constantly until you stop it. You should leave this running for a while, through a range of conditions including
       * During the day
       * During the night
       * During a busy event
    4. This data should show the loss and delay to any given node over time, plot this in a method that shows how likely it is for them to fail under given conditions
5. If it seems stable enough in all conditions in your venue, the nodes can then be reflashed with the correct firmware
    1. This requires some additional configuration for your specific setup but can all be done from the command line tool
    7. The master should be given an address on which to send metrics to for the time being (link to future works)
8. You can monitor continual metrics over time through the aggregate server

In general, the metrics commands are all sub commands of

```bash
$ rfdmx flash monitor
$ rfdmx monitor
```

The web page for monitoring data will show you a plot of loss over time and round trip times. Higher round trip times imply some level of interference of issue on the network and while still usable means that you will run into issues. For example this means that lighting cues are likely to fire slightly later than anticipated and the network in general may be slower if you are running on the edge of the packet detection threshold. Ideally, round trip times should be under 10ms on average. Loss rates should be as low as possible and you should aim for near 100% recovery if you have loss. Any loss above 5% should be considered dangerous and subject to more testing.

## Real Deployment

This has a bit more configuration but the help commands of the cli tool should provide a good overview. Commands for doing this are all generally under

```bash
$ rfdmx flash [master/node]
```

## Installing

This project has a slightly more complex install as it relies on additional math functions in sqlite3. This is achieved through using [dinedal/go-sqlite3-extension-functions](https://github.com/dinedal/go-sqlite3-extension-functions) which will need to be installed by hand to use `aggregate-viewer`
