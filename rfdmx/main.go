package main

import (
	"bytes"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"path"
	"regexp"
	"strconv"
	"strings"

	"github.com/alecthomas/kong"
	"gobn.github.io/coalesce"
	"gopkg.in/ini.v1"
)

// MinVersion is the minimum required version of platform io that is supported by this script
var MinVersion = []int{6, 1, 6}

// VersionRegexp extracts the semver version elements from the PIO version output
var VersionRegexp = regexp.MustCompile("^PlatformIO Core, version (?P<Major>[0-9]+).(?P<Minor>[0-9]+).(?P<Patch>[0-9]+)$")

// dumpCommand will print the given command to stdout including the working directory, with command line arguments added
// if they have been specified
func dumpCommand(cmd *exec.Cmd) {
	if cmd.Args == nil || len(cmd.Args) == 0 {
		Printfln("$ %+v (wd=%+v)", cmd.Path, cmd.Dir)
	} else {
		Printfln("$ %+v %+v (wd=%+v)", cmd.Path, strings.Join(cmd.Args[1:], " "), cmd.Dir)
	}
}

// exists returns if the provided path exists on the file system. This provides no guarantees that the file is readable
func exists(path string) bool {
	if _, err := os.Stat(path); err == nil {
		return true
	} else if errors.Is(err, os.ErrNotExist) {
		return false
	} else {
		Printfln("Warning - got an unexpected result from checking if file exists: %+v", err)
		return false
	}
}

// ref will return the pointer to the provided value, this is the equivalent of & but supports constants
func ref[T interface{}](a T) *T {
	return &a
}

// ExecToExitCode when given an error, if it is specified and an instance of ExitError, this will return the exit code
// and the contents of stderr. If specified but not an exit error, this will return (-1, nil). Finally if specified this
// will return (0, nil)
func ExecToExitCode(err error) (int, *string) {
	if err != nil {
		if exitError, ok := err.(*exec.ExitError); ok {
			return exitError.ExitCode(), ref(string(exitError.Stderr))
		}

		return -1, nil
	}

	return 0, nil
}

// SetupPlatformIOVenv will create a new venv in the provided installLocation using the python argument in pythonPath
func SetupPlatformIOVenv(pythonPath string, installLocation string) {
	installCmd := exec.Command(pythonPath, "-m", "venv", installLocation)
	dumpCommand(installCmd)
	if code, stderr := ExecToExitCode(installCmd.Run()); code != 0 {
		Printfln("Failed to execute venv creation - returned a non zero exit code: %+v", code)
		if stderr != nil {
			Printfln("Stderr was provided: %+v", *stderr)
		} else {
			Printfln("No stderr was given")
		}
		return
	}
}

// InstallPlatformIO will use the python executable contained within the installLocation and attempt to install
// platformio package via pip.
func InstallPlatformIO(pythonPath string, installLocation string) {
	cwd, err := os.Getwd()
	if err != nil {
		Printfln("Failed to get the current working path of the currently running script?\n%+v", err)
		return
	}

	cmd := exec.Command(path.Join(cwd, installLocation, "bin", "python3"), "-m", "pip", "install", "--upgrade", "platformio")
	dumpCommand(cmd)
	cmd.Env = append(cmd.Env, "PATH="+path.Join(cwd, installLocation, "bin"))
	if code, stderr := ExecToExitCode(cmd.Run()); code != 0 {
		Printfln("Failed to execute platformio installation - returned a non zero exit code: %+v", code)
		if stderr != nil {
			Printfln("Stderr was provided: %+v", *stderr)
		} else {
			Printfln("No stderr was given")
		}
		return
	}
}

// GetPath will return the first environment variable entry starting with PATH=, sliced to remove the PATH= prefix. If
// none is found it will return an empty string
func GetPath(environ []string) string {
	for _, v := range environ {
		if strings.HasPrefix(v, "PATH=") {
			return v[5:]
		}
	}

	return ""
}

// StripPath will remove any entries in the provided array that start with PATH= and return the result
func StripPath(environ []string) []string {
	results := make([]string, 0)

	for _, s := range environ {
		if !strings.HasPrefix(s, "PATH=") {
			results = append(results, s)
		}
	}

	return results
}

// Pio will return a prepared command to invoke the pio script in the bi nof the install location. The environment
// variables will be updated to include the install location bin as the first entry which should match how it would be
// executed in terminal. The working directory of the command will match that of this process. The command will not be
// invoked, just returned
func Pio(installLocation string, args ...string) (*exec.Cmd, error) {
	cwd, err := os.Getwd()
	if err != nil {
		return nil, err
	}

	//cmd := exec.Command("/bin/bash", "-c", strings.Join(append(append([]string{}, path.Join(cwd, installLocation, "bin", "pio")), args...), " "))
	cmd := exec.Command(path.Join(cwd, installLocation, "bin", "pio"), args...)
	cmd.Dir = cwd
	cmd.Env = append(StripPath(os.Environ()), "PATH="+path.Join(cwd, installLocation, "bin")+":"+GetPath(os.Environ()))
	return cmd, nil
}

// BootstrapPlatformIO will determine if platform io is installed (if not, installing it) and if the version matches
// (exiting the program if wrong) and then logging the detected version when found.
func BootstrapPlatformIO(pythonPath string, installLocation string) {
	cwd, err := os.Getwd()
	if err != nil {
		Printfln("Failed to bootstrap platformio because the current working directory could not be determined: %+v", err)
		return
	}

	if !exists(path.Join(cwd, installLocation)) {
		Printfln("Couldn't find %+v - installing", path.Join(cwd, installLocation))
		SetupPlatformIOVenv(pythonPath, installLocation)
		InstallPlatformIO(pythonPath, installLocation)
		return
	}

	if !exists(path.Join(cwd, installLocation, "bin")) {
		Printfln("The install location exists but doesn't look like a venv - could not find the bin folder, stopping out of caution")
		os.Exit(1)
	}

	if !exists(path.Join(cwd, installLocation, "bin", "python3")) {
		Printfln("The install location exists but doesn't look like a venv - could not find the python executable, stopping out of caution")
		os.Exit(1)
	}

	if !exists(path.Join(cwd, installLocation, "bin", "pio")) {
		Printfln("The install location venv exists but doesn't seem to contain pio, reinstalling")
		InstallPlatformIO(pythonPath, installLocation)
	}

	cmd, err := Pio(installLocation, "--version")
	if err != nil {
		Printfln("Failed to get version of pio: %+v", err)
		os.Exit(1)
	}

	versionBytes, err := cmd.Output()
	if err != nil {
		Printfln("Failed to get version of pio: %+v", err)
		os.Exit(1)
	}
	versionString := strings.TrimSpace(string(versionBytes))

	regexMatch := VersionRegexp.FindStringSubmatch(versionString)
	if regexMatch == nil || len(regexMatch) != 4 {
		Printfln("Got an invalid version string that could not be parsed: %+v", versionString)
		os.Exit(1)
	}

	major, erMa := strconv.Atoi(regexMatch[1])
	minor, erMi := strconv.Atoi(regexMatch[2])
	patch, erPa := strconv.Atoi(regexMatch[3])

	if erMa != nil || erMi != nil || erPa != nil {
		Printfln("Got an invalid version string that could not be parsed: %+v", versionString)
		os.Exit(1)
	}

	if major < MinVersion[0] || minor < MinVersion[1] || patch < MinVersion[2] {
		Printfln("Version is out of date! Reinstalling")
		InstallPlatformIO(pythonPath, installLocation)
		return
	}

	Printfln("Got pio with version %v.%v.%v", major, minor, patch)
}

// DeriveINI will read from platformio.ini and overwrite the specified properties, merging it with the existing config
// properties. This new environment will be named 'root' and then written to the cli-pio.ini file, ready for executing
// via pio
func DeriveINI(originalKey string, platform string, board string, buildFlags map[string]string) error {
	cfg, err := ini.LoadSources(ini.LoadOptions{
		AllowPythonMultilineValues: true,
	}, "platformio.ini")

	if err != nil {
		return err
	}

	section := cfg.Section(originalKey)
	if section == nil {
		return errors.New("Unknown section " + originalKey)
	}

	section.Key("platform").SetValue(platform)
	section.Key("board").SetValue(board)
	buildFlagsStrings := make([]string, 0)
	for key, value := range buildFlags {
		buildFlagsStrings = append(buildFlagsStrings, fmt.Sprintf("'-D %v=%v'", key, value))
	}

	section.Key("build_flags").SetValue(section.Key("build_flags").MustString("") + "\n    " + strings.Join(buildFlagsStrings, "\n    "))

	newCfg := ini.Empty(ini.LoadOptions{AllowPythonMultilineValues: true})
	newCfg.Section("env:root").SetBody(section.Body())

	for _, elem := range section.Keys() {
		newCfg.Section("env:root").NewKey(elem.Name(), elem.String())
	}

	var buffer bytes.Buffer
	_, err = newCfg.WriteTo(&buffer)
	//err = newCfg.SaveTo("cli-pio.ini")
	if err != nil {
		return err
	}

	byteData := bytes.Replace(buffer.Bytes(), []byte(`"""`), []byte{}, -1)
	err = os.WriteFile("cli-pio.ini", byteData, 0644)
	if err != nil {
		return err
	}

	return nil
}

// FlashWithEnvironment will boostrap pio, then derive a new config with the provided values, finally executing the
// command through the Pio function. On error, the output will be logged and finally the produced config file will be
// cleaned up
func FlashWithEnvironment(environment string, platform string, board string, buildFlags map[string]string) error {
	BootstrapPlatformIO(CLI.PythonExecutable, CLI.PioInstallLocation)

	err := DeriveINI("env:"+environment, platform, board, buildFlags)
	if err != nil {
		Printfln("Failed to derive the new ini file: %+v", err)
		return err
	}

	args := []string{"run", "-t", "upload", "-e", "root", "-c", "cli-pio.ini"}
	if CLI.Flash.Device != nil {
		args = append(args, "--upload-port", *CLI.Flash.Device)
	}
	cmd, err := Pio(CLI.PioInstallLocation, args...)
	if err != nil {
		return err
	}
	dumpCommand(cmd)
	for k, v := range buildFlags {
		Printfln("  -D %v=%v", k, v)
	}

	std, err := cmd.Output()
	if err != nil {
		_, s := ExecToExitCode(err)
		Printfln("Failed to flash! %+v \n%+v\n%+v", err, string(std), *s)

		return err
	}

	if CLI.Verbose {
		Printfln("%v", string(std))
	}

	err = os.Remove("cli-pio.ini")
	if err != nil {
		Printfln("Failed to delete the derive cli-pio.ini file!")
		return err
	}

	return nil
}

// IPToUint32 converts a provided IP address to a Uint32 value that can be used to initialise an IP address constructor
// in the arduino framework, returning it as a string. If the IP address is invalid, or numbers cannot be converted it
// will bubble up the error
func IPToUint32(ip string) (*string, error) {
	entries := strings.Split(ip, ".")
	if len(entries) != 4 {
		return nil, errors.New("invalid IP address, must be 4 octets")
	}

	o1, erO1 := strconv.Atoi(entries[0])
	o2, erO2 := strconv.Atoi(entries[1])
	o3, erO3 := strconv.Atoi(entries[2])
	o4, erO4 := strconv.Atoi(entries[3])

	if erO1 != nil || erO2 != nil || erO3 != nil || erO4 != nil {
		return nil, coalesce.Error(erO1, erO2, erO3, erO4)
	}

	if o1 > 255 || o2 > 255 || o3 > 255 || o4 > 255 {
		return nil, errors.New("one of the octets is too high")
	}

	return ref(strconv.Itoa((o4 << 24) + (o3 << 16) + (o2 << 8) + o1)), nil
}

// MapStrings is equivalent to the javascript Array#map function
func MapStrings(mapper func(s string) string, s []string) []string {
	results := make([]string, len(s))

	for i, sv := range s {
		results[i] = mapper(sv)
	}

	return results
}

// MacToByteArray splits a mac address in the form ??:??:??:??:??:?? to 0x??, 0x??, 0x??, 0x??, 0x??, 0x??
func MacToByteArray(ip string) string {
	return strings.Join(MapStrings(func(r string) string {
		return "0x" + r
	}, strings.Split(ip, ":")), ", ")
}

type CLIMonitorMaster struct {
	DeviceIP     string `required:"" help:"The IP address this device should assign itself on the network"`
	DeviceMAC    string `optional:"" help:"The MAC address the hardware should assign itself, defaults to D8:36:D9:B8:04:E7" default:"D8:36:D9:B8:04:E7"`
	ReceiverIP   string `required:"" help:"The IP address the master should transmit monitoring data to"`
	ReceiverPort uint16 `optional:"" help:"The port on which the data aggregator is running on the receiver IP, defaults to 6453" default:"6453"`
	Platform     string `optional:"" help:"Specify the platform this is being deployed to, should match an available PlatformIO platform, defaults to atmelavr" default:"atmelavr"`
	Board        string `optional:"" help:"Specifies the board this master node will be deployed to, this defaults to megaatmega2560 as from the project" default:"megaatmega2560"`
	Channel      uint8  `optional:"" help:"The RF channel on which this module should transmit and receive, this should be in the range 0-125. Defaults to 76" default:"76"`
}

func (receiver *CLIMonitorMaster) Validate() error {
	if receiver.Channel > 125 {
		return errors.New("invalid channel, must be in the range 0-125")
	}

	return nil
}

func (receiver *CLIMonitorMaster) Run() error {
	ip, err := IPToUint32(receiver.DeviceIP)
	if err != nil {
		Printfln("Invalid device ip address: %+v %+v", receiver.DeviceIP, err)
		return err
	}

	targetIP, err := IPToUint32(receiver.ReceiverIP)
	if err != nil {
		Printfln("Invalid device ip address: %+v %+v", receiver.DeviceIP, err)
		return err
	}

	buildDefines := map[string]string{
		"DEVICE_IP_ADDRESS":  *ip,
		"DEVICE_MAC_ADDRESS": MacToByteArray(receiver.DeviceMAC),
		"RECEIVER_IP":        *targetIP,
		"RECEIVER_PORT":      strconv.Itoa(int(receiver.ReceiverPort)),
		"RF_CHANNEL":         strconv.Itoa(int(receiver.Channel)),
		"METRIC_MASTER":      "true",
	}

	if err := FlashWithEnvironment(
		"metric-master",
		receiver.Platform,
		receiver.Board,
		buildDefines,
	); err != nil {
		return err
	}

	Printfln("Metrics Master has been flashed!")

	return nil
}

type CLIMonitorNode struct {
	NodeID   uint8  `required:"" help:"The ID for this node. It should be unique across the entire monitoring network"`
	Platform string `optional:"" help:"Specify the platform this is being deployed to, should match an available PlatformIO platform, defaults to atmelavr" default:"atmelavr"`
	Board    string `optional:"" help:"Specifies the board this master node will be deployed to, this defaults to uno as from the project" default:"uno"`
	Channel  uint8  `optional:"" help:"The RF channel on which this module should transmit and receive, this should be in the range 0-125, defaults to 76" default:"76"`
}

func (receiver *CLIMonitorNode) Validate() error {
	if receiver.Channel > 125 {
		return errors.New("invalid channel, must be in the range 0-125")
	}
	if receiver.NodeID > 128 {
		return errors.New("invalid node id, must be in the range 0-128")
	}

	return nil
}

func (receiver *CLIMonitorNode) Run() error {

	buildDefines := map[string]string{
		"NODE_ID":     strconv.Itoa(int(receiver.NodeID)),
		"RF_CHANNEL":  strconv.Itoa(int(receiver.Channel)),
		"METRIC_NODE": "true",
	}

	if err := FlashWithEnvironment(
		"metric-node",
		receiver.Platform,
		receiver.Board,
		buildDefines,
	); err != nil {
		return err
	}

	Printfln("Metrics Node has been flashed!")

	return nil
}

type CLIMonitorAggregate struct {
	BindIP   string `optional:"" help:"The IP address on which the aggregation server should listen, defaults to 0.0.0.0" default:"0.0.0.0"`
	BindPort uint16 `optional:"" help:"The port on which the aggregation server should listen, defaults to 6453" default:"6453"`
	DataFile string `optional:"" help:"The file in which received data should be saved, defaults to './aggregate.db'" default:"./aggregate.db"`
	KeepData bool   `optional:"" help:"If specified, the previous data will not be deleted on launch" default:"false"`
}

func (receiver *CLIMonitorAggregate) Run() error {
	LaunchAggregator(*receiver)
	return nil
}

type CLIMonitorAggregateViewer struct {
	BindIP   string `optional:"" help:"The IP address on which the webpage server should listen, defaults to 0.0.0.0" default:"0.0.0.0"`
	BindPort string `optional:"" help:"The port on which the webpage server should listen, defaults to 16453" default:"16453"`
	DataFile string `optional:"" help:"The file in which received data should be saved, defaults to './aggregate.db'" default:"./aggregate.db"`
}

func (receiver *CLIMonitorAggregateViewer) Run() error {
	LaunchAggregateServer(*receiver)
	return nil
}

type CLIMonitor struct {
	Aggregate       CLIMonitorAggregate       `cmd:"" help:"Launch the ingestion client which will receive monitoring data from the master node and store it for analysis over UDP"`
	AggregateViewer CLIMonitorAggregateViewer `cmd:"" help:"Launch a simple web application for viewing the aggregation data and evaluating performance"`
}

type CLIFlashMonitor struct {
	Master CLIMonitorMaster `cmd:"" help:"Compile and flash a given device with the master monitoring firmware"`
	Node   CLIMonitorNode   `cmd:"" help:"Compile and flash a given device with the node monitoring firmware"`
}

type CLIFlashMaster struct {
	DeviceIP       string  `required:"" help:"The IP address this device should assign itself on the network"`
	DeviceMAC      string  `optional:"" help:"The MAC address the hardware should assign itself, defaults to D8:36:D9:B8:04:E7" default:"D8:36:D9:B8:04:E7"`
	MetricsIP      *string `optional:"" help:"The IP address to which metrics should be pushed, if not specified it will use the devices IP but with .255 as the final octet as an attempt at broadcast"`
	MetricsPort    uint16  `optional:"" help:"The port on which metrics should be sent, defaults to 6453" default:"6453"`
	DisableMetrics bool    `optional:"" help:"If set, the system will not send any metrics on the network"`
	UniverseFilter []uint8 `required:"" help:"Sets the universes this master will respond to, limited to 3 for hardware constraints"`
	Channel        uint8   `optional:"" help:"The channel on which the RF chip should send and receive, defaults to 76" default:"76"`
	Platform       string  ` optional:"" help:"Specify the platform this is being deployed to, should match an available PlatformIO platform, defaults to atmelavr" default:"atmelavr"`
	Board          string  ` optional:"" help:"Specifies the board this master node will be deployed to, this defaults to megaatmega2560 as from the project" default:"megaatmega2560"`
}

func (receiver *CLIFlashMaster) Validate() error {
	if receiver.Channel > 125 {
		return errors.New("invalid channel, must be in the range 0-125")
	}
	if len(receiver.UniverseFilter) > 3 {
		return errors.New("invalid universe filter, maximum 3 allowed")
	}
	if len(receiver.UniverseFilter) == 0 {
		return errors.New("invalid universe filter, must have at least 1")
	}

	return nil
}

func (receiver *CLIFlashMaster) Run() error {
	ip, err := IPToUint32(receiver.DeviceIP)
	if err != nil {
		Printfln("Invalid device ip address: %+v %+v", receiver.DeviceIP, err)
		return err
	}

	universeFilterString := ""
	for _, v := range receiver.UniverseFilter {
		universeFilterString += "," + strconv.Itoa(int(v))
	}

	buildDefines := map[string]string{
		"DEVICE_IP_ADDRESS":   *ip,
		"DEVICE_MAC_ADDRESS":  MacToByteArray(receiver.DeviceMAC),
		"METRICS_TARGET_PORT": strconv.Itoa(int(receiver.MetricsPort)),
		"RF_CHANNEL":          strconv.Itoa(int(receiver.Channel)),
		"UNIVERSE_FILTER":     universeFilterString[1:],
		"DEVICE_ID":           "0",
	}

	if receiver.MetricsIP != nil {
		met, err := IPToUint32(*receiver.MetricsIP)
		if err != nil {
			Printfln("Invalid device ip address: %+v %+v", receiver.DeviceIP, err)
			return err
		}
		buildDefines["METRICS_IP_ADDRESS"] = *met
	}

	if receiver.DisableMetrics {
		buildDefines["DISABLE_METRICS"] = "true"
	}

	if err := FlashWithEnvironment(
		"uno-master",
		receiver.Platform,
		receiver.Board,
		buildDefines,
	); err != nil {
		return err
	}

	Printfln("Master has been flashed!")

	return nil
}

type CLIFlashNode struct {
	Universe uint8  `required:"" help:"The universe on which this node should listen for data"`
	Channel  uint8  `optional:"" help:"The channel on which the RF chip should send and receive, defaults to 76" default:"76"`
	Platform string ` optional:"" help:"Specify the platform this is being deployed to, should match an available PlatformIO platform, defaults to atmelavr" default:"atmelavr"`
	Board    string ` optional:"" help:"Specifies the board this master node will be deployed to, this defaults to uno as from the project" default:"uno"`
	DeviceID uint8  `required:"" help:"The unique identifier of this node within the entire rfdmx network"`
}

func (receiver *CLIFlashNode) Run() error {
	if err := FlashWithEnvironment(
		"uno-node",
		receiver.Platform,
		receiver.Board,
		map[string]string{
			"UNIVERSE_FILTER": strconv.Itoa(int(receiver.Universe)),
			"RF_CHANNEL":      strconv.Itoa(int(receiver.Channel)),
			"DEVICE_ID":       strconv.Itoa(int(receiver.DeviceID)),
		},
	); err != nil {
		return err
	}

	Printfln("Node has been flashed!")

	return nil
}

type CLIFlash struct {
	Master  CLIFlashMaster  `cmd:"" help:"Compile and flash the master node firmware for the RFDmx deployment"`
	Node    CLIFlashNode    `cmd:"" help:"Compile and flash the node firmware for the RFDmx deployment"`
	Monitor CLIFlashMonitor `cmd:"" help:"Compile and flash the monitoring module"`
	Device  *string         `optional:"" help:"The path to the device on which the script should be uploaded"`
}

var CLI struct {
	Monitor            CLIMonitor `cmd:"" help:"Setup, monitor and view the monitoring deployment of RFDmx"`
	Flash              CLIFlash   `cmd:"" help:"Flash modules with the master and node firmwares with given configurations"`
	PioInstallLocation string     `optional:"" help:"Specifies the install location of platform IO, defaults to ./.pio-venv" default:"./.pio-venv"`
	PythonExecutable   string     `optional:"" help:"Specifies the python executable to use for venv creation" default:"python3"`
	Verbose            bool       `optional:"" help:"If PIO output should be printed" default:"false"`
}

func main() {
	ctx := kong.Parse(&CLI, kong.ConfigureHelp(kong.HelpOptions{Tree: true, FlagsLast: true, NoExpandSubcommands: false}), kong.UsageOnError())
	// Call the Run() method of the selected parsed command.
	err := ctx.Run()
	ctx.FatalIfErrorf(err)
}
