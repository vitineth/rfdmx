module rfdmx

go 1.20

require (
	github.com/alecthomas/kong v0.7.1
	gopkg.in/ini.v1 v1.67.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dgryski/go-lttb v0.0.0-20230207170358-f8fc36cdbff1 // indirect
	github.com/dinedal/go-sqlite3-extension-functions v0.0.0-20200202173959-679ee1ae1f0b // indirect
	github.com/mattn/go-sqlite3 v1.14.16 // indirect
	gobn.github.io/coalesce v1.0.2 // indirect
	golang.org/x/exp v0.0.0-20220927162542-c76eaa363f9d // indirect
)
