package main

import (
	"database/sql"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"net"
	"os"
)

// RuntimeMetric represents the deserialized data received from a master node during normal operation
type RuntimeMetric struct {
	requestedSinceLastUpdate uint32
	hasOverflowed            bool
	nodeCount                uint8
}

// StoredRuntimeMetric is a received metric but with the added metadata of the time at which it was added, represented
// as a string from SQLite
type StoredRuntimeMetric struct {
	RuntimeMetric
	added string
}

// DropSet represents the deserialized data received from a master node in metrics mode which contains the bitsets for
// packet tracking and some packet counts
type DropSet struct {
	magic                           uint8
	usedBits                        uint8
	packetMatchingBitset            uint32
	packetMatchingBitsetStartOffset uint32
	dropsSinceLastSend              uint32
	usedBitsInvalid                 bool
}

// RTTPoint represents a single RTT measurement, combining the timestamp received and the measured RTT
type RTTPoint struct {
	timestamp uint32
	rtt       uint32
}

// RTTSet is the deserialized rtt metrics packet from a master node during metrics mode. It contains the magic number
// and the series of RTT readings contained within it
type RTTSet struct {
	magic uint8
	rtts  []RTTPoint
}

// DropState is a type alias for the dropped state of a packet. While uint8 is excessive its the best available
type DropState = uint8

var (
	// DSSeen means that a packet was sent and seen in the next ack
	DSSeen DropState = 0
	// DSDropped means that the packet was sent but no ack was ever received
	DSDropped DropState = 1
	// DSKnownDropped means that a packet was sent, its single ack was missed but the ID was included in a later packet
	DSKnownDropped DropState = 2
)

// This is the mapping from packet IDs to their drop states globally received
var globalDropMap = map[int]DropState{}

// handleDropset will extract the packet IDs and states from the provided dropset and insert them into the global map,
// updating any values that are already present and are no longer accurate. This will also insert the data into the
// database using the statement provided
func handleDropset(drops DropSet, db *sql.Stmt) {
	for i := 0; i < 32; i++ {
		packetId := int(drops.packetMatchingBitsetStartOffset-16) + i
		if packetId < 0 {
			continue
		}

		seen := (drops.packetMatchingBitset>>(31-i))&0b1 == 1
		if previouslySeen, present := globalDropMap[packetId]; present {
			if seen {
				// Ignore if this is a repeat of information we already know
				if previouslySeen != DSKnownDropped {
					// If it was previously dropped, we now know it was recovered
					if previouslySeen == DSDropped {
						globalDropMap[packetId] = DSKnownDropped
						if _, err := db.Exec(packetId, true, true); err != nil {
							Printfln("[aggregate]: failed to update status for packet %v due to error %+v", packetId, err)
						}
					} else {
						// Otherwise its just seen
						globalDropMap[packetId] = DSSeen
						if _, err := db.Exec(packetId, false, false); err != nil {
							Printfln("[aggregate]: failed to update status for packet %v due to error %+v", packetId, err)
						}
					}
				}
				// Otherwise is doesn't actually matter as the dropped data is already in the array
			}
		} else {
			if seen {
				globalDropMap[packetId] = DSSeen
				if _, err := db.Exec(packetId, false, false); err != nil {
					Printfln("[aggregate]: failed to update status for packet %v due to error %+v", packetId, err)
				}
			} else {
				globalDropMap[packetId] = DSDropped
				if _, err := db.Exec(packetId, true, false); err != nil {
					Printfln("[aggregate]: failed to update status for packet %v due to error %+v", packetId, err)
				}
			}
		}
	}
}

// handleRTTs will insert each RTT entry into the database using the statement provided, logging any errors
func handleRTTs(rtts RTTSet, db *sql.Stmt) {
	for _, v := range rtts.rtts {
		_, err := db.Exec(v.timestamp, v.rtt)
		if err != nil {
			Printfln("[aggregate]: failed to insert rtt values: %+v", err)
		}
	}
}

// handleRuntimeMetrics will attempt to insert the parsed metrics into the database using the statement provided,
// printing any errors
func handleRuntimeMetrics(metrics RuntimeMetric, stmt *sql.Stmt) {
	_, err := stmt.Exec(metrics.requestedSinceLastUpdate, metrics.hasOverflowed, metrics.nodeCount)
	if err != nil {
		Printfln("[aggregate]: failed to insert runtime values: %+v", err)
	}
}

// bufferUint32 will reconstruct a uint32 from a set of 4 bytes in big endian notation. This assumes the buffer is long
// enough and does no further checks
func bufferUint32(buf []byte) uint32 {
	return (uint32(buf[0]) << 24) | (uint32(buf[1]) << 16) | (uint32(buf[2]) << 8) | uint32(buf[3])
}

// handlePacket will determine the packet type based on length and magic number and distribute it to one of
// handleRuntimeMetrics, handleDropset or handleRTTs, with the parsing of hte packets being processed before
func handlePacket(buf []byte, dropStatement *sql.Stmt, rrtStatement *sql.Stmt, runtimeStmt *sql.Stmt) {
	if len(buf) == 6 {
		handleRuntimeMetrics(RuntimeMetric{
			bufferUint32(buf[0:]),
			buf[4] == 1,
			buf[5],
		}, runtimeStmt)
	} else if buf[0] == 0xFA {
		handleDropset(DropSet{
			magic:                           buf[0],
			usedBits:                        buf[1],
			packetMatchingBitset:            bufferUint32(buf[2:]),
			packetMatchingBitsetStartOffset: bufferUint32(buf[11:]),
			dropsSinceLastSend:              bufferUint32(buf[6:]),
			usedBitsInvalid:                 false,
		}, dropStatement)
	} else if buf[0] == 0xAE {
		amount := (len(buf) - 1) / ((2 * 4) + 3)
		set := make([]RTTPoint, amount)
		for i := 0; i < amount; i++ {
			startIndex := 1 + (i * 11)
			set[i] = RTTPoint{
				timestamp: bufferUint32(buf[startIndex:]),
				rtt:       bufferUint32(buf[startIndex+5:]),
			}
		}

		handleRTTs(RTTSet{
			magic: buf[0],
			rtts:  set,
		}, rrtStatement)
	}
}

// LaunchAggregator will create a new database connection to the provided file, if KeepData is not specified it will
// delete the existing file. If the tables do not exist they will be created and statements prepared for each insert. It
// will start listening for UDP packets on the port host combination, processing them via handlePacket
func LaunchAggregator(config CLIMonitorAggregate) {
	if !config.KeepData {
		err := os.Remove(config.DataFile)
		if err != nil {
			if t, ok := err.(*os.PathError); !ok || (ok && t.Err.Error() != "no such file or directory") {
				Printfln("Failed to remove existing data = %+v", err)
			}
		}
	}

	Printfln("[aggregate]: setting up database")
	db, err := sql.Open("sqlite3", config.DataFile)
	if err != nil {
		Printfln("[aggregate]: failed to open the database at aggregate.db: %+v", err)
		return
	}
	defer db.Close()

	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS drop_mapping(packet_id BIGINT UNIQUE , was_dropped BOOLEAN, recovered BOOLEAN, added DATE DEFAULT (datetime('now')));`)
	if err != nil {
		Printfln("[aggregate]: failed to set up the drop mapping table in the db due to error: %+v", err)
		return
	}
	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS rtt(timestamp BIGINT, rtt BIGINT, added DATE DEFAULT (datetime('now')));`)
	if err != nil {
		Printfln("[aggregate]: failed to set up the RTT table in the db due to error: %+v", err)
		return
	}
	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS runtime(dropped BIGINT, overflow BOOL, nodes INTEGER, added DATE DEFAULT (datetime('now')))`)
	if err != nil {
		Printfln("[aggregate]: failed to set up the RTT table in the db due to error: %+v", err)
		return
	}

	dropStmt, err := db.Prepare("INSERT INTO drop_mapping (packet_id, was_dropped, recovered) VALUES (?, ?, ?) ON CONFLICT (packet_id) DO UPDATE SET was_dropped=excluded.was_dropped, recovered=excluded.recovered;")
	if err != nil {
		Printfln("[aggregate]: failed to prepare drop mapping statement due to error: %+v", err)
		return
	}

	rttStmt, err := db.Prepare("INSERT INTO rtt (timestamp, rtt) VALUES (?, ?);")
	if err != nil {
		Printfln("[aggregate]: failed to prepare rtt statement due to error: %+v", err)
		return
	}

	runtimeStmt, err := db.Prepare("INSERT INTO runtime (dropped, overflow, nodes) VALUES (?, ?, ?);")
	if err != nil {
		Printfln("[aggregate]: failed to prepare runtime statement due to error: %+v", err)
		return
	}

	Printfln("[aggregate]: setting up server")
	addr := net.UDPAddr{
		IP:   net.ParseIP(config.BindIP),
		Port: int(config.BindPort),
	}

	conn, err := net.ListenUDP("udp", &addr)
	if err != nil {
		Printfln("[aggregator]: failed to start listening on the specified network (%v:%v) due to error: %+v", config.BindIP, config.BindPort, err)
		return
	}

	err = conn.SetReadBuffer(5e+8) // 0.5GB
	if err != nil {
		Printfln("[aggregator]: failed to increase the read buffer: %+v", err)
	}
	defer conn.Close()

	Printfln("[aggregator]: launched with binding to %v:%v", config.BindIP, config.BindPort)
	buffer := make([]byte, 2048)
	for {
		rlen, _, err := conn.ReadFromUDP(buffer)
		if err != nil {
			Printfln("[aggregator]: failed to read from the UDP server due to an error: %+v", err)
			continue
		}

		fmt.Printf("[%v]", rlen)
		go handlePacket(buffer[0:rlen], dropStmt, rttStmt, runtimeStmt)
	}
}
