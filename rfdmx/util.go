package main

import "fmt"

// Printfln is a wrapper around fmt.Printf which automatically appends a new line to the end
func Printfln(format string, args ...interface{}) {
	fmt.Printf(format+"\n", args...)
}
