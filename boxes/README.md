# Boxes

These are some simple 3D-printable containers for the nodes. They are not designed to be beautiful or completely perfect (ie the tops might not fit always). They are given in .blend and .stl formats. 

The blend file contains the actual 3d model based on externally sourced models of the components. These have then been exported into 4 STL files representing the top and bottom for each of the master and the node implementations. 

Arduino Uno R3 Source: https://sketchfab.com/3d-models/arduino-uno-board-f31feafc5e9743abbdf33c54f9d92669
Arduino Mega 2560 Source: https://www.thingiverse.com/thing:4586109
nRF Source: https://www.thingiverse.com/thing:3881872/files